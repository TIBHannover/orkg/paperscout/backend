import json
import random
from typing import Dict, List

import requests
from pydantic import BaseModel, Field

from app.core import settings
from app.errors import InternalLLMCallError
from app.models.llm import LLMChatMessage
from app.modules.llms.base import (
    LLM,
    LLMChatRequest,
    LLMGenerateRequest,
    LLMResponse,
    LLMType,
    Prompt,
)


class DummyLLM(LLM):
    def __init__(self):
        super().__init__(name="dummy", llm_type=LLMType.LOCAL)

    def invoke(self, prompt: Prompt, seed: int | None = None) -> LLMResponse:
        return LLMResponse(response="Dummy LLM invoked", params={})

    def generate_invoke(self, _: LLMGenerateRequest) -> Dict:
        return {"response": "Dummy LLM invoked"}

    def generate_chat_invoke(self, _: LLMChatRequest) -> Dict:
        raise {"response": "Dummy LLM invoked"}


class OllamaLLM(LLM):
    ollama_host: str = Field(..., description="The Ollama host")
    model: str = Field(..., description="The model to use")
    stream: bool = Field(
        False, description="Whether to stream the output or not (default: False)"
    )

    def __init__(self, ollama_host: str, model: str, stream: bool = False):
        super().__init__(
            name="ollama",
            llm_type=LLMType.LOCAL,
            ollama_host=ollama_host,
            model=model,
            stream=stream,
        )

    def _call_api(self, payload: Dict) -> Dict:
        with requests.sessions.Session() as http:
            response = http.post(
                self.ollama_host + "api/generate",
                headers={"Content-Type": "application/json"},
                data=json.dumps(payload),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()

    def _call_chat_complete_api(self, payload: Dict) -> Dict:
        with requests.sessions.Session() as http:
            response = http.post(
                self.ollama_host + "api/chat",
                headers={"Content-Type": "application/json"},
                data=json.dumps(payload),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()

    def _call_embeddings_api(self, payload: Dict) -> Dict:
        with requests.sessions.Session() as http:
            response = http.post(
                self.ollama_host
                + ("/" if not self.ollama_host.endswith("/") else "")
                + "api/embeddings",
                headers={"Content-Type": "application/json"},
                data=json.dumps(payload),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()

    def invoke(self, prompt: Prompt, seed: int | None = None) -> LLMResponse:
        options = {
            "top_k": 4,
            "temperature": 1,
            "num_ctx": 4096,
            "seed": seed
            if seed is not None
            else random.randint(10000000000, 9000000000000),
        }
        if settings.llm.params.context_size is not None:
            options["num_ctx"] = settings.llm.params.context_size
        generate_response = self._call_api(
            {
                "model": self.model,
                "system": prompt.system_prompt,
                "prompt": prompt.user_prompt,
                "stream": self.stream,
                "options": options,
            }
        )
        params: dict = options.copy()
        params["finish_reason"] = generate_response.get("done_reason", "stop")
        params["stream"] = self.stream
        params["eval_count"] = generate_response.get("eval_count", None)
        params["total_duration"] = generate_response.get("total_duration", None)
        params["engine"] = "ollama"
        params["model"] = self.model
        return LLMResponse(
            response=generate_response.get("response", ""), params=params
        )

    def generate_invoke(self, generate_request: LLMGenerateRequest) -> Dict:
        # Preparing Ollama /generate payload
        payload = {
            "model": generate_request.model,
            "system": generate_request.system,
            "prompt": generate_request.prompt,
            "stream": generate_request.stream,
            "options": {},
        }
        advanced_options = generate_request.model_dump()
        advanced_options.pop("model")
        advanced_options.pop("system")
        advanced_options.pop("prompt")
        advanced_options.pop("stream")
        payload["options"]["num_ctx"] = advanced_options.pop("context_size", 4096)
        payload["options"].update(advanced_options)

        return {"response": self._call_api(payload)}

    def generate_chat_invoke(self, chat_request: LLMChatRequest) -> Dict:
        request_dict = chat_request.model_dump()
        payload = {"stream": chat_request.stream, "messages": chat_request}
        request_dict.pop("stream")
        request_dict.pop("messages")
        if "model" in request_dict:
            payload["model"] = request_dict.pop("model")
        payload["options"] = request_dict

        return self._call_chat_complete_api(payload)


class TgiRequestPayload(BaseModel):
    best_of: int = Field(1, description="The number of best completions to return")
    decoder_input_details: bool = Field(
        False, description="Whether to return the decoder input details"
    )
    details: bool = Field(False, description="Whether to return the details")
    do_sample: bool = Field(True, description="Whether to sample the output")
    max_new_tokens: int | None = Field(
        None, description="The maximum number of new tokens to generate"
    )
    frequency_penalty: float = Field(0.1, description="The frequency penalty")
    repetition_penalty: float = Field(1.03, description="The repetition penalty")
    return_full_text: bool = Field(False, description="Whether to return the full text")
    seed: int | None = Field(None, description="The seed")
    temperature: float = Field(0.7, description="The temperature")
    top_k: int = Field(
        10, description="The number of tokens to consider for the top-k sampling"
    )
    top_n_tokens: int = Field(
        5, description="The number of tokens to consider for the top-n sampling"
    )
    top_p: float = Field(0.95, description="The nucleus sampling")
    typical_p: float = Field(0.95, description="The typical p")
    watermark: bool = Field(True, description="Whether to add a watermark")
    truncate: int | None = Field(
        None, description="The maximum number of tokens to truncate"
    )


class TgiRequest(BaseModel):
    inputs: str = Field(..., description="The input prompt")
    parameters: TgiRequestPayload = Field(..., description="The request parameters")


class TgiChatRequest(BaseModel):
    model: str | None = Field(
        None,
        description="The model to use for generation. If not provided, the default model will be used",
    )
    stream: bool = Field(
        True, description="Whether to stream the output or not (default: True)"
    )
    messages: List[LLMChatMessage] = Field(
        ..., description="The messages to generate the response for"
    )
    temperature: float = Field(
        0.5, description="The temperature to use for generation. Default is 0.5."
    )
    top_p: float = Field(0.95, description="The nucleus sampling")
    max_tokens: int = Field(100, description="The maximum number of tokens to generate")
    seed: int | None = Field(None, description="The seed to use for generation")
    frequency_penalty: float = Field(1, description="The frequency penalty")
    presence_penalty: float = Field(0.1, description="The presence penalty")


class TgiInfo(BaseModel):
    model: str = Field(..., description="The model ID", alias="model_id")
    sha: str = Field(..., description="The model SHA", alias="model_sha")
    max_concurrent_requests: int = Field(
        ..., description="The maximum number of concurrent requests"
    )
    max_best_of: int = Field(..., description="The maximum number of best completions")
    max_input_tokens: int = Field(..., description="The maximum input tokens")
    max_total_tokens: int = Field(..., description="The maximum total tokens")
    router: str = Field(..., description="The router")
    version: str = Field(..., description="The TGI version")
    validation_workers: int = Field(..., description="The number of validation workers")


class TgiLLM(LLM):
    host: str = Field(..., description="The TGI host")
    stream: bool = Field(
        False, description="Whether to stream the output or not (default: False)"
    )

    def __init__(self, host: str, stream: bool = False):
        super().__init__(name="tgi", llm_type=LLMType.LOCAL, host=host, stream=stream)
        self._model_name_cache = None

    def get_model(self) -> str:
        if self._model_name_cache is None:
            self._model_name_cache = self._info().model
        return self._model_name_cache

    def invoke(self, prompt: Prompt, seed: int | None = None) -> LLMResponse:
        model_prompt = f"{prompt.system_prompt} {prompt.user_prompt}".strip()

        max_tokens_config = settings.llm.params.max_input_tokens

        output_token_size_buffer = 1000
        input_tokens = self._tokenize_text(model_prompt)
        if len(input_tokens) <= max_tokens_config - output_token_size_buffer:
            max_new_tokens = min(
                max_tokens_config - len(input_tokens), output_token_size_buffer
            )
        else:
            # truncate the input tokens to fit the maximum input tokens
            input_tokens = input_tokens[: max_tokens_config - output_token_size_buffer]
            model_prompt = "".join([t["text"] for t in input_tokens])
            max_new_tokens = output_token_size_buffer

        payload = TgiRequest(
            inputs=model_prompt,
            parameters=TgiRequestPayload(
                max_new_tokens=max_new_tokens,
                seed=seed
                if seed is not None
                else random.randint(100000000, 9000000000000),
            ),
        )

        generate_response = self._call_generate(payload)
        llm_response_string = generate_response.get("generated_text", "")
        params = payload.parameters.model_dump()
        params["seed"] = params.get("seed", seed)
        params["finish_reason"] = generate_response.get("finish_reason", "stop")
        params["engine"] = "tgi"
        params["model"] = self.get_model()
        # Remove TGI unnecessary parameters
        params.pop("decoder_input_details")
        params.pop("details")
        params.pop("return_full_text")
        return LLMResponse(response=llm_response_string, params=params)

    def generate_invoke(self, generate_request: LLMGenerateRequest) -> Dict:
        prompt = f"{generate_request.system} {generate_request.prompt}".strip()
        payload_dict = generate_request.model_dump()
        # Remove TGI unnecessary parameters
        payload_dict.pop("model")
        payload_dict.pop("system")
        payload_dict.pop("prompt")
        payload_dict.pop("stream")
        payload_dict.pop("context_size")
        payload_dict.pop("context")
        payload = TgiRequest(
            inputs=prompt,
            parameters=generate_request.model_dump(),
        )

        return {"response": self._call_generate(payload)}

    def generate_chat_invoke(self, chat_request: LLMChatRequest) -> Dict:
        payload = chat_request.model_dump()
        payload["max_tokens"] = payload.pop("truncate", 3000)
        model = payload.get("model", None)
        if model is None:
            payload["model"] = self._info().model
        return self._call_chat_complete_api(payload=TgiChatRequest(**payload))

    def _info(self) -> TgiInfo:
        """
        Get the Text Generation Inference (TGI) info.
        :return: the TGI engine information
        """
        with requests.sessions.Session() as http:
            response = http.get(
                self.host + ("/" if not self.host.endswith("/") else "") + "info",
                headers={"Content-Type": "application/json"},
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return TgiInfo(**response.json())

    def _tokenize_text(self, text: str) -> List:
        with requests.sessions.Session() as http:
            response = http.post(
                self.host + ("/" if not self.host.endswith("/") else "") + "tokenize",
                headers={"Content-Type": "application/json"},
                data=json.dumps({"inputs": text}),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()

    def _call_generate(self, payload: TgiRequest) -> Dict:
        with requests.sessions.Session() as http:
            response = http.post(
                self.host + ("/" if not self.host.endswith("/") else "") + "generate",
                headers={"Content-Type": "application/json"},
                data=json.dumps(payload.model_dump(mode="json")),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()

    def _call_chat_complete_api(self, payload: TgiChatRequest) -> Dict:
        with requests.sessions.Session() as http:
            response = http.post(
                self.host
                + ("/" if not self.host.endswith("/") else "")
                + "v1/chat/completions",
                headers={"Content-Type": "application/json"},
                data=json.dumps(payload.model_dump(mode="json")),
            )
            if response.status_code != 200:
                raise InternalLLMCallError(response.text)
            return response.json()
