# Adapted from https://github.com/ParisNeo/ollama_proxy_server/blob/main/Dockerfile
FROM python:3.11

# Define ARGs
ARG SERVER1
ARG QUEUE1
ARG SERVER2
ARG QUEUE2

# Update packagtes, install necessary tools into the base image, clean up and clone git repository
RUN apt update \
    && apt install -y --no-install-recommends --no-install-suggests git apache2 \
    && apt autoremove -y --purge \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && git clone https://github.com/ParisNeo/ollama_proxy_server.git

# Change working directory to cloned git repository
WORKDIR ollama_proxy_server

# Copy proxy.py file and replace the main.py file with it
RUN rm ollama_proxy_server/main.py
COPY proxy.py ollama_proxy_server/main.py

# Install all needed requirements
RUN pip3 install -e .

# Create the config.init file
RUN echo "[Server1]\nurl = ${SERVER1}\nqueue_size = ${QUEUE1}\n\n[Sever2]\nurl = ${SERVER2}\nqueue_size = ${QUEUE2}\n" > config.ini

# Start the proxy server as entrypoint
ENTRYPOINT ["ollama_proxy_server"]

# Set command line parameters
CMD ["--config", "./config.ini", "--port", "8080"]
