import uuid
from enum import Enum
from typing import Any, Dict, List

from qdrant_client import QdrantClient, models
from qdrant_client.http.models import PayloadSchemaType
from qdrant_client.models import (
    Distance,
    PointStruct,
    Record,
    ScoredPoint,
    VectorParams,
)

from app import FilterString
from app.core import settings
from app.mocks.infinity import DummyInfinityEngine
from app.modules.embeddings.base import EmbeddingsEngine
from app.modules.embeddings.infinity import InfinityEmbeddingsEngine
from app.modules.embeddings.tei import TextEmbeddingsInferenceEngine
from app.modules.indexing.base import InternalID, IStore
from app.modules.indexing.vectordb.models import (
    QdrantDocument,
    QdrantFilter,
    QdrantPageResponse,
)

COLLECTION_NAME = "papers"


class QdrantStore(IStore):
    """
    Vector database store implementation using Qdrant.
    """

    _client: QdrantClient = None
    _encoder: EmbeddingsEngine = None
    instance: "QdrantStore" = None

    def __init__(self, in_memory: bool = False):
        """
        Initialize the store.
        :param in_memory: Whether to use an in-memory store. (default: False) For testing purposes only.
        """
        self._client = QdrantClient(
            ":memory:" if in_memory else settings.vector_store.host,
            prefer_grpc=settings.vector_store.prefer_grpc,
            grpc_port=settings.vector_store.grpc_port,
        )
        if settings.general.is_test_environment():
            self._encoder = DummyInfinityEngine()
        else:
            if settings.embeddings.provider == "infinity":
                self._encoder = InfinityEmbeddingsEngine(
                    model=settings.embeddings.model
                )
            elif settings.embeddings.provider == "tei":
                self._encoder = TextEmbeddingsInferenceEngine()
        self._create_collection_if_needed()

    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super().__new__(cls)
        return cls.instance

    def _reinitialize(self):
        """
        Reinitialize the store.
        For testing purposes only.
        """
        self.instance = None
        self.__init__(in_memory=True)

    @staticmethod
    def _to_domain_model(
        records_or_points: List[Record | ScoredPoint],
    ) -> List[QdrantDocument]:
        if len(records_or_points) == 0:
            return []
        return [QdrantDocument(**record.payload) for record in records_or_points]

    def _create_collection_if_needed(self):
        if COLLECTION_NAME not in [
            collection.name for collection in self._client.get_collections().collections
        ]:
            # The collection does not exist
            self._client.create_collection(
                collection_name=COLLECTION_NAME,
                vectors_config=VectorParams(
                    size=self._encoder.ndims(), distance=Distance.COSINE, on_disk=False
                ),
                shard_number=2,
                optimizers_config=models.OptimizersConfigDiff(
                    indexing_threshold=20000,
                    default_segment_number=4,
                ),
                quantization_config=models.ScalarQuantization(
                    scalar=models.ScalarQuantizationConfig(
                        type=models.ScalarType.INT8, quantile=0.99
                    )
                ),
            )

            # Create index on the id field of the payload
            self.setup_indices()

    def setup_indices(self):
        # Create index on the id field of the payload
        self._client.create_payload_index(
            collection_name=COLLECTION_NAME,
            field_name="id",
            field_schema=PayloadSchemaType.KEYWORD,
        )
        # Create index on the title field of the payload (full text index)
        self._client.create_payload_index(
            collection_name=COLLECTION_NAME,
            field_name="title",
            field_schema=PayloadSchemaType.TEXT,
        )
        # Create index on the language field of the payload
        self._client.create_payload_index(
            collection_name=COLLECTION_NAME,
            field_name="language",
            field_schema=PayloadSchemaType.KEYWORD,
        )
        # Create index on the year field of the payload
        self._client.create_payload_index(
            collection_name=COLLECTION_NAME,
            field_name="year",
            field_schema=PayloadSchemaType.INTEGER,
        )

    def turn_off_indexing(self):
        self._client.update_collection(
            collection_name=COLLECTION_NAME,
            optimizer_config=models.OptimizersConfigDiff(
                indexing_threshold=0,
            ),
        )

    def turn_on_indexing(self):
        self._client.update_collection(
            collection_name=COLLECTION_NAME,
            optimizer_config=models.OptimizersConfigDiff(
                indexing_threshold=20000,
            ),
        )

    def set_vectors_to_memory(self):
        self._client.update_collection(
            collection_name=COLLECTION_NAME,
            vectors_config={"": models.VectorParamsDiff(on_disk=False)},
        )

    def index(self, document: QdrantDocument, *args, **kwargs) -> str:
        # Check if the document ID already exists
        if self.count(QdrantFilter.id_filter(document.id)) > 0:
            if "ignore_existing" in kwargs and kwargs["ignore_existing"]:
                return document.id
            raise ValueError(f"Document with id {document.id} already exists")

        # We can now index the document
        self._client.upsert(
            collection_name=COLLECTION_NAME,
            points=[
                PointStruct(
                    id=str(uuid.uuid4()),
                    vector=self._encoder.compute_source_embeddings(
                        texts=document.get_source_fields_as_text()
                    )[0],
                    payload=document.model_dump(),
                )
            ],
        )
        return document.id

    def bulk_index(self, documents: List[QdrantDocument], *args, **kwargs) -> List[str]:
        if len(documents) == 0:
            return []

        vectors = self._encoder.compute_source_embeddings(
            texts=[document.get_source_fields_as_text() for document in documents]
        )

        self._client.upsert(
            collection_name=COLLECTION_NAME,
            points=[
                PointStruct(
                    id=str(uuid.uuid4()),
                    vector=vectors[idx],
                    payload=document.model_dump(),
                )
                for idx, document in enumerate(documents)
            ],
        )

        return [document.id for document in documents]

    def get(
        self,
        doc_id: InternalID | None = None,
        criteria: QdrantFilter | FilterString | None = None,
        limit: int = 1,
        offset: uuid.UUID | None = None,
        order_by: str | None = None,
        focus: List[str] | None = None,
        *args,
        **kwargs,
    ) -> QdrantPageResponse:
        # Defensive check
        if doc_id is None and criteria is None:
            raise ValueError("Either doc_id or criteria must be provided")
        if doc_id is not None and focus is not None:
            raise ValueError("Focus cannot be applied when fetching by ID")

        # Prepare the focus filter and merge it with the criteria filter
        focus_condition = self._convert_focus_to_filter(focus=focus)
        criteria_condition = self._merge_conditions_with_focus(
            focus_condition=focus_condition,
            criteria_condition=None if criteria is None else criteria.to_filter(),
        )

        count = self.count(criteria=criteria_condition)

        if doc_id is not None:
            records = self._client.scroll(
                collection_name=COLLECTION_NAME,
                scroll_filter=models.Filter(
                    must=[
                        models.FieldCondition(
                            key="id", match=models.MatchValue(value=doc_id)
                        )
                    ]
                ),
                limit=limit,
                offset=offset,
                with_payload=True,
            )[0]
        else:
            records = self._client.scroll(
                collection_name=COLLECTION_NAME,
                scroll_filter=criteria_condition,
                limit=limit * 2,
                offset=str(offset) if isinstance(offset, uuid.UUID) else offset,
                with_payload=True,
                order_by=order_by,
            )
            # Deduplicate the points based on their payload (id), and keeping the order intact
            uuid_offset = records[1]
            records = [
                record
                for idx, record in enumerate(records[0])
                if record.payload["id"]
                not in [r.payload["id"] for r in records[0][:idx]]
            ]
            records = (records[:limit], uuid_offset)

        return QdrantPageResponse.from_offset_and_points(
            items=[
                item
                for item in self._to_domain_model(
                    records_or_points=(
                        records[0] if isinstance(records, tuple) else records
                    )
                )
            ],
            total=count,
            offset=records[1] if isinstance(records, tuple) else offset,
        )

    @staticmethod
    def _convert_focus_to_filter(focus: List[str] | None) -> models.Filter | None:
        if focus is None:
            return None
        # Prepare the source filter
        sources = (
            "[" + ", ".join([f'"{s}"' for s in focus or []]) + "]" if focus else None
        )
        source_filter: FilterString = ...
        if focus is None or settings.vector_store.search.DEFAULT_SOURCE in focus:
            source_filter = FilterString(f"IS_EMPTY(source) OR source IN {sources}")
        elif focus is not None:
            source_filter = FilterString(f"source IN {sources}")
        # Return the filter
        return source_filter.to_filter() if source_filter else None

    @staticmethod
    def _merge_conditions_with_focus(
        focus_condition: models.Filter | None, criteria_condition: models.Filter | None
    ) -> models.Filter | None:
        if focus_condition is None:
            return criteria_condition
        if criteria_condition is None:
            return focus_condition
        # Append the should part (in case of IS_BLANK)
        if focus_condition.should is not None:
            if criteria_condition.should is None:
                criteria_condition.should = focus_condition.should
            else:
                criteria_condition.should += focus_condition.should
        # Append the must part (in case of select values)
        if focus_condition.must is not None:
            if criteria_condition.must is None:
                criteria_condition.must = focus_condition.must
            else:
                criteria_condition.must += focus_condition.must
        return criteria_condition

    def search(
        self,
        query: str,
        limit: int = 10,
        offset: int | None = None,
        criteria: QdrantFilter | FilterString | None = None,
        include_vectors: bool = False,
        focus: List[str] | None = None,
        *args,
        **kwargs,
    ) -> QdrantPageResponse:
        # Prepare the focus filter and merge it with the criteria filter
        focus_condition = self._convert_focus_to_filter(focus=focus)
        criteria_condition = self._merge_conditions_with_focus(
            focus_condition=focus_condition,
            criteria_condition=None if criteria is None else criteria.to_filter(),
        )
        # Get total count
        count = self.count(criteria=criteria_condition)

        # Perform the search
        points = self._client.search(
            collection_name=COLLECTION_NAME,
            query_vector=self._encoder.compute_query_embeddings(query=query)[0],
            query_filter=criteria_condition,
            limit=(limit + (offset or 0)) * 2,
            with_vectors=include_vectors,
            search_params=models.SearchParams(
                indexed_only=settings.vector_store.search.indexed_only,
            ),
        )
        # Deduplicate the points based on their payload (id), and keeping the order intact
        points = [
            point
            for idx, point in enumerate(points)
            if point.payload["id"] not in [p.payload["id"] for p in points[:idx]]
        ]
        if settings.vector_store.search.distinct_by_title:
            points = [
                point
                for idx, point in enumerate(points)
                if point.payload["title"]
                not in [p.payload["title"] for p in points[:idx]]
            ]
        points = points[(offset or 0) : (limit + (offset or 0))]
        return QdrantPageResponse.from_offset_and_points(
            items=[item for item in self._to_domain_model(records_or_points=points)],
            total=count,
            offset=offset,
        )

    def delete(
        self,
        criteria: QdrantFilter | FilterString | None = None,
        doc_id: str | int = None,
        *args,
        **kwargs,
    ) -> int:
        # Defensive check
        if doc_id is None and criteria is None:
            raise ValueError("Either doc_id or criteria must be provided")
        if doc_id is not None:
            self._client.delete(
                collection_name=COLLECTION_NAME,
                points_selector=models.Filter(
                    must=[
                        models.FieldCondition(
                            key="id", match=models.MatchValue(value=doc_id)
                        )
                    ]
                ),
            )
            return 1
        else:
            points = self.count(criteria=criteria)
            self._client.delete(
                collection_name=COLLECTION_NAME, points_selector=criteria.to_filter()
            )
            return points

    def count(
        self,
        criteria: QdrantFilter | FilterString | models.Filter | None = None,
        *args,
        **kwargs,
    ) -> int:
        if criteria is None:
            return self._client.get_collection(
                collection_name=COLLECTION_NAME
            ).points_count
        else:
            count_filter = (
                criteria
                if isinstance(criteria, models.Filter)
                else criteria.to_filter()
            )
            return self._client.count(
                collection_name=COLLECTION_NAME, count_filter=count_filter
            ).count

    def _get_internal_ids(self, ids: List[InternalID]) -> List[InternalID]:
        records = self._client.scroll(
            collection_name=COLLECTION_NAME,
            scroll_filter=QdrantFilter.multiple_ids_filter(ids).to_filter(),
            limit=len(ids),
            offset=0,
            with_payload=False,
        )[0]
        return [record.id for record in records]

    def recommend(
        self,
        ids: List[InternalID],
        avoid: List[InternalID] = None,
        limit: int = 10,
        offset: int | None = None,
        criteria: QdrantFilter | FilterString | None = None,
        *args,
        **kwargs,
    ) -> List[QdrantDocument]:
        positive_ids = self._get_internal_ids(ids)

        negative_ids = None if avoid is None else self._get_internal_ids(avoid)

        points = self._client.recommend(
            collection_name=COLLECTION_NAME,
            positive=positive_ids,
            negative=negative_ids,
            query_filter=None if criteria is None else criteria.to_filter(),
            limit=(limit + (offset or 0)) * 2,
            offset=offset,
            strategy=models.RecommendStrategy.BEST_SCORE,
        )

        # Deduplicate the points based on their payload (id), and keeping the order intact
        points = [
            point
            for idx, point in enumerate(points)
            if point.payload["id"] not in [p.payload["id"] for p in points[:idx]]
        ]
        if settings.vector_store.search.distinct_by_title:
            points = [
                point
                for idx, point in enumerate(points)
                if point.payload["title"]
                not in [p.payload["title"] for p in points[:idx]]
            ]
        points = points[(offset or 0) : (limit + (offset or 0))]

        return self._to_domain_model(records_or_points=points)

    def info(self) -> Dict[str, Any]:
        def object_to_dict(obj) -> Dict[str, Any]:
            result = {}
            for k, v in obj.__dict__.items():
                if not k.startswith("_") and not callable(v):
                    if isinstance(v, list):
                        result[k] = object_to_dict(v) if len(v) > 0 else v
                    elif isinstance(v, Enum):
                        result[k] = v.value
                    elif (
                        isinstance(v, int)
                        or isinstance(v, float)
                        or isinstance(v, str)
                        or isinstance(v, bool)
                        or isinstance(v, dict)
                        or isinstance(v, set)
                    ):
                        result[k] = v
                    elif v is None:
                        result[k] = None
                    else:
                        result[k] = object_to_dict(v)
            return result

        return object_to_dict(
            self._client.get_collection(collection_name=COLLECTION_NAME)
        )
