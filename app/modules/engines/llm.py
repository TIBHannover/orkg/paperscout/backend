from typing import Any, Dict

from singleton_decorator import singleton

from app.models.llm import LLMChatRequest
from app.modules.caching.models import UsedPrompt
from app.modules.engines.base import BaseEngine
from app.modules.llms.base import LLMGenerateRequest, ParserOutput
from app.modules.llms.factories import LLMFactory
from app.modules.prompting.prompts import PromptIds
from app.modules.statemachine.machines import (
    KeyValuePairsExtractionStateMachine,
    SynthesisStateMachine,
)

OptionalSeed = int | None
KvpPrompts = Dict[str, UsedPrompt]
SynPrompt = Dict[str, Any]


@singleton
class LLMEngine(BaseEngine):
    def run_key_value_pairs_extraction(
        self,
        item_id: int | str | None,
        collection_item_id: str | None,
        invalidate_cache: bool,
        response_language: str | None,
        seed: OptionalSeed,
        should_fetch_prompts: bool,
        **kwargs
    ) -> (ParserOutput[Dict], OptionalSeed, KvpPrompts):
        machine = KeyValuePairsExtractionStateMachine.from_values(
            PromptIds.ROW_EXTRACTION,
            item_id,
            collection_item_id,
            invalidate_cache,
            response_language,
            seed,
            should_fetch_prompts,
            **kwargs
        )
        machine.run()
        return machine.llm_response, machine.seed, machine.prompts_used

    def run_synthesis_of_abstracts_for_question(
        self,
        item_ids: list[int | str],
        collection_item_ids: list[int | str],
        question: str,
        invalidate_cache: bool,
        response_language: str | None,
        seed: OptionalSeed,
        should_fetch_prompts: bool,
        **kwargs
    ) -> (ParserOutput[Dict], Dict, OptionalSeed, SynPrompt):
        machine = SynthesisStateMachine.from_values(
            PromptIds.ABSTRACT_SYNTHESIS,
            item_ids,
            collection_item_ids,
            question,
            invalidate_cache,
            response_language,
            seed,
            should_fetch_prompts,
            **kwargs
        )
        machine.run()
        prompt = machine.prompts_used.get("synthesis", None)
        return (
            machine.llm_response,
            machine.mappings,
            machine.seed,
            prompt.model_dump() if prompt else {},
        )

    def generate_raw_llm_response(self, generate_request: LLMGenerateRequest) -> Dict:
        return LLMFactory.create_llm().generate_invoke(generate_request)

    def generate_chat_llm_response(self, chat_request: LLMChatRequest) -> Dict:
        return LLMFactory.create_llm().generate_chat_invoke(chat_request)
