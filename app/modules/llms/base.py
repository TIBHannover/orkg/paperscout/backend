import logging
from abc import abstractmethod
from enum import Enum
from typing import Dict, Generic, List, Optional, TypeAlias, TypeVar

from pydantic import BaseModel, Field

from app.models.llm import LLMChatRequest, LLMGenerateRequest
from app.modules.utils import merge_dicts

IdType: TypeAlias = int | str
JSON: TypeAlias = Dict | List[Dict]
T = TypeVar("T")
logger = logging.getLogger(__name__)


class LLMResponse(BaseModel):
    """
    The response of an LLM.
    """

    response: str = Field(..., alias="response", title="The response of the LLM")
    params: Dict | None = Field(None, alias="params", title="The parameters of the LLM")


class Prompt(BaseModel):
    """
    The representation of a filled out prompt template.
    """

    id_: IdType = Field(..., alias="id", title="The ID of the prompt")
    system_prompt: Optional[str] = Field(
        None, alias="system_prompt", title="The system prompt"
    )
    user_prompt: str = Field(..., alias="user_prompt", title="The user prompt")


class BaseLLM(BaseModel):
    """
    The base representation of a LLM.
    """

    @abstractmethod
    def invoke(self, prompt: Prompt, seed: int | None) -> LLMResponse:
        raise NotImplementedError("The invoke method must be implemented!")


class ParserOutput(BaseModel, Generic[T]):
    """
    The output of a parser.
    """

    raw_response: LLMResponse | None = Field(
        ..., alias="raw_response", title="The raw response of the parser"
    )
    output: T = Field(..., alias="output", title="The output of the parser")
    llm_params: JSON | None = Field(
        None,
        alias="llm_params",
        title="The language model parameters for reproducing the output",
    )

    @property
    def is_cached(self) -> bool:
        """
        Whether the output is cached.
        :return: whether the output is cached
        """
        return self.raw_response is not None

    def __add__(self, other: "ParserOutput") -> "ParserOutput":
        """
        Merge two ParserOutputs.
        :param other: the other ParserOutput
        :return: the merged ParserOutput
        """
        # merge the outputs of the two ParserOutputs
        merged_output = merge_dicts(self.output, other.output)
        # Leave raw response empty -> to be set by the caller
        raw_response = None
        # merge the llm_params of the two ParserOutputs
        new_llm_params = [{} for _ in range(len(merged_output))]

        merged_indices = list(merged_output.keys())
        # iterate over the original llm_params and based on the index add them to the new llm_params
        own_indices = list(self.output.keys())
        for k, _ in self.output.items():
            new_llm_params[merged_indices.index(k)] = self.llm_params[
                own_indices.index(k)
            ]
        other_indices = list(other.output.keys())
        for k, _ in other.output.items():
            new_llm_params[merged_indices.index(k)] = other.llm_params[
                other_indices.index(k)
            ]

        return ParserOutput(
            raw_response=raw_response, output=merged_output, llm_params=new_llm_params
        )


class BaseOutputParser(BaseModel):
    """
    The base representation of a LLM output parser.
    """

    @abstractmethod
    def parse(self, llm_response: LLMResponse) -> ParserOutput:
        """
        Parse the output of the LLM.
        :param llm_response: the response object of the LLM
        :return: a ParserOutput object
        """
        pass

    def construct_raw_response(self, structured_output: dict) -> str:
        """
        Construct the raw response from the structured output.
        :param structured_output: the structured output
        :return: the raw response
        """
        raise NotImplementedError(
            "The construct_raw_response method must be implemented in child classes!"
        )


class PassThroughOutputParser(BaseOutputParser):
    """
    A pass-through output parser.
    """

    def parse(self, response: LLMResponse) -> ParserOutput[str]:
        return ParserOutput(
            raw_response=response, output=response.response, llm_params=response.params
        )


class PromptTemplate(BaseModel):
    """
    The base representation of a prompt.
    """

    id_: IdType = Field(..., alias="id", title="The ID of the prompt")
    system_prompt: Optional[str] = Field(
        None, alias="system_prompt", title="The system prompt"
    )
    user_prompt: str = Field(..., alias="user_prompt", title="The user prompt")

    @classmethod
    def from_id_and_user_prompt(cls, id_: IdType, user_prompt: str) -> "PromptTemplate":
        """
        Create a prompt from the ID and user prompt.
        :param id_: the ID of the prompt
        :param user_prompt: the user prompt
        :return: the prompt
        """
        return cls(id=id_, user_prompt=user_prompt)

    def format(self, **kwargs) -> "PromptTemplate":
        """
        Format the prompt.
        :param kwargs: any additional keyword arguments
        :return: the formatted prompt
        """
        formatted_template = PromptTemplate(
            id=self.id_,
            system_prompt=self.system_prompt.format(**kwargs)
            if self.system_prompt is not None
            else None,
            user_prompt=self.user_prompt.format(**kwargs),
        )
        logger.debug(f"Formatted prompt: {formatted_template}")
        return formatted_template

    def __or__(self, other: BaseLLM) -> "Chain":
        """
        Chain the prompt with an LLM.
        :param other: the LLM to chain with
        :return: the chain
        """
        return Chain(prompt_template=self, llm=other)

    def __hash__(self):
        return hash(self.id_)


class Chain(BaseModel):
    prompt_template: PromptTemplate = Field(
        ..., alias="prompt_template", title="The prompt template"
    )
    llm: BaseLLM = Field(..., alias="llm", title="The LLM to use")
    output_parser: BaseOutputParser = Field(
        PassThroughOutputParser(),
        alias="output_parser",
        title="The output parser to use",
    )

    def __or__(self, other: BaseOutputParser) -> "Chain":
        if isinstance(other, BaseOutputParser):
            self.output_parser = other
        return self

    def __call__(self, **kwargs):
        return self.invoke(**kwargs)

    def invoke(self, **kwargs) -> ParserOutput:
        """
        Starts the chain
        """
        # Format the prompt template
        self.prompt_template = self.prompt_template.format(**kwargs)
        # Get the seed if it exists
        seed = kwargs.get("seed", None)
        # Start the chain
        return self.output_parser.parse(  # Parse the output
            self.llm.invoke(  # Invoke the LLM
                Prompt(  # Fill out the prompt
                    id=self.prompt_template.id_,
                    system_prompt=self.prompt_template.system_prompt,
                    user_prompt=self.prompt_template.user_prompt,
                ),
                seed=seed,
            )
        )


class LLMType(Enum):
    """
    The type of the LLM.
    """

    LOCAL = "local"
    REMOTE = "remote"


class LLM(BaseLLM):
    name: str = Field(..., alias="name", title="The name of the LLM")
    llm_type: LLMType = Field(..., alias="llm_type", title="The type of the LLM")

    def invoke(self, prompt: Prompt, seed: int | None = None) -> LLMResponse:
        raise NotImplementedError("The invoke method must be implemented!")

    def generate_invoke(self, generate_request: LLMGenerateRequest) -> Dict:
        raise NotImplementedError("The raw_invoke method must be implemented!")

    def generate_chat_invoke(self, chat_request: LLMChatRequest) -> Dict:
        raise NotImplementedError("The raw_invoke method must be implemented!")
