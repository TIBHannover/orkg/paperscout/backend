from typing import Dict, List, Type

from pocketbase.models.utils import ListResult
from pydantic import BaseModel, Field


class PocketBaseListResult(ListResult):
    def as_pydantic(self, model: Type[BaseModel]) -> List:
        return [model.model_validate(item.__dict__) for item in self.items]

    @property
    def has_items(self) -> bool:
        """
        Check if there are items in the list.
        :return: True if there are items, False otherwise
        """
        return bool(self.items)


class Item(BaseModel):
    item_id: str = Field(..., description="The CoreID of the item")
    prompt_id: int = Field(..., description="The ID of the prompt")
    parameter: str = Field(..., description="The parameter of the item")
    llm_response: dict = Field(..., description="The LLM response")
    is_collection_item: bool = Field(
        ..., description="Whether the item is a collection item"
    )
    generation_parameters: dict = Field(
        ..., description="The generation parameters of the item"
    )
    internal_id: str | None = Field(
        None, description="The internal ID of the item", alias="id"
    )


class UsedPrompt(BaseModel):
    system: str = Field(..., description="The system that used the prompt")
    user: str = Field(..., description="The user that used the prompt")
    variables: Dict = Field(..., description="The variables used with the prompt")
    hash_code: str = Field(..., description="The hash code of the prompt")
    produced: List[str] = Field(..., description="The produced items by this prompt")


class CollectionItem(BaseModel):
    id_: str = Field(
        ..., alias="id", description="The Pocketbase ID of the collection item"
    )
    linked_item_id: str | None = Field(
        None, description="An optional CoreID for the item"
    )
    csl_data: Dict = Field(..., description="The CSL data for the item")


class QuestionRecord(BaseModel):
    question: str = Field(..., description="The question")
    frequency: int = Field(1, description="The frequency of the question")
    nsfw: bool = Field(False, description="Whether the question is NSFW")
