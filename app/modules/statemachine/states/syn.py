import logging
from typing import Dict

from app.errors import ItemNotFoundError
from app.modules.caching.manager import CacheManager
from app.modules.indexing.vectordb.store import QdrantStore
from app.modules.llms.base import ParserOutput
from app.modules.llms.factories import LLMFactory
from app.modules.prompting.prompts import Prompts
from app.modules.statemachine.states.base import SynLLMState

logger = logging.getLogger(__name__)


class CheckSynInCacheState(SynLLMState):
    def run(self):
        cache_item, prompts_used = CacheManager().check_if_abstract_synthesis_in_cache(
            prompt_id=self.machine.prompt_id,
            item_ids=self.machine.item_ids + self.machine.collection_item_ids,
            question=self.machine.question,
            language=self.machine.translation_language,
            seed=self.machine.seed,
            should_fetch_prompts=self.machine.should_fetch_prompts,
        )
        if cache_item:
            logger.debug(f"Found item in cache: {cache_item}")
            self.machine.llm_response = ParserOutput[Dict](
                raw_response=None,
                output={"synthesis": cache_item.llm_response.pop("synthesis")},
                llm_params=cache_item.generation_parameters,
            )
            self.machine.mappings = cache_item.llm_response
            self.machine.in_cache = True
            self.machine.prompts_used = prompts_used
        else:
            logger.debug("Item not found in cache.")
            self.machine.in_cache = False


class GetItemsContentState(SynLLMState):
    def run(self):
        abstracts = []
        counter = 1
        # Custom collection IDs have priority over regular item IDs
        if self.machine.collection_item_ids:
            items = [
                CacheManager().get_collection_item_from_cache(custom_item_id)
                for custom_item_id in self.machine.collection_item_ids
            ]
            for item in items:
                abstract = item.csl_data.get("abstract", "")
                if abstract:
                    abstracts.append(abstract)
                    self.machine.mappings["collection"][counter] = item.id_
                    counter += 1

        # If there is still some item IDs left, fetch them from the store
        if self.machine.item_ids:
            qdrant_items = []
            for item_id in self.machine.item_ids:
                store_response = QdrantStore().get(doc_id=item_id)
                if len(store_response.items) == 0:
                    raise ItemNotFoundError(
                        f"Item with ID {self.machine.item_ids} not found!"
                    )
                qdrant_items.append(store_response.items[0])
            for item in [item for item in qdrant_items]:
                abstracts.append(item.abstract)
                self.machine.mappings["core"][counter] = item.id
                counter += 1

        # Set the abstracts for the machine
        self.machine.abstracts = abstracts


class MakeSynLLMChainState(SynLLMState):
    @staticmethod
    def _combine_abstracts_into_string(abstracts: list[str]) -> str:
        resulting_string = ""
        for idx, abstract in enumerate(abstracts):
            resulting_string += f"Abstract {idx + 1}:\n {abstract}\n\n"
        return resulting_string.strip()

    def run(self):
        chain = (
            self.machine.template
            | LLMFactory.create_llm()
            | Prompts.get_parser(self.machine.template)
        )
        # Set the content of the paper for the user prompt
        self.machine.user_kwargs["abstracts"] = self._combine_abstracts_into_string(
            self.machine.abstracts
        )
        self.machine.user_kwargs["question"] = self.machine.question
        self.machine.chain = chain


class RunSynLLMChainState(SynLLMState):
    def run(self):
        kwargs = self.machine.user_kwargs
        if self.machine.seed is not None:
            kwargs["seed"] = self.machine.seed
        output = self.machine.chain(**kwargs)
        self.machine.llm_response = output


class AddSynResponseToCacheState(SynLLMState):
    def run(self):
        cached_item = CacheManager().add_abstract_synthesis_to_cache(
            prompt_id=self.machine.prompt_id.value,
            item_ids=self.machine.item_ids + self.machine.collection_item_ids,
            question=self.machine.question,
            llm_response=self.machine.llm_response,
            mappings=self.machine.mappings,
            language=self.machine.translation_language,
            seed=self.machine.seed,
        )

        # Add prompt used to cache
        self.machine.prompts_used = {
            "synthesis": CacheManager().add_prompt_to_cache(
                prompt_id=self.machine.prompt_id,
                cached_items_ids=[cached_item.internal_id],
                variables=self.machine.user_kwargs,
            )
        }


class TranslateSynthesisResponseState(SynLLMState):
    def run(self):
        chain = (
            Prompts.TRANSLATE_SYN_RESPONSE
            | LLMFactory.create_llm()
            | Prompts.get_parser(Prompts.TRANSLATE_SYN_RESPONSE)
        )
        kwargs = {
            "synthesis": self.machine.llm_response.output["synthesis"],
            "language": self.machine.translation_language,
        }
        self.machine.llm_response = chain(**kwargs)
        logger.debug(
            f"Translated {self.machine.llm_response.raw_response.response[:30]}"
            f"... to {self.machine.translation_language}"
        )
