from typing import List

from pydantic import BaseModel, Field

from app.models import TimeStampedResponse
from app.modules.caching.models import QuestionRecord


class BackendVersionResponse(TimeStampedResponse):
    class Payload(BaseModel):
        version: str = Field(..., description="The version of the backend service")

    payload: Payload


class VectorConfig(BaseModel):
    """
    The configuration of the vectors in the vector store.
    """

    vector_size: int = Field(
        ..., description="The size of the vectors in the vector store"
    )
    distance_method: str = Field(
        ..., description="The method used to calculate the distance between vectors"
    )


class IndexingStats(BaseModel):
    """
    The indexing statistics of the system.
    """

    num_indexed_vectors: int = Field(
        ..., description="The number of indexed vectors in the vector store"
    )
    vector_config: VectorConfig = Field(
        ..., description="The configuration of the vectors in the vector store"
    )

    @classmethod
    def from_values(
        cls, num_indexed_vectors: int, vector_size: int, distance_method: str
    ):
        return cls(
            num_indexed_vectors=num_indexed_vectors,
            vector_config=VectorConfig(
                vector_size=vector_size, distance_method=distance_method
            ),
        )


class DatasetStats(BaseModel):
    """
    The dataset statistics of the system.
    """

    num_items_with_authors: int | None = Field(
        None, description="The number of items with authors"
    )
    num_items_with_abstracts: int | None = Field(
        None, description="The number of items with abstracts"
    )
    num_items_with_languages: int | None = Field(
        None, description="The number of items with language tags"
    )
    num_items_with_topics: int | None = Field(
        None, description="The number of items with topics"
    )
    num_items_with_subjects: int | None = Field(
        None, description="The number of items with subjects"
    )
    num_items_with_year: int | None = Field(
        None, description="The number of items with year values"
    )
    num_items_with_identifiers: int | None = Field(
        None, description="The number of items with identifiers"
    )
    num_items_with_type: int | None = Field(
        None, description="The number of items with a specific document type"
    )
    num_items_with_issn: int | None = Field(
        None, description="The number of items with an ISSN"
    )
    num_items_with_doi: int | None = Field(
        None, description="The number of items with a DOI"
    )
    num_items_with_citations: int | None = Field(
        None, description="The number of items with citation counts"
    )
    num_items_with_publication_date: int | None = Field(
        None, description="The number of items with publication dates"
    )
    num_items_with_publisher: int | None = Field(
        None, description="The number of items with publishers"
    )
    num_items_with_journal: int | None = Field(
        None, description="The number of items with journal names"
    )
    num_items_with_dbpedia_entities: int | None = Field(
        None, description="The number of items with DBpedia entities"
    )

    class Builder:
        def __init__(self):
            self._num_items_with_authors = None
            self._num_items_with_abstracts = None
            self._num_items_with_languages = None
            self._num_items_with_topics = None
            self._num_items_with_subjects = None
            self._num_items_with_year = None
            self._num_items_with_identifiers = None
            self._num_items_with_type = None
            self._num_items_with_issn = None
            self._num_items_with_doi = None
            self._num_items_with_citations = None
            self._num_items_with_publication_date = None
            self._num_items_with_publisher = None
            self._num_items_with_journal = None
            self._num_items_with_dbpedia_entities = None

        def with_num_items_with_dbpedia_entities(
            self, num_items_with_dbpedia_entities: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_dbpedia_entities = num_items_with_dbpedia_entities
            return self

        def with_num_items_with_authors(
            self, num_items_with_authors: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_authors = num_items_with_authors
            return self

        def with_num_items_with_abstracts(
            self, num_items_with_abstracts: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_abstracts = num_items_with_abstracts
            return self

        def with_num_items_with_languages(
            self, num_items_with_languages: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_languages = num_items_with_languages
            return self

        def with_num_items_with_topics(
            self, num_items_with_topics: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_topics = num_items_with_topics
            return self

        def with_num_items_with_subjects(
            self, num_items_with_subjects: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_subjects = num_items_with_subjects
            return self

        def with_num_items_with_year(
            self, num_items_with_year: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_year = num_items_with_year
            return self

        def with_num_items_with_identifiers(
            self, num_items_with_identifiers: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_identifiers = num_items_with_identifiers
            return self

        def with_num_items_with_type(
            self, num_items_with_type: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_type = num_items_with_type
            return self

        def with_num_items_with_issn(
            self, num_items_with_issn: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_issn = num_items_with_issn
            return self

        def with_num_items_with_doi(
            self, num_items_with_doi: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_doi = num_items_with_doi
            return self

        def with_num_items_with_citations(
            self, num_items_with_citations: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_citations = num_items_with_citations
            return self

        def with_num_items_with_publication_date(
            self, num_items_with_publication_date: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_publication_date = num_items_with_publication_date
            return self

        def with_num_items_with_publisher(
            self, num_items_with_publisher: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_publisher = num_items_with_publisher
            return self

        def with_num_items_with_journal(
            self, num_items_with_journal: int
        ) -> "DatasetStats.Builder":
            self._num_items_with_journal = num_items_with_journal
            return self

        def build(self) -> "DatasetStats":
            return DatasetStats(
                num_items_with_authors=self._num_items_with_authors,
                num_items_with_abstracts=self._num_items_with_abstracts,
                num_items_with_languages=self._num_items_with_languages,
                num_items_with_topics=self._num_items_with_topics,
                num_items_with_subjects=self._num_items_with_subjects,
                num_items_with_year=self._num_items_with_year,
                num_items_with_identifiers=self._num_items_with_identifiers,
                num_items_with_type=self._num_items_with_type,
                num_items_with_issn=self._num_items_with_issn,
                num_items_with_doi=self._num_items_with_doi,
                num_items_with_citations=self._num_items_with_citations,
                num_items_with_publication_date=self._num_items_with_publication_date,
                num_items_with_publisher=self._num_items_with_publisher,
                num_items_with_journal=self._num_items_with_journal,
                num_items_with_dbpedia_entities=self._num_items_with_dbpedia_entities,
            )


class UsageStats(BaseModel):
    """
    The usage statistics of the system.
    """

    num_registered_users: int = Field(
        ..., description="The number of registered users in the system"
    )
    num_cache_hits: int = Field(
        ..., description="The number of cache hits in the system"
    )
    num_collections: int = Field(
        ..., description="The number of user-created collections in the system"
    )
    num_collection_items: int = Field(
        ..., description="The number of items in bibtex imported items to the system"
    )
    num_saved_searches: int = Field(
        ..., description="The number of saved searches in the system"
    )
    num_shared_links: int = Field(
        ..., description="The number of shared links in the system"
    )
    num_questions_asked: int = Field(
        ...,
        description="Approximate number of non-unique questions asked to the system",
    )


class StatisticsResponse(TimeStampedResponse):
    class Payload(BaseModel):
        indexing: IndexingStats
        dataset: DatasetStats
        usage: UsageStats

    payload: Payload


class QuestionsResponse(TimeStampedResponse):
    class Payload(BaseModel):
        questions: List[QuestionRecord]

    payload: Payload


class BackendHealthResponse(TimeStampedResponse):
    class Payload(BaseModel):
        message: str = Field(
            ..., description="The health status of the backend service"
        )
        code: int = Field(..., description="The status code of the health check")
        # Other fields like data can be added for detailed health check response

        @classmethod
        def a_ok(cls):
            return cls(message="Backend is healthy.", code=200)

    payload: Payload
