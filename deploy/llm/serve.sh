#!/bin/bash

# Extract model name from command-line arguments
MODEL_NAME=$1

if [ -z "$MODEL_NAME" ]; then
    echo "Please provide a model name as a command-line argument."
    exit 1
fi

# Serve OLLaMA
/bin/ollama serve &

# Sleep a bit to make sure it started
sleep 5

# Display model
echo "Model $MODEL_NAME is being loaded..."

# Pull model
/bin/ollama pull $MODEL_NAME

# Run model
/bin/ollama run $MODEL_NAME

# Keep the container running
sleep infinity
