import json
import random
from random import randint

import click
import openai
from tqdm import tqdm

random.seed(42)

LANGUAGES = [
    "Spanish",
    "French",
    "German",
    "Italian",
    "Dutch",
    "Portuguese",
    "Russian",
    "Chinese",
    "Japanese",
    "Korean",
    "Arabic",
    "Farsi",
]

TONES = {
    "Child": "The language is at a middle school aged child's level (10-11 years old). It is simple and easy to understand.",
    "Teenager": "The language is at a (high school) teenager's level. It is more complex than a child's level but still easy to understand.",
    "Adult": "The language is at an adult's level. It is complex and contains few (if any) technical terms.",
}


def get_some_languages(count: int) -> list[str]:
    if count > len(LANGUAGES):
        return LANGUAGES
    if count == 1:
        return ["English"]
    return ["English"] + random.sample(LANGUAGES, count - 1)


def get_some_tones(count: int, add_researcher: bool = True) -> list[dict]:
    if count > len(TONES):
        return [{"tone": tone, "description": desc} for tone, desc in TONES.items()]
    tones = random.sample(list(TONES.items()), count)
    if add_researcher:
        tones.append(
            (
                "Researcher",
                "The language is at a researcher's level. It is highly complex and contains technical terms.",
            )
        )
    return [{"tone": tone, "description": desc} for tone, desc in tones]


@click.command()
@click.option("--api-key", "-k", help="OpenAI API key")
@click.option("--model", "-m", default="gpt-4o-mini", help="OpenAI model name")
@click.option(
    "--input-file",
    "-i",
    help="Input datasets in json file as created by the fetching.py script",
)
@click.option(
    "--output-file",
    "-o",
    help="Output file to store the generated data in jsonl format",
)
def create_synthesis(api_key: str, model: str, input_file: str, output_file: str):
    client = openai.OpenAI(api_key=api_key)
    prompt = "You are a scientific research bot. Your task is to create a synthesis of the provided scholarly abstracts based on a research question."
    synthesis_usecase_prompt = """You will generate a comprehensive answer to the given research question (but no more than three/four sentences)
solely based on the content provided.
Cite the number of the content referenced for each claim like this:
[1] for a single reference or [2][3] for multiple references.
Emphasize brevity, focusing on essential details and omitting unnecessary information.
Avoid adding the question in the answer and Do not include any notes or comments
Your synthesis should be in the "{}" language. And the tone of the language should be for a "{}" audience ({}). """
    abstracts_to_synthesize = "# Research Question: {}\n# Abstracts:{}\n\n# Answer with inline-citations as [#]:\n"

    with open(input_file, "r") as f:
        data = json.load(f)
        with open(output_file, "a+", encoding="utf8") as jsonl:
            for item in tqdm(data, unit="question", desc="Generating synthesis data"):
                question = item["question"]
                documents = item["documents"][:5]
                abstracts = "\n\n".join(
                    [
                        f"Abstract #{i+1}:\n {doc['abstract']}"
                        for i, doc in enumerate(documents)
                    ]
                )
                languages = get_some_languages(randint(3, 5))
                for language in tqdm(languages, unit="language", leave=False):
                    for tone in get_some_tones(randint(1, 2)):
                        response = client.chat.completions.create(
                            model=model,
                            messages=[
                                {"role": "system", "content": prompt},
                                {
                                    "role": "system",
                                    "content": synthesis_usecase_prompt.format(
                                        language, tone["tone"], tone["description"]
                                    ),
                                },
                                {
                                    "role": "user",
                                    "content": abstracts_to_synthesize.format(
                                        question, abstracts
                                    ),
                                },
                            ],
                        )
                        synthesis = response.choices[0].message.content
                        jsonl.write(
                            json.dumps(
                                {
                                    "question": question,
                                    "documents": documents,
                                    "language": language,
                                    "synthesis": synthesis,
                                    "level": tone["tone"],
                                },
                                ensure_ascii=False,
                            )
                            + "\n"
                        )


if __name__ == "__main__":
    create_synthesis()
