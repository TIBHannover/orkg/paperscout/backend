import unittest

from app.modules.indexing.base import FilterOperator, FilterType, KeyValueFilter
from app.modules.indexing.vectordb.models import QdrantFilter
from app.modules.indexing.vectordb.store import QdrantStore
from tests import create_document_object


class TestQdrantStore(unittest.TestCase):
    store: QdrantStore = QdrantStore(in_memory=True)

    def setUp(self):
        # reinitialize the store
        self.store._reinitialize()

    def test_correct_initialization(self):
        assert self.store is not None, "The store should be initialized"
        assert self.store.count() == 0, "The store should be empty"

    def test_adding_document_to_index(self):
        document = create_document_object(
            id="1",
            title="Test document",
            abstract="This is a test document",
            year=2021,
        )
        self.store.index(document)
        assert self.store.count() == 1, "The store should have one document"

    def _add_dummy_documents(self):
        self.store.index(
            create_document_object(
                id=1,
                title="Lady gaga concert in italy",
                abstract="loud music",
                year=2021,
            )
        )
        self.store.index(
            create_document_object(
                id=2,
                title="GTA 6 gameplay footage was leaked",
                abstract="gaming",
                year=2022,
            )
        )
        self.store.index(
            create_document_object(
                id=3,
                title="Earth is a big blue planet",
                abstract="mother nature",
                year=2020,
            )
        )

    def test_semantic_search(self):
        # Add dummy documents
        self._add_dummy_documents()
        assert self.store.count() == 3, "The store should have three documents"

        # Search for music related documents -> doc(1)
        results = self.store.search("Open air music festival", limit=1)
        assert len(results.items) == 1, "The results should only have one document"
        assert results.items[0].id == 1, "The document should have id 1"

        # Search for gaming related documents -> doc(2)
        results = self.store.search("The new XBOX has a massive GPU", limit=1)
        assert results.items[0].id == 2, "The document should have id 2"

        # Search for nature related documents -> doc(3)
        results = self.store.search("Elon Musk wants to colonize Mars", limit=1)
        assert results.items[0].id == 3, "The document should have id 3"

    def test_retrieve_documents(self):
        # Add dummy documents
        self._add_dummy_documents()
        assert self.store.count() == 3, "The store should have three documents"

        # Retrieve document with int id 1
        retrieved_documents = self.store.get(doc_id=1, limit=1)
        assert (
            len(retrieved_documents.items) == 1
        ), "The results should only have one document"
        assert retrieved_documents.items[0].id == 1, "The document should have id 1"
        assert (
            retrieved_documents.items[0].title == "Lady gaga concert in italy"
        ), "Incorrect title"
        assert (
            retrieved_documents.items[0].abstract == "loud music"
        ), "Incorrect abstract"
        assert retrieved_documents.items[0].year == 2021, "Incorrect year"

        # Retrieve document with str id 1
        retrieved_documents = self.store.get(doc_id="1", limit=1)
        assert len(retrieved_documents.items) == 0, "The results should be empty"

        # Retrieve document with year
        retrieved_documents = self.store.get(
            criteria=QdrantFilter(
                on=[
                    KeyValueFilter(
                        key="year",
                        value=2020,
                        filter_type=FilterType.MUST,
                        operator=FilterOperator.EQUALS,
                    )
                ]
            ),
            limit=1,
        )
        assert (
            len(retrieved_documents.items) == 1
        ), "The results should only have one document"
        assert retrieved_documents.items[0].id == 3, "The document should have id 3"
        assert (
            retrieved_documents.items[0].title == "Earth is a big blue planet"
        ), "Incorrect title"
        assert (
            retrieved_documents.items[0].abstract == "mother nature"
        ), "Incorrect abstract"
        assert retrieved_documents.items[0].year == 2020, "Incorrect year"

    def test_delete_documents(self):
        # Add dummy documents
        self._add_dummy_documents()
        assert self.store.count() == 3, "The store should have three documents"

        # Delete document with int id 1
        self.store.delete(doc_id=1)
        assert self.store.count() == 2, "The store should have two documents"

        # Delete document with year
        self.store.delete(
            criteria=QdrantFilter(
                on=[
                    KeyValueFilter(
                        key="year",
                        value=2020,
                        filter_type=FilterType.MUST,
                        operator=FilterOperator.EQUALS,
                    )
                ]
            )
        )
        assert self.store.count() == 1, "The store should have one document"
