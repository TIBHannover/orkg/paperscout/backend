from qdrant_client import models

from app.modules.indexing.vectordb.filtering import is_valid_filter_string, parser


class FilterString(str):
    """
    A string that can be converted to a Qdrant filter object.
    Should be used by the API for parsing filter strings.
    """

    def is_valid_filter(self) -> bool:
        """
        Check if the string is a valid filter string according to the grammar.
        :return: True if the string is a valid filter string, False otherwise.
        """
        return is_valid_filter_string(self)

    def to_filter(self) -> models.Filter:
        """
        Convert the filter string to a Qdrant filter object.
        :return: The Qdrant filter object.
        """
        return parser.parse(self)
