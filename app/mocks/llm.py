from app.modules.llms.base import BaseLLM, LLMResponse, Prompt


class DummyOllamaLLM(BaseLLM):
    def invoke(self, prompt: Prompt | str, seed: int | None = None) -> LLMResponse:
        return LLMResponse(response=prompt, params={})


class DummyLLM(DummyOllamaLLM):
    def invoke(self, prompt: Prompt, seed: int | None = None) -> LLMResponse:
        return super().invoke(
            "Methods: LLM, AI, ML\nMetrics: F1, Recall, Precision\nDatasets: MNIST, CIFAR-10, ImageNet\n",
            seed,
        )
