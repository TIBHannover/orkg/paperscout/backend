from fastapi import HTTPException


def create_404_document_not_found(document_id: str) -> HTTPException:
    """
    Create a 404 error for document not found.
    :param document_id: The document ID.
    :return: The HTTPException.
    """
    raise HTTPException(
        status_code=404, detail=f"Document with ID {document_id} not found"
    )


def create_422_unprocessable_entity(detail: str) -> HTTPException:
    """
    Create a 422 error for unprocessable entity.
    :param detail: The detail of the error.
    :return: The HTTPException.
    """
    raise HTTPException(status_code=422, detail=detail)
