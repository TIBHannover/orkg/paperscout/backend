from app.modules.prompting.prompts import Prompts
from app.modules.statemachine.states.base import LLMState


class FetchPromptState(LLMState):
    def run(self):
        self.machine.template = Prompts.map(self.machine.prompt_id)
