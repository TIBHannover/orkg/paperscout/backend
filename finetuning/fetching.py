# This script fetches questions from Ask, checks they are actually questions
# and then collects a list of documents that are relevant to the question.

import json
import time
from typing import List

import click
import requests
from datasets import Dataset
from pocketbase import PocketBase
from pocketbase.models.utils import ListResult
from tqdm import tqdm
from transformers import pipeline
from transformers.pipelines.pt_utils import KeyDataset


def recursively_collect_content(
    pocket: PocketBase, collection_name: str, sleep_time: float
) -> List:
    records: List = []
    per_page = 100
    # Starting with page 1
    results: ListResult = pocket.collection(collection_name).get_list(
        page=1, per_page=per_page
    )
    records.extend([record.question for record in results.items])
    for page in tqdm(
        range(2, results.total_pages + 2), unit="page", desc="Collecting content"
    ):
        results = pocket.collection(collection_name).get_list(
            page=page, per_page=per_page
        )
        records.extend([record.question for record in results.items])
        time.sleep(sleep_time)
    return records


def classify_questions(
    questions: List,
    cls_model: str,
    cls_threshold: float,
    model_device: str,
    batch_size: int,
) -> List:
    classified_questions = []
    # Create pipeline
    classifier = pipeline(
        "zero-shot-classification",
        model=cls_model,
        device=model_device,
        batch_size=batch_size,
    )
    # Load into datasets to parallize the processing
    dataset = Dataset.from_dict({"train": questions})
    dataset = KeyDataset(dataset["train"], "question")
    for verdict in classifier(dataset, ["Question", "Statement"]):
        if verdict["scores"][0] > cls_threshold:
            classified_questions.append(verdict["sequence"])
    return classified_questions


def get_relevant_documents_via_ask(questions: List, ask_host: str) -> List:
    relevant_documents = []
    for question in tqdm(questions, unit="question", desc="Fetching ASK documents"):
        documents = []
        # Fetch documents from ASK
        ask_response = requests.get(
            f"{ask_host}/index/search", params={"query": question, "limit": 10}
        ).json()
        for item in ask_response["payload"]["items"]:
            documents.append(
                {
                    "title": item["title"],
                    "abstract": item["abstract"],
                    "full_text": item["full_text"],
                }
            )
        relevant_documents.append({"question": question, "documents": documents})
    return relevant_documents


@click.command()
@click.option(
    "--pocket-base", "-pb", default="https://pb.ask.orkg.org", help="PocketBase URL"
)
@click.option(
    "--collection-name",
    "-cn",
    default="questions",
    help="Collection name in pocketbase",
)
@click.option("--admin-email", "-ae")
@click.option("--admin-password", "-ap")
@click.option("--sleep-time", "-st", default=1.5, help="Sleep time between PB requests")
@click.option(
    "--cls-model",
    "-cm",
    default="facebook/bart-large-mnli",
    help="Classification model name",
)
@click.option("--cls-threshold", "-th", default=0.9, help="Classification threshold")
@click.option("--model-device", "-md", default="cuda", help="Model device")
@click.option("--batch-size", "-bs", default=8, help="Batch size")
@click.option(
    "--ask-host", "-h", default="https://api.ask.orkg.org", help="ORKG Ask host"
)
@click.option("--output", "-o", default="ask-dataset.json", help="Output file")
def start_collection(
    pocket_base: str,
    collection_name: str,
    admin_email: str,
    admin_password: str,
    sleep_time: float,
    cls_model: str,
    cls_threshold: float,
    model_device: str,
    batch_size: int,
    ask_host: str,
    output: str,
):
    pb = PocketBase(pocket_base)
    pb.admins.auth_with_password(email=admin_email, password=admin_password)
    questions = recursively_collect_content(pb, collection_name, sleep_time)
    print(f"Found {len(questions)} questions in collection {collection_name}")
    relevant_questions = classify_questions(
        questions, cls_model, cls_threshold, model_device, batch_size
    )
    print(f"Found {len(relevant_questions)} relevant questions")
    relevant_documents = get_relevant_documents_via_ask(relevant_questions, ask_host)
    print("Found relevant documents for all questions")
    with open(output, "w") as f:
        json.dump(relevant_documents, f)


if __name__ == "__main__":
    start_collection()
