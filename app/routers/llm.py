import http
from typing import Annotated, List, Optional
from urllib.parse import unquote

from fastapi import APIRouter, Query

from app.core.decorators import log
from app.errors import IncorrectAPIInvocationError
from app.models.llm import (
    ExtractItemValuesFromPropertiesRequest,
    ExtractItemValuesFromPropertiesResponse,
    LLMChatRequest,
    LLMGenerateRequest,
    LLMRawResponse,
    SynthesisAnswerOfQuestionFromAbstractsResponse,
)
from app.routers.common import ResponseWrapper, allowed_translation_languages
from app.services import llm

router = APIRouter(prefix="/llm", tags=["Large Language Models"])


@router.get(
    "/extract/item/values",
    status_code=http.HTTPStatus.OK,
    response_model=ExtractItemValuesFromPropertiesResponse,
)
@log(__name__)
def extract_item_values(
    properties: Annotated[
        list[str],
        Query(
            description="The properties to extract from the item",
        ),
    ],
    item_id: Annotated[
        Optional[int | str],
        Query(
            description="The item ID to extract the values from",
        ),
    ] = None,
    collection_item_id: Annotated[
        Optional[str],
        Query(
            description="The custom collection item ID to extract the values from",
        ),
    ] = None,
    invalidate_cache: Annotated[
        bool,
        Query(
            description="Whether to invalidate the cache for the item",
        ),
    ] = False,
    response_language: Annotated[
        str | None,
        Query(
            description="The language of the response. Should be one of the following: en, de, es, fr, it, pt, nl, "
            "ru, ja, ar, fa, tr, ko, zh, hi, vi, id",
        ),
    ] = None,
    seed: Annotated[
        int | None,
        Query(
            description="The seed to use during extraction",
        ),
    ] = None,
    reproducibility: Annotated[
        bool,
        Query(
            description="Whether to include the generation parameters and prompts for the response",
        ),
    ] = False,
) -> ExtractItemValuesFromPropertiesResponse:
    """
    Extracts the properties with their corresponding values from the given item.
    """
    # Defensive checks
    if not collection_item_id and not item_id:
        raise IncorrectAPIInvocationError(
            "Either item_id or collection_item_id must be provided"
        )
    if collection_item_id and item_id:
        raise IncorrectAPIInvocationError(
            "Only one of item_id or collection_item_id must be provided"
        )
    if response_language and response_language not in allowed_translation_languages:
        raise IncorrectAPIInvocationError(
            f"If the response_language is provided it must be either {', '.join(allowed_translation_languages.keys())}"
        )

    if response_language and response_language == "en":
        # English is the default language, so we don't need to specify it
        response_language = None

    # URL decode the item ID
    if item_id:
        item_id = unquote(item_id)

    # Call the service
    return ResponseWrapper.wrap_response(
        payload=llm.extract_values_for_properties_of_item(
            request=ExtractItemValuesFromPropertiesRequest(
                item_id=item_id,
                collection_item_id=collection_item_id,
                properties=properties,
                invalidate_cache=invalidate_cache,
                response_language=allowed_translation_languages[response_language]
                if response_language
                else None,
                seed=seed,
            ),
            include_reproducibility=reproducibility,
        ),
        type_=ExtractItemValuesFromPropertiesResponse,
    )


@router.get(
    "/synthesize/items/abstracts",
    status_code=http.HTTPStatus.OK,
    response_model=SynthesisAnswerOfQuestionFromAbstractsResponse,
)
@log(__name__)
def synthesize_abstracts_for_question(
    question: Annotated[
        str,
        Query(
            description="The question to synthesize the abstracts for",
        ),
    ],
    item_ids: Annotated[
        List[int | str],
        Query(
            description="The item ID to use during synthesis",
        ),
    ] = None,
    collection_item_ids: Annotated[
        List[int | str],
        Query(
            description="The custom collection item IDs to use during synthesis",
        ),
    ] = None,
    invalidate_cache: Annotated[
        bool,
        Query(
            description="Whether to invalidate the cache for the item",
        ),
    ] = False,
    response_language: Annotated[
        str | None,
        Query(
            description="The language of the response. Should be one of the following: en, de, es, fr, it, pt, nl, "
            "ru, ja, ar, fa, tr, ko, zh, hi, vi, id",
        ),
    ] = None,
    seed: Annotated[
        int | None,
        Query(
            description="The seed to use during extraction",
        ),
    ] = None,
    reproducibility: Annotated[
        bool,
        Query(
            description="Whether to include the generation parameters and prompts for the response",
        ),
    ] = False,
):
    """
    Synthesizes a citable answer for a given research question from the abstracts of the given items.
    """
    # Defensive checks
    if not item_ids and not collection_item_ids:
        raise IncorrectAPIInvocationError(
            "Either item_ids or collection_item_ids must be provided"
        )
    if response_language and response_language not in allowed_translation_languages:
        raise IncorrectAPIInvocationError(
            f"If the response_language is provided it must be either {', '.join(allowed_translation_languages.keys())}"
        )

    if response_language and response_language == "en":
        # English is the default language, so we don't need to specify it
        response_language = None

    # URL decode the item IDs
    if item_ids:
        item_ids = [unquote(item_id) for item_id in item_ids]

    # Call the service
    return ResponseWrapper.wrap_response(
        payload=llm.synthesize_abstracts_for_question(
            item_ids=item_ids,
            collection_item_ids=collection_item_ids,
            question=question,
            invalidate_cache=invalidate_cache,
            response_language=allowed_translation_languages[response_language]
            if response_language
            else None,
            seed=seed,
            include_reproducibility=reproducibility,
        ),
        type_=SynthesisAnswerOfQuestionFromAbstractsResponse,
    )


@router.post(
    "/generate",
    status_code=http.HTTPStatus.OK,
    response_model=LLMRawResponse,
)
@log(__name__)
def generate_llm(request: LLMGenerateRequest):
    return ResponseWrapper.wrap_response(
        payload=llm.generate_llm_response(request=request),
        type_=LLMRawResponse,
    )


@router.post(
    "/chat/completions",
    status_code=http.HTTPStatus.OK,
    response_model=LLMRawResponse,
)
@log(__name__)
def chat_completions_llm(request: LLMChatRequest):
    return ResponseWrapper.wrap_response(
        payload=llm.generate_chat_llm_response(request=request),
        type_=LLMRawResponse,
    )
