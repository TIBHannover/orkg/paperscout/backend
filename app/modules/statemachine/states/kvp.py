from logging import getLogger

from app.errors import ItemNotFoundError
from app.modules.caching.manager import CacheManager
from app.modules.indexing.vectordb.store import QdrantStore
from app.modules.llms.factories import LLMFactory
from app.modules.prompting.prompts import Prompts
from app.modules.statemachine.states.base import KVPLLMState

logger = getLogger(__name__)


class GetKVPPaperContentState(KVPLLMState):
    def run(self):
        if self.machine.item_id:
            content, item_id = self._get_item_content_from_vector_store()
        else:
            content, item_id = self._get_item_content_from_cache()

        self.machine.paper_content = {
            "id": item_id,
            "content": content.strip(),
        }

    def _get_item_content_from_cache(self):
        collection_item = CacheManager().get_collection_item_from_cache(
            self.machine.collection_item_id
        )
        item_id = self.machine.collection_item_id
        # TODO: CLS doesn't have a full text field
        content = (
            collection_item.csl_data.get("title", "")
            + "\n"
            + collection_item.csl_data.get("abstract", "")
            + "\n"
            + collection_item.csl_data.get("full_text", "")
        )
        return content, item_id

    def _get_item_content_from_vector_store(self):
        store_response = QdrantStore().get(doc_id=self.machine.item_id)
        if len(store_response.items) == 0:
            logger.error(f"Item with ID {self.machine.item_id} not found")
            raise ItemNotFoundError(f"Item with ID {self.machine.item_id} not found")
        paper_content = store_response.items[0]
        item_id = self.machine.item_id
        content = paper_content.title + " " + paper_content.abstract
        if paper_content.full_text:
            content += " " + paper_content.full_text
        return content, item_id


class MakeKVPLLMChainState(KVPLLMState):
    def run(self):
        chain = (
            self.machine.template
            | LLMFactory.create_llm()
            | Prompts.get_parser(self.machine.template)
        )
        # Set the content of the paper for the user prompt
        self.machine.user_kwargs["content"] = self.machine.paper_content["content"]
        self.machine.chain = chain


class RunKVPLLMChainState(KVPLLMState):
    def run(self):
        kwargs = self.machine.user_kwargs
        if "cache" in self.machine.machine_kwargs:
            kwargs[
                Prompts.get_parameter_field(prompt_id=self.machine.prompt_id)
            ] = self.machine.machine_kwargs["cache"]
        if self.machine.seed is not None:
            kwargs["seed"] = self.machine.seed
        output = self.machine.chain(**kwargs)
        if self.machine.in_cache:
            # The cache already has some values, so we need to merge them
            merged_output = self.machine.llm_response + output
            merged_output.raw_response = (
                self.machine.chain.output_parser.construct_raw_response(
                    merged_output.output
                )
            )
            self.machine.llm_response = merged_output
        else:
            self.machine.llm_response = output


class CheckKVPItemInCacheState(KVPLLMState):
    def run(self):
        (
            llm_response,
            missing_params,
            prompts,
        ) = CacheManager().check_row_extraction_in_cache(
            prompt_id=self.machine.prompt_id,
            item_id=self.machine.item_id or self.machine.collection_item_id,
            parameters=[]
            if not self.machine.user_kwargs
            else self.machine.user_kwargs.get(
                Prompts.get_parameter_field(prompt_id=self.machine.prompt_id), []
            ),
            language=self.machine.translation_language,
            seed=self.machine.seed,
            should_fetch_prompts=self.machine.should_fetch_prompts,
        )
        self.machine.prompts_used = prompts
        logger.debug(
            f"{'Partial' if len(missing_params) > 0 else 'Complete'} response from cache: {llm_response}"
        )
        logger.debug(f"Missing parameters: {missing_params}")
        if llm_response:
            self.machine.llm_response = llm_response
            self.machine.in_cache = True
            if missing_params:
                self.machine.machine_kwargs["cache"] = missing_params
        else:
            self.machine.in_cache = False


class AddKVPResponseToCacheState(KVPLLMState):
    def run(self):
        # Set the item ID to the collection item ID if it doesn't exist
        item_id = self.machine.item_id or self.machine.collection_item_id
        # Set the parameters to the user kwargs if they exist or the cache if it exists
        parameters = (
            []
            if not self.machine.user_kwargs
            else self.machine.user_kwargs.get(
                Prompts.get_parameter_field(prompt_id=self.machine.prompt_id), []
            )
            if not self.machine.in_cache
            else self.machine.machine_kwargs["cache"]
        )
        # Get the generation parameters, parameter as key, then index of key from the list of llm response parameters
        parameters_with_generation_parameters = {
            parameter: self.machine.llm_response.llm_params[
                list(self.machine.llm_response.output.keys()).index(parameter)
            ]
            if parameter in self.machine.llm_response.output
            else {}  # FIXME: this is needed for translated properties
            for parameter in parameters
        }

        # Check if the item is a collection item
        is_collection_item = False if self.machine.item_id else True
        new_cached_items = CacheManager().add_row_extraction_to_cache(
            prompt_id=self.machine.prompt_id.value,
            item_id=item_id,
            llm_response=self.machine.llm_response,
            parameters=parameters_with_generation_parameters,
            is_collection_item=is_collection_item,
            language=self.machine.translation_language,
            seed=self.machine.seed,
        )

        # Add the prompt to the cache
        variables = self.machine.user_kwargs.copy()
        prompt = CacheManager().add_prompt_to_cache(
            prompt_id=self.machine.prompt_id,
            cached_items_ids=[item.internal_id for item in new_cached_items],
            variables=variables,
        )
        if self.machine.prompts_used:
            # Update the prompt with the used prompt
            for param in parameters:
                if param not in self.machine.prompts_used:
                    self.machine.prompts_used[param] = prompt
        else:
            self.machine.prompts_used = {param: prompt for param in parameters}

        logger.debug(f"Added item(s) to cache: {new_cached_items}")


class TranslateKVPResponseState(KVPLLMState):
    def run(self):
        chain = (
            Prompts.TRANSLATE_KVP_RESPONSE
            | LLMFactory.create_llm()
            | Prompts.get_parser(self.machine.template)
        )
        kwargs = {
            "language": self.machine.translation_language,
            "response": self.machine.llm_response.raw_response,
        }
        self.machine.llm_response = chain(**kwargs)
        logger.debug(
            f"Translated: {self.machine.llm_response.raw_response.response[:30]}"
            f"... to {self.machine.translation_language}"
        )
