# Finetuning LLMs for Ask
This folder contains scripts that can be used to finetune large language models (LLMs) for the use cases of ORKG Ask in order to improve the performance in comparison to using vanilla models out of the box.

## Requirements
- Python 3.11 or higher (currently only tested with Python 3.11)
- pip
- GPU with at least 48GB of memory (tested with NVIDIA 2xH100 80GB)

## Finetuning workflow
- Get data from ORKG Ask using the [data fetching script](./fetching.py)
- With the help of bigger models like GPT-4o generate more accurate datasets using the [summarization script](./summarization.py) and the [information extraction script](./ie.py)
- Finetune the model using the [finetuning script](./finetuning.py)

## Usage
The scripts are all created as CLI scripts and can be used by running them with the `--help` flag to get more information about the available options.

You first need to set up a virtual environment and install the requirements:
```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### How-to "Fetch data"
The fetching script can be used to get question data from ORKG Ask. The script will fetch all questions from the ORKG Ask instance and save them in a JSON file. The script can be used as follows:
```bash
python fetching.py -pb <PB-host> -cn questions -ae email@email.com -ap password --output ask-dataset.json
```
**Note:** The script runs a 0-shot classification model to detect if questions are in proper question format or not. ideally this needs to run on a GPU-enabled machine.

### How-to "Generate the summarize data usecase"
The invocation of the summarization script is as follows:
```bash
python summarization.py -k KEY -m MODEL -i ./ask-dataset.json -o ./new-synthesis-dataset.jsonl
```
The `KEY` here refers to OpenAI key to use their models. If you want to use other providers the script needs to be updated.

### How-to "Generate the information extraction usecase"
The invocation of the information extraction script is as follows:
```bash
python ie.py --api-key KEY --model MODEL --input-file ./ask-dataset.json --output-file ./new-extraction-dataset.jsonl
```

### How-to "Finetune the model"
For practical purposes we use `deepspeed` to finetune the model. The invocation of the finetuning script is as follows:

For the summarization task
```bash
accelerate launch --config_file ./accelerate.yaml ./finetune.py -m <BASE-MODEL> -d cuda -o ./finetuned-model -i ./new-synthesis-dataset.jsonl -ml <CTX-SIZE> -t synthesis -k <HF-TOKEN>
```

The accelerate config file is generated and it looks like this in our case:
```yaml
compute_environment: LOCAL_MACHINE
debug: false
deepspeed_config:
  gradient_accumulation_steps: auto
  offload_optimizer_device: none
  offload_param_device: none
  zero3_init_flag: false
  zero_stage: 2
distributed_type: DEEPSPEED
downcast_bf16: 'no'
enable_cpu_affinity: false
machine_rank: 0
main_training_function: main
mixed_precision: fp16
num_machines: 1
num_processes: 2
rdzv_backend: static
same_network: true
tpu_env: []
tpu_use_cluster: false
tpu_use_sudo: false
use_cpu: false
```

Similarly, for the information extraction task:

```bash
accelerate launch --config_file ./accelerate.yaml ./finetune.py -m <BASE-MODEL> -d cuda -o ./output -i ./new-extraction-dataset.jsonl -ml <CTX-SIZE> -dr <DATASET-RATIO> -t extraction -k <HF-TOKEN> -p ./properties.json -q ./translated-questions.json
```
The [properties.json](./properties.json) file contains the properties and their descriptions in multiple languages.
The [translated-questions.json](./translated-questions.json) file contains the translated questions to the target language.

**Note:** These files are samples only!

## Artifacts
The dataset used for the training of the currently used model are available at:
```commandline
LINK TO BE ADDED
```

The model(s) used for the training are available at:
```commandline
LINK TO BE ADDED
```

## Transparency
The scripts are created to be used in a specific environment and might need to be adjusted to work in other environments. The scripts are created to be used with the OpenAI API and the HuggingFace Transformers library. The scripts are created to be used with the ORKG Ask instance and might need to be adjusted to work with other instances.

The main reason of having them available online is for transparency purposes and to show the process of how the models are trained and used in the ORKG Ask instance.
We do not provide any support for the scripts and they are provided as is.
