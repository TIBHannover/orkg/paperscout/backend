import json

from lark import Lark, LarkError, ParseError, Transformer
from qdrant_client import models

grammar = r"""
    start: filter

    filter: combined_condition
        | single_condition

    single_condition: condition

    combined_condition: condition AND condition_list
        | condition OR condition_list
        | "(" condition AND condition_list ")"
        | "(" condition OR condition_list ")"

    condition_list: condition (AND condition)*
        | condition (OR condition)*
        | bracketed_list

    bracketed_list: "(" condition_list ")"

    condition: like_condition
        | equal_value_condition
        | not_equal_value_condition
        | in_values_condition
        | not_in_values_condition
        | range_condition
        | is_null_condition
        | is_empty_condition

    like_condition: CNAME LIKE VALUE
    is_null_condition: IS_NULL "(" CNAME ")"
    is_empty_condition: IS_EMPTY "(" CNAME ")"
    equal_value_condition: CNAME EQ VALUE
    not_equal_value_condition: CNAME NEQ VALUE
    in_values_condition: CNAME IN VALUES
    not_in_values_condition: CNAME NOT IN VALUES
    range_condition: CNAME RANGE_OP NUMBER | CNAME RANGE_OP NUMBER SEP? RANGE_OP NUMBER


    RANGE_OP: GTE | LTE | GT | LT

    VALUES: "[" [VALUE (SEP VALUE)*] "]"

    LIKE: "LIKE"
    NOT: "NOT"
    AND: "AND"
    OR: "OR"
    IN: "IN"
    IS_NULL: "IS_NULL"
    IS_EMPTY: "IS_EMPTY"
    EQ: "="
    NEQ: "!="
    SEP: "," | /\s*,\s*/
    GT: ">" | /\s*>\s*/
    GTE: ">=" | /\s*>=\s*/
    LT: "<" | /\s*<\s*/
    LTE: "<=" | /\s*<=\s*/

    CNAME: /(\w+(\.\w+)*(\[\])?(\.\w+)*)/
    VALUE: STRING | NUMBER | /\w+/

    %import common.ESCAPED_STRING -> STRING
    %import common.SIGNED_NUMBER -> NUMBER
    %import common.WS
    %ignore WS
"""


range_operators_map = {">=": "gte", "<=": "lte", ">": "gt", "<": "lt"}


class FilterTransformer(Transformer):
    _filter: models.Filter

    def __init__(self):
        super().__init__()
        self._filter = models.Filter()

    def _add_must(self, condition) -> models.Filter:
        if self._filter.must is None:
            self._filter.must = [condition]
        else:
            self._filter.must.append(condition)
        return self._filter

    def _add_should(self, condition) -> models.Filter:
        if self._filter.should is None:
            self._filter.should = [condition]
        else:
            self._filter.should.append(condition)
        return self._filter

    def _add_must_not(self, condition) -> models.Filter:
        if self._filter.must_not is None:
            self._filter.must_not = [condition]
        else:
            self._filter.must_not.append(condition)
        return self._filter

    def start(self, _) -> models.Filter:
        try:
            return self._filter
        finally:
            self._filter = models.Filter()

    def filter(self, _):
        return self._filter

    def bracketed_list(self, args):
        return self.condition_list(args)

    def condition_list(self, args):
        if len(args) == 1:
            return args[0]
        method = self._add_must
        match args[1]:
            case "AND":
                method = self._add_must
            case "OR":
                method = self._add_should
        if not isinstance(args[0], models.Filter):
            if isinstance(args[0], list):
                method(args[0][0])
            else:
                method(args[0])
        if not isinstance(args[-1], models.Filter):
            if isinstance(args[-1], list):
                method(args[-1][0])
            else:
                method(args[-1])
        return self._filter

    def combined_condition(self, args):
        return self.condition_list(args)

    def condition(self, args):
        return args[0]

    def single_condition(self, args):
        if isinstance(args[0], models.Filter):
            return args[0]
        else:
            self._add_must(args[0])
        return self._filter

    def range_condition(self, args):
        return models.FieldCondition(
            key=args[0].value,
            range=models.Range(**{range_operators_map[args[1].value.strip()]: args[2]})
            if len(args) == 3
            else models.Range(
                **{
                    range_operators_map[args[1].value.strip()]: args[2],
                    range_operators_map[args[4].value.strip()]: args[5],
                }
            ),
        )

    def equal_value_condition(self, args):
        clean_value = args[-1]
        if isinstance(clean_value, str):
            if clean_value.startswith('"') or clean_value.startswith("'"):
                clean_value = clean_value[1:-1]
            if clean_value.isdigit():
                clean_value = int(clean_value)
        if isinstance(clean_value, float):
            raise ParseError("Float values are not supported for match filters")
        return models.FieldCondition(
            key=args[0].value, match=models.MatchValue(value=clean_value)
        )

    def not_equal_value_condition(self, args):
        clean_value = args[-1]
        if isinstance(clean_value, str):
            if clean_value.startswith('"') or clean_value.startswith("'"):
                clean_value = clean_value[1:-1]
        return models.FieldCondition(
            key=args[0].value, match=models.MatchExcept(**{"except": [clean_value]})
        )

    def in_values_condition(self, args):
        return models.FieldCondition(
            key=args[0].value, match=models.MatchAny(any=args[-1])
        )

    def not_in_values_condition(self, args):
        return models.FieldCondition(
            key=args[0].value, match=models.MatchExcept(**{"except": args[-1]})
        )

    def like_condition(self, args):
        clean_value = args[-1]
        if (
            isinstance(clean_value, str)
            and clean_value.startswith('"')
            or clean_value.startswith("'")
        ):
            clean_value = clean_value[1:-1]
        return models.FieldCondition(
            key=args[0].value, match=models.MatchText(text=clean_value)
        )

    def is_null_condition(self, args):
        return models.IsNullCondition(is_null=models.PayloadField(key=args[-1].value))

    def is_empty_condition(self, args):
        return models.IsEmptyCondition(is_empty=models.PayloadField(key=args[-1].value))

    def LIKE(self, args):
        return args

    def NOT(self, args):
        return args

    def EQ(self, args):
        return args

    def NEQ(self, args):
        return args

    def IS_NULL(self, args):
        return args

    def IS_EMPTY(self, args):
        key = args[0]
        return models.IsEmptyCondition(is_empty=models.PayloadField(key=key))

    def VALUES(self, args):
        values = json.loads(args.value)
        return [values] if not isinstance(values, list) else values

    def VALUE(self, args):
        return json.loads(args)


parser = Lark(grammar, start="start", parser="lalr", transformer=FilterTransformer())


def is_valid_filter_string(filter_str):
    try:
        parser.parse(filter_str)
        return True
    except LarkError:
        return False
