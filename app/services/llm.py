from typing import Dict, List

from app.models.llm import (
    ExtractItemValuesFromPropertiesRequest,
    ExtractItemValuesFromPropertiesResponse,
    LLMChatRequest,
    LLMGenerateRequest,
    SynthesisAnswerOfQuestionFromAbstractsResponse,
)
from app.modules.engines.llm import LLMEngine
from app.modules.llms.base import ParserOutput


def _normalize_key_value_pairs(values: Dict, properties: List[str]) -> (Dict, Dict):
    """
    Filters the values to only include those that are relevant to the given properties.
    :param values: The values to filter.
    :param properties: The properties to filter by.
    :return: A tuple containing the filtered values and the values that were not found.
    """
    normalized_values = {}
    extra_values = {}

    def normalize_key(key: str) -> str:
        key = key.lower()
        key = key.strip()
        key = key.replace(" ", "_")
        key = key.replace("-", "_")
        key = key.replace(".", "_")
        key = key.strip()
        return key

    keys_normalized = [(normalize_key(key), key) for key in values.keys()]
    properties_normalized = [normalize_key(prop) for prop in properties]

    for key, original_key in keys_normalized:  # type: str, str
        if key in properties_normalized:
            normalized_values[properties[properties_normalized.index(key)]] = values[
                original_key
            ]
        else:
            extra_values[original_key] = values[original_key]
    return normalized_values, extra_values


def _create_values_extraction_reproducibility(
    llm_output: Dict, llm_params: List, prompts_used: Dict
) -> ExtractItemValuesFromPropertiesResponse.Reproducibility:
    """
    Creates a reproducibility object for the Key value pairs extraction.
    :param llm_params: The LLM parameters.
    :param llm_output: The LLM output.
    :param prompts_used: The prompts used.
    :return: The reproducibility object.
    """
    # compute hashes for the collection
    hashes = {}
    indexes = {}
    collection = []
    collection_map = {}
    llm_params_index_key_map = {k: i for i, k in enumerate(llm_output.keys())}
    for k, _ in llm_output.items():
        hashes[k] = hash(str(llm_params[llm_params_index_key_map[k]]))
        if hashes[k] in collection_map:
            indexes[k] = collection_map[hashes[k]]
        else:
            indexes[k] = len(collection)
            collection.append(llm_params[llm_params_index_key_map[k]])
            collection_map[hashes[k]] = indexes[k]

    # compute hashes for the prompts
    prompt_hashes = {}
    prompt_indexes = {}
    prompt_collection = []
    prompt_collection_map = {}

    for k, _ in prompts_used.items():
        prompt_hashes[k] = hash(str(prompts_used[k]))
        if prompt_hashes[k] in prompt_collection_map:
            prompt_indexes[k] = prompt_collection_map[prompt_hashes[k]]
        else:
            prompt_indexes[k] = len(prompt_collection)
            prompt_collection.append(prompts_used[k])
            prompt_collection_map[prompt_hashes[k]] = prompt_indexes[k]

    prompt_collection = [p.model_dump() for p in prompt_collection]

    return ExtractItemValuesFromPropertiesResponse.Reproducibility(
        parameters=ExtractItemValuesFromPropertiesResponse.GenerationParameters(
            indexes=indexes, collection=collection
        ),
        prompts=ExtractItemValuesFromPropertiesResponse.PromptsUsed(
            indexes=prompt_indexes, collection=prompt_collection
        ),
    )


def extract_values_for_properties_of_item(
    request: ExtractItemValuesFromPropertiesRequest,
    include_reproducibility: bool,
) -> ExtractItemValuesFromPropertiesResponse.Payload:
    """
    Extracts the values for the given properties from the given item.
    """
    engine_response, seed, prompts_used = LLMEngine().run_key_value_pairs_extraction(
        item_id=request.item_id,
        properties=[prop.strip() for prop in request.properties],
        invalidate_cache=request.invalidate_cache,
        collection_item_id=request.collection_item_id,
        response_language=request.response_language,
        seed=request.seed,
        should_fetch_prompts=include_reproducibility,
    )
    values, extra = _normalize_key_value_pairs(
        engine_response.output, request.properties
    )
    payload = ExtractItemValuesFromPropertiesResponse.Payload(
        item_id=request.item_id or request.collection_item_id,
        properties=[prop.strip() for prop in request.properties],
        values=values,
        extra=extra,
        seed=seed,
    )
    if include_reproducibility:
        payload.reproducibility = _create_values_extraction_reproducibility(
            engine_response.output, engine_response.llm_params, prompts_used
        )

    return payload


def synthesize_abstracts_for_question(
    item_ids: List[str | int],
    collection_item_ids: List[str | int],
    question: str,
    invalidate_cache: bool = False,
    response_language: str | None = None,
    seed: int | None = None,
    include_reproducibility: bool = False,
) -> SynthesisAnswerOfQuestionFromAbstractsResponse.Payload:
    """
    Synthesizes the abstracts for the given item IDs and question.
    :param item_ids: a list of item ids
    :param collection_item_ids: a list of custom item ids
    :param question: a research question
    :param invalidate_cache: whether to invalidate the cache
    :param response_language: the language of the response (if explicitly set)
    :param seed: the seed to use during synthesis (if explicitly set)
    :param include_reproducibility: whether to include the reproducibility parameters of the response
    """
    llm_response: ParserOutput[Dict]
    mappings: Dict

    (
        llm_response,
        mappings,
        response_seed,
        used_prompt,
    ) = LLMEngine().run_synthesis_of_abstracts_for_question(
        item_ids=item_ids or [],
        collection_item_ids=collection_item_ids or [],
        question=question.strip(),
        invalidate_cache=invalidate_cache,
        response_language=response_language,
        seed=seed,
        should_fetch_prompts=include_reproducibility,
    )

    payload = SynthesisAnswerOfQuestionFromAbstractsResponse.Payload(
        items_mapping=mappings["core"],
        collection_items_mapping=mappings["collection"],
        question=question.strip(),
        seed=response_seed,
        **llm_response.output,
    )
    if include_reproducibility:
        payload.reproducibility = (
            SynthesisAnswerOfQuestionFromAbstractsResponse.Reproducibility(
                parameters=llm_response.llm_params, prompt=used_prompt
            )
        )
    return payload


def generate_llm_response(request: LLMGenerateRequest) -> Dict:
    """
    Generates a response for the given LLM request.
    """
    return LLMEngine().generate_raw_llm_response(request)


def generate_chat_llm_response(request: LLMChatRequest) -> Dict:
    """
    Generates a response for the given LLM chat request.
    """
    return LLMEngine().generate_chat_llm_response(request)
