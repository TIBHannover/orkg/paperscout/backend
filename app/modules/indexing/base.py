from abc import ABC, abstractmethod
from enum import Enum
from typing import Any, Dict, List, TypeAlias

from pydantic import BaseModel, Field

InternalID: TypeAlias = str | int


class StorePageResponse(ABC):
    ...


class IDocument(BaseModel, ABC):
    """
    Base class for a document. It only contains the document ID.
    Should be extended by the vector store implementation.
    """

    id: str | int = Field(..., description="The document ID, here is the coreID")

    @abstractmethod
    def get_source_fields_as_text(self) -> str:
        """
        Get the source fields as text. This is used for indexing the document.
        If you have two fields, title and abstract, you should return something like:
        return f"{self.title} {self.abstract}"
        :return: The source fields as text.
        """
        pass


class FilterType(Enum):
    MUST = "MUST"
    MUST_NOT = "MUST-NOT"
    SHOULD = "SHOULD"


class FilterOperator(Enum):
    EQUALS = "EQUALS"
    EXCEPT = "EXCEPT"
    ANY = "ANY"
    LESS_THAN = "LESS-THAN"
    LESS_THAN_EQUALS = "LESS-THAN-EQUALS"
    GREATER_THAN = "GREATER-THAN"
    GREATER_THAN_EQUALS = "GREATER-THAN-EQUALS"
    IS_NULL = "IS-NULL"
    LIKE = "LIKE"


class KeyValueFilter(BaseModel, extra="forbid"):
    key: str = Field(..., description="The key of the payload to filter on")
    value: str | int | float | bool | List | None = Field(
        default=None, description="The value expression to filter on"
    )
    filter_type: FilterType | None = Field(
        default=None, description="The type of filter"
    )
    operator: FilterOperator | None = Field(
        default=None, description="The operator to use for matching"
    )


class RecordFilters(BaseModel, extra="forbid"):
    on: List[KeyValueFilter] = Field(
        default=None, description="The filters of the records"
    )

    @abstractmethod
    def to_filter(self):
        """
        Convert the record filters to a filter object.
        Should be implemented by the vector store implementation.
        :return: The filter object.
        """
        raise NotImplementedError


class IStore(ABC):
    @abstractmethod
    def index(self, document: IDocument, *args, **kwargs) -> str:
        """
        Index a document into the vector store.
        :param document: The document to index.
        :return: The document ID.
        """
        pass

    @abstractmethod
    def bulk_index(self, documents: List[IDocument], *args, **kwargs) -> List[str]:
        """
        Index a list of documents into the vector store. Ideally via grpc streaming.
        :param documents: The documents to index.
        :return: The document IDs.
        """
        pass

    @abstractmethod
    def get(
        self,
        doc_id: str | int | None = None,
        criteria: RecordFilters | None = None,
        limit: int = 1,
        offset: int | None = None,
        *args,
        **kwargs
    ) -> StorePageResponse:
        """
        Get a document from the vector store. The document can be fetched via ID or a criteria selection.
        :param doc_id: The document ID. (Optional)
        :param criteria: The criteria to fetch the document. (Optional)
        :param limit: The maximum number of documents to return. (default: 1)
        :param offset: The offset to start fetching documents from. (Optional)
        :return: The document(s) in question.
        """
        pass

    @abstractmethod
    def search(
        self,
        query: str,
        limit: int = 10,
        offset: int | None = None,
        criteria: RecordFilters | None = None,
        include_vectors: bool = False,
        focus: List[str] | None = None,
        *args,
        **kwargs
    ) -> StorePageResponse:
        """
        Search the vector store for a document.
        :param query: The text to search for.
        :param limit: The maximum number of documents to return. (default: 10)
        :param offset: The offset to start fetching documents from. (Optional)
        :param criteria: The criteria to filter the search results. (Optional)
        :param include_vectors: Whether to include the vectors in the search results. (default: False)
        :param focus: The sources to search from. (Optional)
        :return: The list of documents that match the search.
        """
        pass

    @abstractmethod
    def delete(
        self,
        criteria: RecordFilters | None = None,
        doc_id: str | int = None,
        *args,
        **kwargs
    ) -> int:
        """
        Delete a document from the vector store. The document can be deleted via ID or a criteria selection.
        :param criteria: The criteria to delete the document. (Optional)
        :param doc_id: The document ID to be deleted. (Optional)
        :return: The number of documents deleted.
        """
        pass

    @abstractmethod
    def count(self, criteria: RecordFilters | None = None, *args, **kwargs) -> int:
        """
        Count the number of documents in the vector store. The documents can be counted via a criteria selection.
        :param criteria: The criteria to count the documents. (Optional)
        :return: The number of documents.
        """
        pass

    @abstractmethod
    def info(self) -> Dict[str, Any]:
        """
        Get information about the vector store.
        :return: The vector store information.
        """
        pass

    @abstractmethod
    def recommend(
        self,
        ids: List[str | int],
        avoid: List[str | int] = None,
        limit: int = 10,
        offset: int | None = None,
        criteria: RecordFilters | None = None,
        *args,
        **kwargs
    ) -> List[IDocument]:
        """
        Recommend documents based on a document ID.
        :param ids: The document IDs to use as positive examples while recommending.
        :param avoid: The document IDs to use as negative examples. (Optional)
        :param limit: The maximum number of documents to return. (default: 10)
        :param offset: The offset to start fetching documents from. (Optional)
        :param criteria: The criteria to filter the recommended documents (Optional).
        :return: The list of recommended documents.
        """
        pass


class IManager(ABC):
    @abstractmethod
    def store(self) -> IStore:
        """
        Get the vector store.
        :return: The vector store.
        """
        pass
