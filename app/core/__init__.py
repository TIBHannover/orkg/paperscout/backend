import os

from dotenv import load_dotenv

from app.core.config.models import AskSettings
from app.core.config.readers import TomlReader

load_dotenv()

# If ASK_CONFIG is set, use it as the path to the config file
# Otherwise, use the default config file
path = os.environ.get("ASK_CONFIG", None)

settings: AskSettings = TomlReader.get_settings(path)
