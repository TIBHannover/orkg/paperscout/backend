from typing import Dict, List, Optional
from urllib.parse import quote as url_encode

from pocketbase import PocketBase
from pydantic import BaseModel

from app.core import settings
from app.errors import CachingUserCredentialsError
from app.modules.caching.base import ICache
from app.modules.caching.models import PocketBaseListResult


class PocketBaseCache(ICache):
    _cache: PocketBase

    def __init__(self, host: str):
        super().__init__()
        self._cache = PocketBase(base_url=host)
        if not settings.cache.credentials:
            raise CachingUserCredentialsError("PocketBase credentials are not set.")
        if settings.cache.authenticate:
            self._cache.admins.auth_with_password(
                email=settings.cache.credentials.email,
                password=settings.cache.credentials.password,
            )

    def search(
        self,
        collection: str,
        page: Optional[int] = None,
        per_page: int = 30,
        sort: Optional[str] = None,
        filters: Optional[str] = None,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ) -> PocketBaseListResult:
        """
        Fetch a paginated items records list, supporting sorting and filtering.
        :param collection: The collection to fetch the items from.
        :param page: The page (aka. offset) of the paginated list (default to 1).
        :param per_page: Specify the max returned records per page (default to 30).
        :param sort: Specify the records order attribute(s).
         Add - / + (default) in front of the attribute for DESC / ASC order.
         Example: sort=-created,+updated
        :param filters: Filter the returned records. Example: id='abc' && created>'2022-01-01'
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField.
         Supports up to 6-levels depth nested relations expansion.
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields). Example: *,expand.relField.name.
         In addition, the following field modifiers are also supported: description:excerpt(200,true)
          for excerpt(maxLength, withEllipsis?)
        :return: a list of items records
        """
        query_params = {}
        if sort:
            query_params["sort"] = sort
        if filters:
            query_params["filter"] = filters
        if expand:
            query_params["expand"] = expand
        if fields:
            query_params["fields"] = fields

        if page is None:
            page = 1

        result = self._cache.collection(collection).get_list(
            page=page, per_page=per_page, query_params=query_params
        )
        return PocketBaseListResult(
            page=result.page,
            per_page=result.per_page,
            total_items=result.total_items,
            total_pages=result.total_pages,
            items=result.items,
        )

    def get_all(
        self,
        collection: str,
        sort: Optional[str] = None,
        filters: Optional[str] = None,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ) -> List[BaseModel]:
        """
        Fetch all items records.
        :param collection: The collection to fetch the items from.
        :param sort: Specify the records order attribute(s).
         Add - / + (default) in front of the attribute for DESC / ASC order.
         Example: sort=-created,+updated
        :param filters: Filter the returned records. Example: id='abc' && created>'2022-01-01'
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields)
        :return: a list of items records
        """
        return self._cache.collection(collection).get_full_list(
            batch=10000,
            query_params={
                "sort": sort,
                "filter": filters,
                "expand": expand,
                "fields": fields,
            },
        )

    def get(
        self,
        collection: str,
        item_id: str,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ) -> BaseModel:
        """
        Fetch a single items record.
        :param collection: The collection to fetch the item from.
        :param item_id: ID of the record to view
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields)
        :return: a single item record
        """
        return self._cache.collection(collection).get_one(
            item_id, query_params={"expand": expand, "fields": fields}
        )

    def insert(
        self,
        collection: str,
        item: Dict,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ) -> BaseModel:
        """
        Create a new items record.
        :param collection: The collection to fetch the item from.
        :param item: The item (dict) to create
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields)
        :return: the created item record
        """
        return self._cache.collection(collection).create(
            body_params=item, query_params={"expand": expand, "fields": fields}
        )

    def upsert(
        self,
        collection: str,
        item: Dict,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
        overwrite_on: Optional[str] = None,
    ):
        """
        Create a new items record.
        :param collection: The collection to fetch the item from.
        :param item: The item (dict) to create
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields)
        :param overwrite_on: which filter to overwrite on
        :return: the created item record
        """
        if overwrite_on:
            overwrite_on = url_encode(overwrite_on)
            results = self.search(collection, filters=overwrite_on)
            if results.total_items > 0:
                # If results, update the first item
                return self.update(
                    collection,
                    results.items[0].id,
                    item,
                    expand=expand,
                    fields=fields,
                )
        # If no results, create a new item
        return self.insert(
            collection,
            item,
            expand=expand,
            fields=fields,
        )

    def update(
        self,
        collection: str,
        item_id: str,
        item: Dict,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ) -> BaseModel:
        """
        Update an existing items record.
        :param collection: The collection to fetch the item from.
        :param item_id: ID of the record to update
        :param item: The item (instance of BaseModel) to update
        :param expand: Auto expand record relations. Example: relField1,relField2.subRelField
        :param fields: Comma separated string of the fields to return in the JSON response
         (by default returns all fields)
        :return: the updated item record
        """
        return self._cache.collection(collection).update(
            item_id,
            body_params=item,
            query_params={"expand": expand, "fields": fields},
        )

    def delete(self, collection: str, item_id: str) -> bool:
        """
        Delete an existing items record.
        :param collection: The collection to fetch the item from.
        :param item_id: ID of the record to delete
        :return: True if the record was deleted successfully, False otherwise
        """
        return self._cache.collection(collection).delete(item_id)

    def count(self, collection: str) -> int:
        """
        Count the number of items in a collection.
        :param collection: The collection to count the items from.
        :return: The number of items in the collection
        """
        return self._cache.collection(collection).get_list(per_page=1).total_items
