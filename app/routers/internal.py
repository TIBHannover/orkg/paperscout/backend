import http

from fastapi import APIRouter

from app.core.decorators import log
from app.services import search

router = APIRouter(prefix="/index")


@router.get(
    "/reinitialize",
    status_code=http.HTTPStatus.OK,
    response_model=bool,
)
@log(__name__)
def reinitialize_store():
    """
    Reinitialize the vector store by removing all documents.
    This is for internal use only. The router is not connected if the application is not in test.
    """
    return search.reinitialize_store()
