import unittest
from unittest.mock import Mock, patch

from faker import Faker
from fastapi.testclient import TestClient

from app.modules.indexing.vectordb.models import QdrantDocument

fake = Faker()


def create_document_object(**kwargs) -> QdrantDocument:
    return QdrantDocument(**kwargs)


def create_randomized_document_object(**kwargs) -> QdrantDocument:
    document = QdrantDocument(
        id=fake.msisdn(),
        title=fake.sentence(),
        abstract=fake.text(),
        year=fake.year(),
        url=fake.url(),
        authors=[fake.name() for _ in range(3)],
    )
    # if any of the kwargs are provided, update the document
    if kwargs:
        document = document.model_copy(update=kwargs)
    return document


class TestAPIEndpoints(unittest.TestCase):
    """
    Base class for testing API endpoints
    The class contains the testing client for the FastAPI application
    """

    _client: TestClient

    @classmethod
    def setUpClass(cls):
        from app.app_factory import get_application

        app = get_application(testing=True)
        cls._client = TestClient(app)


def mock_test(
    target=None,
    attribute=None,
    new_value=None,
    return_value=None,
    **kwargs,
):
    """
    Decorator to patch a function or object.
    :param target: The target function or object to patch.
    :param new_value: The new value to replace the target with. If not specified,
            a mock object will be created.
    :param attribute: The attribute of the target to patch.
    :param return_value: The fabrication response to the mocked function or object.
    :param kwargs: Keyword arguments to pass to the mock.patch() decorator.
    :return The patched function or object.
    """

    if target is None:
        return None

    if new_value is None:
        new_value = Mock()

    if attribute is None:
        if not isinstance(target, str):
            target_str = f"{target.__module__}.{target.__qualname__}"
        else:
            target_str = target
        return patch(
            target_str, new_value=new_value, return_value=return_value, **kwargs
        )
    else:
        return patch.object(
            target, attribute, new_value=new_value, return_value=return_value, **kwargs
        )
