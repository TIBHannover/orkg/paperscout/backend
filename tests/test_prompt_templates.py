import unittest

from app.modules.llms.base import (
    BaseOutputParser,
    Chain,
    LLMResponse,
    ParserOutput,
    PassThroughOutputParser,
    PromptTemplate,
)
from app.modules.llms.models import DummyLLM


class TestPromptTemplates(unittest.TestCase):
    def test_prompt_template(self):
        prompt_template = PromptTemplate.from_id_and_user_prompt(1, "user prompt")
        assert prompt_template.id_ == 1, "The ID should be 1"
        assert prompt_template.system_prompt is None, "The system prompt should be None"
        assert (
            prompt_template.user_prompt == "user prompt"
        ), "The user prompt should be 'user prompt'"

    def test_prompt_template_with_system_prompt(self):
        prompt_template = PromptTemplate(
            id=1, system_prompt="system prompt", user_prompt="user prompt"
        )
        assert prompt_template.id_ == 1, "The ID should be 1"
        assert (
            prompt_template.system_prompt == "system prompt"
        ), "The system prompt should be 'system prompt'"
        assert (
            prompt_template.user_prompt == "user prompt"
        ), "The user prompt should be 'user prompt'"

    def test_prompt_template_formatting(self):
        prompt_template = PromptTemplate.from_id_and_user_prompt(
            1, "Hello {name}!"
        ).format(name="World")
        assert prompt_template.id_ == 1, "The ID should be 1"
        assert prompt_template.system_prompt is None, "The system prompt should be None"
        assert (
            prompt_template.user_prompt == "Hello World!"
        ), "The user prompt should be 'Hello World!'"

    def test_prompt_template_chain(self):
        prompt_template = PromptTemplate.from_id_and_user_prompt(1, "user prompt")
        chain = prompt_template | DummyLLM()
        assert isinstance(
            chain, Chain
        ), "The resulting object from the pipe should be of type Chain"
        assert (
            chain.prompt_template == prompt_template
        ), "The prompt template should be the same"
        assert chain.llm.name == "dummy", "The LLM should be the DummyLLM"
        assert isinstance(
            chain.output_parser, PassThroughOutputParser
        ), "The default PassThroughOutputParser should be used"

    def test_chain_custom_parser(self):
        class RedactedParser(BaseOutputParser):
            def parse(self, text: str) -> str:
                return "REDACTED"

        prompt_template = PromptTemplate.from_id_and_user_prompt(1, "user prompt")
        chain = prompt_template | DummyLLM() | RedactedParser()
        assert isinstance(
            chain, Chain
        ), "The resulting object from the pipe should be of type Chain"
        assert isinstance(
            chain.output_parser, RedactedParser
        ), "The custom RedactedParser should be used"

    def test_full_chain(self):
        prompt_template = PromptTemplate.from_id_and_user_prompt(1, "user prompt")
        chain = prompt_template | DummyLLM()
        response = chain.invoke()
        assert isinstance(response.output, str), "The response should be a string"
        assert (
            response.output == "Dummy LLM invoked"
        ), "The response should be 'Dummy LLM invoked'"

    def test_full_chain_with_parser(self):
        class ToUpperParser(BaseOutputParser):
            def parse(self, response: LLMResponse) -> ParserOutput[str]:
                return ParserOutput(
                    raw_response=response, output=response.response.upper(), params={}
                )

        prompt_template = PromptTemplate.from_id_and_user_prompt(1, "user prompt")
        chain = prompt_template | DummyLLM() | ToUpperParser()
        response = chain.invoke()
        assert isinstance(response.output, str), "The response should be a string"
        assert (
            response.output == "DUMMY LLM INVOKED"
        ), "The response should be 'DUMMY LLM INVOKED'"
