def merge_dicts(*dicts):
    """
    Merge multiple dictionaries recursively.
    The process is as follows:
    - If a key is present in multiple dictionaries, the values are merged:
        - If multiple values are dictionaries, merge them recursively.
        - Otherwise, create a list containing all values.
    - If a key is present in only one dictionary, add that key-value pair to the result.

    Example:
    >>> dict1 = {'a': 1, 'b': {'c': 2, 'd': 3}, 'e': 4}
    >>> dict2 = {'a': 5, 'b': {'c': 6, 'f': 7}, 'g': 8}
    >>> merge_dicts(dict1, dict2)
    >>> {'a': [1, 5], 'b': {'c': [2, 6], 'd': 3, 'f': 7}, 'e': 4, 'g': 8}
    :param dicts: One or more dictionaries to merge.
    :return: a new dictionary with the merged values
    """
    result = {}
    for d in dicts:
        for key in d.keys():
            if key in result:
                if isinstance(result[key], dict) and isinstance(d[key], dict):
                    result[key] = merge_dicts(result[key], d[key])
                else:
                    result[key] = [
                        value for value in [result[key], d[key]] if value is not None
                    ]
            else:
                result[key] = d[key]
    return result
