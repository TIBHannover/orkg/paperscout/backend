from app.core import settings
from app.errors import ProjectSetupError
from app.modules.llms.base import LLM
from app.modules.llms.models import OllamaLLM, TgiLLM


class LLMFactory:
    @staticmethod
    def create_ollama(model: str = "mistral:instruct") -> OllamaLLM:
        """
        Create an instance of the Ollama LLM
        :param model: the model to use (default: mistral:instruct)
        """
        if settings.get_default_llm().provider != "ollama":
            raise ProjectSetupError("llm.provider is not set to ollama")
        if settings.get_default_llm().host is None:
            raise ProjectSetupError("llm.host is not set")
        if model is None:
            raise AttributeError("`model` value for the Ollama-based LLM is not set!")
        return OllamaLLM(ollama_host=settings.get_default_llm().host, model=model)

    @staticmethod
    def create_tgi() -> TgiLLM:
        """
        Create an instance of the TGI LLM
        """
        if settings.get_default_llm().provider != "tgi":
            raise ProjectSetupError("llm.provider is not set to tgi")
        return TgiLLM(host=settings.get_default_llm().host)

    @staticmethod
    def create_llm() -> LLM:
        """
        Create an instance of the main LLM to use
        """
        match settings.get_default_llm().provider:
            case "ollama":
                return LLMFactory.create_ollama(settings.get_default_llm().model)
            case "tgi":
                return LLMFactory.create_tgi()
            case _:
                raise ProjectSetupError(
                    f"llm.provider is not set to a valid value: {settings.get_default_llm().provider}"
                )
