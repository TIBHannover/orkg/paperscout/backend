from fastapi import HTTPException


class ProjectSetupError(RuntimeError):
    """Raised when the project is not setup properly"""

    pass


class InternalLLMCallError(RuntimeError):
    """Raised when the call to the internal LLM fails"""

    pass


class IndexNotAvailableError(RuntimeError):
    """Raised when an index is not available"""

    pass


class StateMachineNotInitializedError(RuntimeError):
    """Raised when the state machine is not initialized"""

    pass


class PromptIdNotRecognizedError(RuntimeError):
    """Raised when the prompt ID is not recognized"""

    pass


class CachingUserCredentialsError(RuntimeError):
    """Raised when the caching user credentials are not set"""

    pass


class ItemNotFoundError(HTTPException):
    """Raised when an item is not found"""

    def __init__(self, msg: str):
        super().__init__(status_code=404, detail=msg)


class IncorrectAPIInvocationError(HTTPException):
    """Raised when the API is invoked incorrectly"""

    def __init__(self, msg: str):
        super().__init__(status_code=400, detail=msg)
