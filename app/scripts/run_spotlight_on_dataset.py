# This script runs the spotlight on the dataset and updates the entities in the payload of the items in the collection
# You can use this script like this:
# python ./run_spotlight_on_dataset.py -d -q <qdrant-host> -e <spotlight-endpoint> -v -se <every-mins> -sf <sleep-mins>

from argparse import ArgumentParser
from time import sleep, time

import requests
from qdrant_client import QdrantClient
from qdrant_client.models import FieldCondition, Filter, MatchValue, Record

SPOTLIGHT_ENDPOINT = "/rest/annotate"
CONFIDENCE = 0.75
ITEMS_DONE = set()
OFFSET = "0cf98c08-43a4-4468-a735-bf44f77eb1fc"


def scroll_every_item(
    qdrant_host: str,
    collection_name: str,
    duplicate: bool,
    spotlight_host: str,
    verbose: bool,
    sleep_every: int,
    sleep_for: int,
):
    client = QdrantClient(qdrant_host, timeout=30)
    offset = OFFSET
    start = time()
    while True:
        if verbose:
            print(f"=== Requesting a new batch of items with offset {offset} ===")
        items = client.scroll(collection_name, limit=200, offset=offset)
        if not items[0]:
            break
        offset = items[1]
        for item in items[0]:
            if duplicate:
                get_all_items_with_same_id(
                    client, item, collection_name, spotlight_host, verbose
                )
            else:
                entities, in_cache = run_spotlight_on_item(
                    item, spotlight_host, verbose
                )
                if not in_cache:
                    update_item_payload(
                        client, item, collection_name, item.payload, entities
                    )
                    ITEMS_DONE.add(item.id)
            current = time()
            if current - start > sleep_every * 60:
                if verbose:
                    print(f"** Sleeping for {sleep_for} minutes")
                sleep(sleep_for * 60)
                start = time()


def get_all_items_with_same_id(
    qdrant_client: QdrantClient,
    item: Record,
    collection_name: str,
    spotlight_host: str,
    verbose: bool,
):
    count = qdrant_client.count(
        collection_name=collection_name,
        count_filter=Filter(
            must=[FieldCondition(key="id", match=MatchValue(value=item.payload["id"]))]
        ),
    )
    duplicated_items = qdrant_client.scroll(
        collection_name,
        limit=count.count,
        scroll_filter=Filter(
            must=[FieldCondition(key="id", match=MatchValue(value=item.payload["id"]))]
        ),
    )
    if not duplicated_items[0]:
        return
    one_item = duplicated_items[0][0]
    entities, in_cache = run_spotlight_on_item(one_item, spotlight_host, verbose)

    if in_cache:
        return

    # Update the payload of all the points
    update_item_payload(
        qdrant_client, duplicated_items[0], collection_name, one_item.payload, entities
    )
    ITEMS_DONE.update([item.id for item in duplicated_items[0]])

    if verbose:
        print(f"Updated {len(duplicated_items[0])} items with id {item.payload['id']}")


def update_item_payload(
    qdrant_client: QdrantClient,
    item_or_items: Record | list[Record],
    collection_name: str,
    payload: dict,
    entities: list,
):
    if isinstance(item_or_items, Record):
        item_or_items = [item_or_items]
    if "extractions" not in payload:
        payload["extractions"] = {"entities": {"dbpedia": []}}
    payload["extractions"]["entities"]["dbpedia"] = [
        {"uri": entity[0], "surface_form": entity[1]} for entity in entities
    ]

    # Deduplicate the entities based on the surface form and the uri
    deduplicated_entities = list(
        {
            (entity["uri"], entity["surface_form"])
            for entity in payload["extractions"]["entities"]["dbpedia"]
        }
    )
    payload["extractions"]["entities"]["dbpedia"] = [
        {"uri": entity[0], "surface_form": entity[1]}
        for entity in deduplicated_entities
    ]

    qdrant_client.overwrite_payload(
        collection_name=collection_name,
        payload=payload,
        points=[item.id for item in item_or_items],
    )


def run_spotlight_on_item(
    item: Record, spotlight_host: str, verbose: bool
) -> (list, bool):
    # Check if item in the done list
    if item.id in ITEMS_DONE:
        return [], True

    abstract = item.payload["abstract"]

    # Make request to spotlight
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    data = {
        "text": abstract,
        "confidence": CONFIDENCE,
    }
    response = requests.post(
        spotlight_host + SPOTLIGHT_ENDPOINT, headers=headers, data=data
    )
    output = response.json()
    if "Resources" not in output:
        return [], False
    if verbose:
        print(f"Extracted {len(output['Resources'])} entities from item {item.id}")
    return [(res["@URI"], res["@surfaceForm"]) for res in output["Resources"]], False


def cli():
    parser = ArgumentParser(description="Run Spotlight on a dataset")
    # Add the spotlight endpoint
    parser.add_argument(
        "-e", "--endpoint", help="The endpoint to run spotlight on", required=True
    )
    # Add the Qdrant host
    parser.add_argument(
        "-q", "--qdrant", help="The host to connect to Qdrant", required=True
    )
    # Add flag for duplicate
    parser.add_argument(
        "-d", "--duplicate", help="Whether to remove duplicates", action="store_true"
    )
    # Add collection name with default to `papers`
    parser.add_argument(
        "-c", "--collection", help="The name of the collection", default="papers"
    )
    # Verbose flag
    parser.add_argument(
        "-v", "--verbose", help="Whether to print verbose output", action="store_true"
    )
    parser.add_argument(
        "-se", "--sleep-every", help="Sleep every n minutes", default=5, type=int
    )
    parser.add_argument(
        "-sf", "--sleep-for", help="Sleep for n minutes", default=1, type=int
    )

    args = parser.parse_args()
    scroll_every_item(
        args.qdrant,
        args.collection,
        args.duplicate,
        args.endpoint,
        args.verbose,
        int(args.sleep_every),
        int(args.sleep_for),
    )


if __name__ == "__main__":
    cli()
