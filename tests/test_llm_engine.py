import unittest

from app.mocks.pocketbase import InMemoryPocketBase
from app.modules.caching.manager import PocketBaseCache
from app.modules.engines.llm import LLMEngine
from app.modules.llms.models import DummyLLM, OllamaLLM
from tests import mock_test


class TestLLMEngine(unittest.TestCase):
    def test_engine_is_singleton(self):
        engine1 = LLMEngine()
        engine2 = LLMEngine()
        assert engine1 is engine2, "The two engine instances should be the same."

    @mock_test(target=OllamaLLM, new_value=DummyLLM)
    @mock_test(target=PocketBaseCache, new_value=InMemoryPocketBase)
    @unittest.skip("Not all the components are mocked yet.")
    def test_engine_run(self, *args):
        final_result, _, _ = LLMEngine().run_key_value_pairs_extraction(
            item_id="9998851275",
            collection_item_id=None,
            properties=["Methods", "Results"],
            invalidate_cache=False,
            response_language=None,
            seed=None,
        )
        assert final_result is not None, "The final result should not be None."
        assert isinstance(
            final_result, dict
        ), "The final result should be a dictionary."
        print(final_result)
