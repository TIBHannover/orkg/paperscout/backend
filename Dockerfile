FROM python:3.11 as requirements-stage
LABEL maintainer="Yaser Jaradeh <Yaser.Jaradeh@tib.eu>"

WORKDIR /tmp

RUN pip install poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

# Install the Poetry Export Plugin
RUN poetry self add poetry-plugin-export

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes --with dev

# Splitting in two stages gets rid of poetry, as for now, since it's not required for the application itself.
FROM python:3.11
LABEL maintainer="Yaser Jaradeh <Yaser.Jaradeh@tib.eu>"

RUN install --owner=nobody --group=nogroup --directory /ask

WORKDIR /ask

COPY --from=requirements-stage --chown=nobody:nogroup /tmp/requirements.txt /ask/requirements.txt

USER root

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --upgrade -r /ask/requirements.txt

COPY --chown=nobody:nogroup ./ /ask/

# Make a directory for config and copy the example toml file
RUN install --owner=nobody --group=nogroup --directory /ask/config
COPY --chown=nobody:nogroup ./config.toml.example /ask/config/config.toml

USER nobody

CMD ["gunicorn", "app.main:app", "--workers", "8",  "--timeout", "0", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:4321", "--access-logfile=-", "--error-logfile=-"]
