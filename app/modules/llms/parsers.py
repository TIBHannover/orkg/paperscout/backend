import re
from typing import Dict

from app.modules.llms.base import BaseOutputParser, LLMResponse, ParserOutput


class SanitizeParser(BaseOutputParser):
    def parse(self, llm_response: LLMResponse) -> ParserOutput[str]:
        return ParserOutput(
            raw_response=llm_response,
            output=llm_response.response.strip(),
            llm_params=llm_response.params,
        )

    def sanitize_value(self, value: str) -> str:
        """
        Sanitize the value by removing leading and trailing whitespace, brackets, and quotes.
        :param value: The value to sanitize.
        :return: The sanitized value.
        """
        # Remove leading and trailing whitespace
        value = value.strip()
        # Remove brackets if they exist
        if value.startswith("[") or value.startswith("("):
            value = value[1:]
        if value.endswith("]") or value.endswith(")"):
            value = value[:-1]
        # Remove quotes if they exist
        if value.startswith('"') or value.startswith("'"):
            value = value[1:]
        if value.endswith('"') or value.endswith("'"):
            value = value[:-1]
        value = value.strip()
        return value


class KeyValueListParser(SanitizeParser):
    def parse(self, llm_response: LLMResponse) -> ParserOutput[Dict]:
        llm_response = super().parse(llm_response).raw_response
        content = llm_response.response
        if not content:
            return ParserOutput(
                raw_response=llm_response, output={}, llm_params=llm_response.params
            )
        key_value_pairs = dict()
        for split in content.split("\n"):
            if ":" in split:
                key, value = split.split(":", 1)
                # Remove leading and trailing whitespace
                value = value.strip()
                # Remove quotes if they exist
                if value.startswith('"') or value.startswith("'"):
                    value = value[1:]
                if value.endswith('"') or value.endswith("'"):
                    value = value[:-1]
                # Sanitize the values and add them to the key value pairs
                key_value_pairs[key.strip()] = []
                for v in value.split(","):
                    clean_value = self.sanitize_value(v)
                    if clean_value and len(clean_value) > 0:
                        key_value_pairs[key.strip()].append(clean_value)
        params = [llm_response.params for k, _ in key_value_pairs.items()]
        return ParserOutput(
            raw_response=llm_response, output=key_value_pairs, llm_params=params
        )


class KeyValueListXMLParser(SanitizeParser):
    def parse(self, llm_response: LLMResponse) -> ParserOutput[Dict]:
        llm_response = super().parse(llm_response).raw_response
        content = llm_response.response
        if not content:
            return ParserOutput(
                raw_response=llm_response, output={}, llm_params=llm_response.params
            )
        content = content.replace("\n", "")
        key_value_pairs = dict()
        for split in content.split("<extraction>"):
            if "<key>" in split:
                key = split.split("<key>")[1].split("</key>")[0]
                if "<values>" in split:
                    values = split.split("<values>")[1].split("</values>")[0]
                    value_list = values.split("<value>")
                    value_list = [
                        self.sanitize_value(v.split("</value>")[0])
                        for v in value_list
                        if v
                    ]
                    value_list = [v for v in value_list if v]
                    key_value_pairs[key] = value_list
                else:
                    if "<value>" in split:
                        value = split.split("<value>")[1].split("</value>")[0]
                        clean_value = self.sanitize_value(value)
                        if clean_value and len(clean_value) > 0:
                            key_value_pairs[key] = [clean_value]
        params = [llm_response.params for k, _ in key_value_pairs.items()]
        return ParserOutput(
            raw_response=llm_response, output=key_value_pairs, llm_params=params
        )

    def construct_raw_response(self, structured_output: dict) -> str:
        raw_output = ""
        for key, values in structured_output.items():
            raw_output += f"<extraction><key>{key}</key>"
            if len(values) > 1:
                raw_output += "<values>"
                for value in values:
                    raw_output += f"<value>{value}</value>"
                raw_output += "</values>"
            else:
                raw_output += f"<value>{values[0] if len(values) == 1 else ''}</value>"
            raw_output += "</extraction>"
        return raw_output


class SynthesisStringParser(SanitizeParser):
    def parse(self, llm_response: LLMResponse) -> ParserOutput[Dict]:
        llm_response = super().parse(llm_response).raw_response
        content = llm_response.response
        clean_response = content.replace("\n\n", "\n").replace("\n", " ").strip()
        # Find any occurrence of (Abstract #) and replace it with [#] using regex (case-insensitive)
        clean_response = re.sub(r"Abstract (\d+)", r"[\1]", clean_response, flags=re.I)

        # If the response contains certain terms at its end, remove it
        clean_response = self._remove_references(clean_response)

        # Added to synthesis after translation
        clean_response = self._remove_translation_artifacts(clean_response)

        # Make sure there are no trailing whitespaces due to the removal of the above
        clean_response = clean_response.strip()

        return ParserOutput(
            raw_response=llm_response,
            output={"synthesis": clean_response},
            llm_params=llm_response.params,
        )

    @staticmethod
    def _remove_translation_artifacts(syn_response):
        if "I am a translation" in syn_response:
            syn_response = syn_response[: syn_response.index("I am a translation")]
        if "I am now ready to perform" in syn_response:
            syn_response = syn_response[
                : syn_response.index("I am now ready to perform")
            ]
        if "I am now waiting" in syn_response:
            syn_response = syn_response[: syn_response.index("I am now waiting")]
        if "That is all." in syn_response:
            syn_response = syn_response[: syn_response.index("That is all.")]
        if "The translation is done" in syn_response:
            syn_response = syn_response[: syn_response.index("The translation is done")]
        if "Note:" in syn_response:
            syn_response = syn_response[: syn_response.index("Note:")]
        return syn_response

    @staticmethod
    def _remove_references(syn_response):
        if "References:" in syn_response:
            syn_response = syn_response[: syn_response.index("References:")]
        if "Reference:" in syn_response:
            syn_response = syn_response[: syn_response.index("Reference:")]
        if "Reference(s):" in syn_response:
            syn_response = syn_response[: syn_response.index("Reference(s):")]
        if "Reference List" in syn_response:
            syn_response = syn_response[: syn_response.index("Reference List")]
        return syn_response
