from app import is_valid_filter_string
from app.services.errors import create_422_unprocessable_entity


def check_for_valid_filter_string(field: str):
    def wrapper(func):
        def inner(*args, **kwargs):
            parameter = args[0] if args else kwargs.get("request")
            if parameter is not None and getattr(parameter, field) is not None:
                if not is_valid_filter_string(getattr(parameter, field)):
                    raise create_422_unprocessable_entity(
                        f"The filed {field} is not a valid filter string"
                    )
            return func(*args, **kwargs)

        return inner

    return wrapper
