from typing import List
from uuid import UUID

from pydantic import BaseModel, Field
from qdrant_client import models

from app.modules.indexing.base import (
    FilterOperator,
    FilterType,
    IDocument,
    KeyValueFilter,
    RecordFilters,
    StorePageResponse,
)


class QdrantFilter(RecordFilters):
    def to_filter(self) -> models.Filter:
        """
        Convert the record filters to a Qdrant filter object.
        :return: The Qdrant filter object.
        """
        qdrant_filter = models.Filter()
        for filter_spec in self.on or []:
            filter_type = self._get_filter_type(filter_spec)
            filter_list = self._get_filter_list(qdrant_filter, filter_type)

            if filter_spec.operator == FilterOperator.IS_NULL:
                condition = self._create_null_condition(filter_spec)
            else:
                condition = self._create_field_condition(filter_spec)

            filter_list.append(condition)
            self._set_filter_list(qdrant_filter, filter_type, filter_list)

        return qdrant_filter

    @staticmethod
    def _get_filter_type(filter_spec: KeyValueFilter) -> FilterType:
        """
        Get the filter type from the KeyValueFilter object, defaulting to FilterType.MUST if not provided.
        :param filter_spec: The KeyValueFilter object.
        :return: The filter type.
        """
        return filter_spec.filter_type or FilterType.MUST

    @staticmethod
    def _get_filter_list(qdrant_filter: models.Filter, filter_type: FilterType) -> List:
        """
        Get the appropriate filter list (must, must_not, or should) from the Qdrant filter object.
        :param qdrant_filter: The Qdrant filter object.
        :param filter_type: The filter type.
        :return: The filter list.
        """
        type_map = {
            FilterType.MUST: "must",
            FilterType.MUST_NOT: "must_not",
            FilterType.SHOULD: "should",
        }
        return getattr(qdrant_filter, type_map[filter_type], []) or []

    @staticmethod
    def is_empty_condition(filter_spec: KeyValueFilter) -> models.IsEmptyCondition:
        """
        Check if the filter condition is empty.
        :param filter_spec: The KeyValueFilter object.
        :return: The IsEmptyCondition object.
        """
        return models.IsEmptyCondition(
            is_empty=models.PayloadField(key=filter_spec.key),
        )

    @staticmethod
    def _create_null_condition(filter_spec: KeyValueFilter) -> models.IsNullCondition:
        """
        Create an IsNullCondition object for the IS_NULL operator.
        :param filter_spec: The KeyValueFilter object.
        :return: The IsNullCondition object.
        """
        return models.IsNullCondition(
            is_null=models.PayloadField(key=filter_spec.key),
        )

    @staticmethod
    def _create_field_condition(filter_spec: KeyValueFilter) -> models.FieldCondition:
        """
        Create a FieldCondition object for all operators except IS_NULL.
        :param filter_spec: The KeyValueFilter object.
        :return: The FieldCondition object.
        """
        operator_map = {
            FilterOperator.EQUALS: (models.MatchValue, "value"),
            FilterOperator.EXCEPT: (models.MatchExcept, "except"),
            FilterOperator.ANY: (models.MatchAny, "any"),
            FilterOperator.LESS_THAN: (models.Range, "lt"),
            FilterOperator.LESS_THAN_EQUALS: (models.Range, "lte"),
            FilterOperator.GREATER_THAN: (models.Range, "gt"),
            FilterOperator.GREATER_THAN_EQUALS: (models.Range, "gte"),
            FilterOperator.LIKE: (models.MatchText, "text"),
        }

        filter_operator = filter_spec.operator or FilterOperator.EQUALS
        field_condition = models.FieldCondition(key=filter_spec.key)
        comparator_type, param = operator_map[filter_operator]
        value = (
            filter_spec.value
            if comparator_type not in [models.MatchExcept, models.MatchAny]
            else [filter_spec.value]
            if not isinstance(filter_spec.value, list)
            else filter_spec.value
        )
        setattr(
            field_condition,
            "range" if comparator_type is models.Range else "match",
            comparator_type(**{param: value}),
        )
        return field_condition

    @staticmethod
    def _set_filter_list(
        qdrant_filter: models.Filter, filter_type: FilterType, filter_list: List
    ):
        """
        Set the appropriate filter list (must, must_not, or should) in the Qdrant filter object.
        :param qdrant_filter: The Qdrant filter object.
        :param filter_type: The filter type.
        :param filter_list: The filter list to set.
        """
        type_map = {
            FilterType.MUST: "must",
            FilterType.MUST_NOT: "must_not",
            FilterType.SHOULD: "should",
        }
        setattr(qdrant_filter, type_map[filter_type], filter_list)

    @classmethod
    def universal_filter(cls) -> "QdrantFilter":
        return cls(
            on=[
                KeyValueFilter(
                    key="id",
                    operator=FilterOperator.IS_NULL,
                    filter_type=FilterType.MUST_NOT,
                )
            ]
        )

    @classmethod
    def id_filter(cls, doc_id: str) -> "QdrantFilter":
        return cls(
            on=[
                KeyValueFilter(
                    key="id",
                    value=doc_id,
                    operator=FilterOperator.EQUALS,
                    filter_type=FilterType.MUST,
                )
            ]
        )

    @classmethod
    def multiple_ids_filter(cls, doc_ids: List[str]) -> "QdrantFilter":
        return cls(
            on=[
                KeyValueFilter(
                    key="id",
                    value=doc_ids,
                    operator=FilterOperator.ANY,
                    filter_type=FilterType.MUST,
                )
            ]
        )


class DBpediaEntity(BaseModel):
    uri: str = Field(..., description="The URI of the DBpedia entity")
    surface_form: str = Field(..., description="The surface form of the DBpedia entity")


class EntityExtractions(BaseModel):
    dbpedia: List[DBpediaEntity] | None = Field(
        None, description="The DBpedia entities extracted from the item"
    )


class Extractions(BaseModel):
    entities: EntityExtractions = Field(..., description="The list of entities")


class QdrantDocument(IDocument):
    doi: str | None = Field(None, description="The DOI of the document")
    oai: str | None = Field(None, description="The OAI of the document")
    title: str = Field(..., description="The title of the document")
    date_published: str | None = Field(
        None, description="The date published of the document"
    )
    abstract: str | None = Field(None, description="The abstract of the document")
    year: int | str | None = Field(None, description="The year of the document")
    issn: str | None = Field(None, description="The ISSN of the document")
    full_text: str | None = Field(None, description="The full text of the document")
    subjects: List[str] | None = Field(
        None, description="A list of subjects of the document"
    )
    urls: List[str] | None = Field(None, description="A list of URLs of the document")
    identifiers: List[str] | None = Field(
        None, description="A list of identifiers of the document"
    )
    topics: List[str] | None = Field(
        None, description="A list of topics of the document"
    )
    download_url: str | None = Field(
        None, description="The download URL of the document"
    )
    full_text_identifier: str | None = Field(
        None, description="The full text identifier of the document"
    )
    pdf_hash_value: str | None = Field(
        None, description="The PDF hash value of the document"
    )
    raw_record_xml: str | None = Field(
        None, description="The raw record XML of the document"
    )
    journals: List[str] | None = Field(
        None, description="A list of journals of the document"
    )
    authors: List[str] | None = Field(
        None, description="A list of authors of the document"
    )
    publisher: str | None = Field(None, description="The publisher of the document")
    relations: List[str] | None = Field(
        None, description="A list of relations of the document"
    )
    contributors: List[str] | None = Field(
        None, description="A list of contributors of the document"
    )
    language: str | None = Field(None, description="The language of the document")
    citation_count: int | None = Field(
        None, description="The citation count of the document"
    )
    document_type: str | None = Field(None, description="The inferred document type")
    extractions: Extractions | None = Field(
        None, description="The automatic extractions from the item"
    )
    source: str | None = Field(None, description="The source of the document")

    def get_source_fields_as_text(self) -> str:
        field_content = self.title
        if self.abstract:
            field_content += " " + self.abstract
        if self.full_text:
            field_content += " " + self.full_text
        return field_content


class QdrantDocumentWithInternalId(BaseModel):
    document: QdrantDocument = Field(
        ..., description="The payload retrieved from Qdrant with its internal point ID"
    )
    internal_id: int | str = Field(
        ..., description="The internal point ID of the payload"
    )


class QdrantPageResponse(StorePageResponse, BaseModel):
    items: List[QdrantDocument] = Field(
        ..., description="The list of items retrieved from Qdrant"
    )
    total_hits: int = Field(
        ...,
        description="The total number of documents that can be retrieved from Qdrant",
    )
    has_more: bool = Field(
        ..., description="Whether there are more documents to be retrieved from Qdrant"
    )
    offset: int | str | UUID | None = Field(
        None,
        description="The offset to start fetching documents from, could be int, str, or UUID",
    )

    @classmethod
    def from_offset_and_points(
        cls, items: List[QdrantDocument], offset: int | str | None, total: int
    ) -> "QdrantPageResponse":
        return cls(
            items=items,
            total_hits=total,
            has_more=True
            if isinstance(offset, str) and any([char.isascii() for char in offset])
            else (offset or 0) + len(items) < total,
            offset=offset,
        )
