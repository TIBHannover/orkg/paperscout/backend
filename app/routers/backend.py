import http
from typing import Annotated

from fastapi import APIRouter, Query

from app.core.decorators import log
from app.models.backend import (
    BackendHealthResponse,
    BackendVersionResponse,
    QuestionsResponse,
    StatisticsResponse,
)
from app.routers.common import ResponseWrapper
from app.services import backend

router = APIRouter(prefix="/backend", tags=["Common Functionality"])


@router.get(
    "/stats",
    status_code=http.HTTPStatus.OK,
    response_model=StatisticsResponse,
)
@log(__name__)
def get_data_stats():
    """
    Get the statistics of the dataset.
    """
    return ResponseWrapper.wrap_response(
        payload=backend.get_stats(),
        type_=StatisticsResponse,
    )


router.get(
    "/version", status_code=http.HTTPStatus.OK, response_model=BackendVersionResponse
)


@router.get(
    "/questions/recent",
    status_code=http.HTTPStatus.OK,
    response_model=QuestionsResponse,
)
@log(__name__)
def get_recently_asked_questions(
    count: Annotated[
        int,
        Query(description="the number of recent questions to return"),
    ] = 5,
):
    """
    Get the recent questions.
    """
    return ResponseWrapper.wrap_response(
        payload=backend.get_recent_questions(limit=count),
        type_=QuestionsResponse,
    )


@router.get(
    "/questions/popular",
    status_code=http.HTTPStatus.OK,
    response_model=QuestionsResponse,
)
@log(__name__)
def get_popular_asked_questions(
    count: Annotated[
        int,
        Query(description="the number of popular questions to return"),
    ] = 5,
):
    """
    Get the recent questions.
    """
    return ResponseWrapper.wrap_response(
        payload=backend.get_popular_questions(limit=count),
        type_=QuestionsResponse,
    )


@router.get(
    "/health",
    status_code=http.HTTPStatus.OK,
    response_model=BackendHealthResponse,
)
@log(__name__)
def get_backend_health():
    """
    Get the health of the backend.
    """
    return ResponseWrapper.wrap_response(
        payload=backend.get_backend_health(),
        type_=BackendHealthResponse,
    )
