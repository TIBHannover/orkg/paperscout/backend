import unittest

from app.modules.statemachine.machines import Machine
from app.modules.statemachine.state import State


class TestStateMachine(unittest.TestCase):
    def test_machine_not_ready(self):
        machine = Machine()
        assert len(machine.states) == 0, "States should be empty"
        assert len(machine.transitions) == 0, "Transitions should be empty"
        assert machine.is_ready is False, "Machine should not be ready"

    def test_machine_ready(self):
        machine = Machine()
        state1 = State(machine)
        state2 = State(machine)
        machine.add_state(state1, is_start=True)
        machine.add_state(state2, from_state=state1)
        assert machine.is_ready is True, "Machine should be ready"
        assert len(machine.states) == 2, "States should have 2 states"
        assert len(machine.transitions) == 1, "Transitions should have 1 transition"

    def test_machine_conditional_transition(self):
        machine = Machine()
        state1 = State(machine)
        state2 = State(machine)
        state3 = State(machine)
        machine.add_state(state1, is_start=True)
        machine.add_state(state2, from_state=state1, condition=lambda: True)
        machine.add_state(state3, from_state=state1, condition=lambda: False)

        machine.run()

        assert machine.current_state == state2, "Current state should be state2"

    def test_machine_input_output_pipelining(self):
        class TestMachine(Machine):
            count: int

            def __init__(self, start_count: int):
                super().__init__()
                self.count = start_count

        class TestState(State):
            def run(self):
                self.machine.count = self.machine.count + 1

        machine = TestMachine(start_count=1)
        state1 = TestState(machine)
        state2 = TestState(machine)
        state3 = TestState(machine)
        machine.add_state(state1, is_start=True)
        machine.add_state(state2, from_state=state1)
        machine.add_state(state3, from_state=state2)

        machine.run()

        assert machine.current_state == state3, "Current state should be state3"
        assert machine.count == 4, "Output should be 4"
