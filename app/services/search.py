from typing import Dict, List

from app import FilterString
from app.core import settings
from app.models.search import (
    CountRequest,
    ExploreRequest,
    IndexRequest,
    RecommendRequest,
    SearchRequest,
)
from app.modules.caching.manager import CacheManager
from app.modules.indexing.indexer import Indexer
from app.modules.indexing.vectordb.models import QdrantDocument, QdrantPageResponse
from app.services import check_for_valid_filter_string
from app.services.errors import create_404_document_not_found

indexer = Indexer()


def index_new_document(document: IndexRequest) -> bool:
    """
    Index a new document in the vector database.
    :param document: The document to index.
    :return: True if successful, False otherwise.
    """
    indexer.index_document(document=document.model_dump())
    return True


def index_new_documents(documents: List[IndexRequest]) -> bool:
    """
    Index a new document in the vector database.
    :param documents: The document to index.
    :return: True if successful, False otherwise.
    """
    indexer.index_documents(documents=[d.model_dump() for d in documents])
    return True


@check_for_valid_filter_string("filter")
def vector_search(request: SearchRequest) -> QdrantPageResponse:
    """
    Perform a KNN search on the vector database.
    :param request: The search request.
    :return: The search response.
    """
    if not settings.general.is_test_environment():
        CacheManager().add_question_to_cache(request.query)
    return indexer.vector_search(
        text=request.query,
        limit=request.limit,
        criteria=FilterString(request.filter) if request.filter else None,
        offset=request.offset,
        focus=request.focus,
    )


def retrieve_single_document(document_id: str) -> QdrantDocument:
    """
    Retrieve documents from the vector database.
    :param document_id: The document ID.
    :return: The search response.
    """
    search_result = indexer.retrieve_documents(
        document_id=document_id,
    )
    if not search_result or not search_result.items:
        raise create_404_document_not_found(document_id)
    return search_result.items[0]


@check_for_valid_filter_string("filter")
def retrieve_documents(request: ExploreRequest) -> QdrantPageResponse:
    """
    Retrieve documents from the vector database.
    :param request: The search request.
    :return: The search response.
    """
    return indexer.retrieve_documents(
        criteria=FilterString(request.filter) if request.filter else None,
        limit=request.limit,
        offset=request.offset,
        focus=request.focus,
    )


@check_for_valid_filter_string("filter")
def count_documents(request: CountRequest) -> int:
    """
    Count documents from the vector database.
    :param request: The search request.
    :return: The search response.
    """
    return indexer.count_documents(
        criteria=FilterString(request.filter) if request.filter else None
    )


@check_for_valid_filter_string("filter")
def recommend_documents(request: RecommendRequest) -> List[QdrantDocument]:
    """
    Perform a KNN search on the vector database.
    :param request: The search request.
    :return: The search response.
    """
    search_result = indexer.recommend_documents(
        document_ids=request.document_ids,
        avoid_ids=request.avoid_ids,
        limit=request.limit,
        offset=request.offset,
        criteria=FilterString(request.filter) if request.filter else None,
    )
    return [QdrantDocument(**r.model_dump()) for r in search_result]


def index_info() -> Dict:
    """
    Get information about the indices.
    :return: The indices' information.
    """
    return indexer.get_info()


def reinitialize_store() -> bool:
    """
    This is for internal use only. It reinitializes the store.
    :return: True if successful, False otherwise.
    """
    return indexer.delete_all_documents() > 0
