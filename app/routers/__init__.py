FILTER_EXAMPLES = {
    "year equal to": {
        "summary": "year equal to a specific value",
        "description": "The `year` field equal to a specific value here `2023`",
        "value": "year = 2023",
    },
    "year greater than": {
        "summary": "year greater than a specific value",
        "description": "The `year` field greater than a specific value here `2013`",
        "value": "year > 2013",
    },
    "citations in range": {
        "summary": "citations in a specific range",
        "description": "The `citations` field in a specific range here `10` to `100`",
        "value": "citations > 10, <=100",
    },
    "title like": {
        "summary": "title like a specific value",
        "description": """The `title` field like a specific value here `machine learning`.
         This will use full text search index to find the documents.""",
        "value": 'title LIKE "machine learning"',
    },
    "language in list": {
        "summary": "language in a specific list",
        "description": "The `language` field in a specific list here `en` or `fr`",
        "value": 'language IN ["en", "fr"]',
    },
    "full text is null": {
        "summary": "full text is null",
        "description": "The `full_text` field is null",
        "value": "IS_NULL(full_text)",
    },
    "complex filter": {
        "summary": "complex filter",
        "description": """A complex filter with multiple fields.
         You can use `AND`, `OR` and `NOT` operators to combine the filters.
          Here `year` not equal to `2013` and `issbn` not in a specific list and `title` like a specific value""",
        "value": '(year != 2013 OR issbn NOT IN ["1231313", "1231355444"]) AND title LIKE "machine learning"',
    },
}
