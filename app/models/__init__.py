import uuid
from datetime import datetime

from pydantic import BaseModel


class TimeStampedResponse(BaseModel):
    uuid: uuid.UUID
    timestamp: datetime
