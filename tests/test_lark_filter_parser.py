import unittest

from qdrant_client import models

from app import FilterString, is_valid_filter_string


class TestLarkFilterParser(unittest.TestCase):
    def test_simple_grammar_parsing(self):
        filter_str = 'title LIKE "ORKG"'
        assert is_valid_filter_string(filter_str), "The filter string should be valid"

    def test_nested_cname_grammar_parsing(self):
        filter_str = 'some.nested.object LIKE "ORKG"'
        assert is_valid_filter_string(filter_str), "The filter string should be valid"

    def test_and_grammar_parsing(self):
        filter_str = "year = 2020 AND IS_NULL(author)"
        assert is_valid_filter_string(filter_str), "The filter string should be valid"

    def test_filter_eq_doi_parses(self):
        filter_str = 'doi = "10.1234/abcd"'
        assert is_valid_filter_string(filter_str), "The filter string should be valid"

    def test_filter_eq_float_fails(self):
        filter_str = "age = 3.145"
        assert not is_valid_filter_string(
            filter_str
        ), "The filter string should NOT be valid"

    def test_simple_grammar_to_filter(self):
        filter_str = FilterString('title LIKE "ORKG"')
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(key="title", match=models.MatchText(text="ORKG"))
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_and_grammar_to_filter(self):
        filter_str = FilterString("year = 2020 AND IS_NULL(author)")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(key="year", match=models.MatchValue(value=2020)),
                models.IsNullCondition(is_null=models.PayloadField(key="author")),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_and_or_combination_grammar_to_filter(self):
        filter_str = FilterString("year = 2020 AND (IS_NULL(author) OR IS_EMPTY(doi))")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(key="year", match=models.MatchValue(value=2020)),
            ],
            should=[
                models.IsNullCondition(is_null=models.PayloadField(key="author")),
                models.IsEmptyCondition(is_empty=models.PayloadField(key="doi")),
            ],
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_in_values_grammar_to_filter(self):
        filter_str = FilterString("year IN [2020, 2021]")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(
                    key="year", match=models.MatchAny(any=[2020, 2021])
                ),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_single_range_grammar_to_filter(self):
        filter_str = FilterString("year > 2020")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(key="year", range=models.Range(gt=2020)),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_double_range_grammar_to_filter(self):
        filter_str = FilterString("year > 2020, <=  2032")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(
                    key="year", range=models.Range(gt=2020, lte=2032)
                ),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_not_equal_grammar_to_filter(self):
        filter_str = FilterString("year != 2020")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(
                    key="year", match=models.MatchExcept(**{"except": [2020]})
                ),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_not_equal_string_grammar_to_filter(self):
        filter_str = FilterString('title != "ORKG"')
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(
                    key="title", match=models.MatchExcept(**{"except": ["ORKG"]})
                ),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"

    def test_not_in_operator_grammar_to_filter(self):
        filter_str = FilterString("year NOT IN [2020, 2021]")
        assert isinstance(
            filter_str.to_filter(), models.Filter
        ), "The filter string should be converted to a Qdrant filter object"

        qdrant_filter = models.Filter(
            must=[
                models.FieldCondition(
                    key="year", match=models.MatchExcept(**{"except": [2020, 2021]})
                ),
            ]
        )
        assert (
            filter_str.to_filter() == qdrant_filter
        ), "The filter string should be converted to the correct Qdrant filter object"
