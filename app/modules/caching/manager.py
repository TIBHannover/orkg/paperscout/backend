import hashlib
from typing import Dict, List

from pocketbase.utils import ClientResponseError
from singleton_decorator import singleton

from app.core import settings
from app.errors import ItemNotFoundError
from app.modules.caching.dialects import PocketBaseCache
from app.modules.caching.models import CollectionItem, Item, QuestionRecord, UsedPrompt
from app.modules.llms.base import ParserOutput
from app.modules.prompting.prompts import PromptIds, Prompts
from app.modules.utils import merge_dicts

ITEMS_COLLECTION_NAME = "itemsCache"
COLLECTION_ITEMS_COLLECTION_NAME = "collectionItems"
TOKENS_COLLECTION_NAME = "tokens"
QUESTIONS_COLLECTION_NAME = "questions"
PROMPTS_COLLECTION_NAME = "promptsCache"


@singleton
class CacheManager:
    _cache: PocketBaseCache

    @property
    def cache(self) -> PocketBaseCache:
        return self._cache

    def __init__(self):
        self._cache = PocketBaseCache(settings.cache.host)

    def check_row_extraction_in_cache(
        self,
        prompt_id: PromptIds,
        item_id: str,
        parameters: List,
        language: str | None,
        seed: int | None,
        should_fetch_prompts: bool,
    ) -> (ParserOutput | None, List[str], Dict):
        """
        Check if the result of a row extraction is in the cache.
        :param prompt_id: the prompt ID
        :param item_id: the item ID
        :param parameters: the parameters used to generate the response
        :param language: the language of the response (if explicitly set)
        :param seed: the seed used during extraction (if explicitly set)
        :param should_fetch_prompts: whether to fetch the prompts used
        :return: a partial or a complete response, a list of missing parameters, and the prompts used
        """
        # Create a filter for the cache query
        filters = f"(promptId='{prompt_id.value}' && itemId='{item_id}' && language="
        filters += f"'{language}'" if language else "null"
        filters += " && seed="
        filters += f"'{seed}'" if seed else "null"
        filters += ")"

        # Get the result from the cache
        result = self.cache.get_all(
            collection=ITEMS_COLLECTION_NAME,
            filters=filters,
        )

        # If the result is not empty, filter the items in the cache by the parameters
        if len(result) > 0:
            items_in_cache = [Item.model_validate(item.__dict__) for item in result]
            items_in_cache = list(
                filter(lambda item: item.parameter in parameters, items_in_cache)
            )
            prompts = dict()
            if should_fetch_prompts:
                for item in items_in_cache:
                    prompts[item.parameter] = UsedPrompt.model_validate(
                        self.cache.search(
                            collection=PROMPTS_COLLECTION_NAME,
                            filters=f"(produced ~ '{item.internal_id}')",
                        )
                        .items[0]
                        .__dict__
                    )
            merged_response = merge_dicts(
                *[item.llm_response for item in items_in_cache]
            )
            merged_llm_params = []
            for k, _ in merged_response.items():
                # get the item in the item cache that has the parameter k
                item = next(item for item in items_in_cache if item.parameter == k)
                merged_llm_params.append(item.generation_parameters)
            return (
                ParserOutput(
                    raw_response=None,
                    output=merged_response,
                    llm_params=merged_llm_params,
                ),
                list(
                    set(parameters) - set([item.parameter for item in items_in_cache])
                ),
                prompts,
            )
        return None, [], {}

    def check_if_abstract_synthesis_in_cache(
        self,
        prompt_id: PromptIds,
        item_ids: List[int | str],
        question: str,
        language: str | None,
        seed: int | None,
        should_fetch_prompts: bool,
    ) -> (Item | None, Dict):
        """
        Check if the result of an abstract synthesis is in the cache.
        :param prompt_id: the prompt ID
        :param item_ids: a list of item IDs
        :param question: the research question
        :param language: the language of the response (if explicitly set)
        :param seed: the seed used during synthesis (if explicitly set)
        :param should_fetch_prompts: whether to fetch the prompts used
        :return: the item if it is in the cache, otherwise None, and the prompts used
        """
        ids_string = ",".join([str(item_id) for item_id in sorted(item_ids)])
        key = f"{ids_string}_{self._sanitize_parameter_for_pocketbase(question)}"
        filters = f"(promptId='{prompt_id.value}' && parameter='{key}' && language="
        filters += f"'{language}'" if language else "null"
        filters += " && seed="
        filters += f"'{seed}'" if seed else "null"
        filters += ")"

        result = self.cache.search(
            collection=ITEMS_COLLECTION_NAME,
            filters=filters,
        )
        if result.has_items:
            item_in_cache = Item.model_validate(result.items[0].__dict__)
            prompts_used = {}
            if should_fetch_prompts:
                prompts_used = {
                    "synthesis": UsedPrompt.model_validate(
                        self.cache.search(
                            collection=PROMPTS_COLLECTION_NAME,
                            filters=f"(produced ~ '{item_in_cache.internal_id}')",
                        )
                        .items[0]
                        .__dict__
                    )
                }
            return item_in_cache, prompts_used
        return None, {}

    @staticmethod
    def _sanitize_parameter_for_pocketbase(parameter: str) -> str:
        """
        Sanitize the parameter for Pocketbase.
        :param parameter: the parameter
        :return: the sanitized parameter
        """
        return parameter.replace("'", "\\'").replace('"', '\\"')

    def add_row_extraction_to_cache(
        self,
        prompt_id: int,
        item_id: str,
        llm_response: ParserOutput[Dict],
        parameters: Dict[str, Dict],
        is_collection_item: bool,
        language: str | None = None,
        seed: int | None = None,
    ) -> List[Item]:
        """
        Add the row extraction response to the cache
        :param prompt_id: the prompt ID
        :param item_id: the item ID
        :param llm_response: the LLM response output with the parsed output as a dictionary
        :param parameters: Dict of parameters and their llm generation parameters
        :param is_collection_item: whether the item is a collection item
        :param language: the language of the response (if explicitly set)
        :param seed: the seed used during extraction (if explicitly set)
        :return: the item that was added to the cache
        """
        language_overwrite_filter = "&& language="
        language_overwrite_filter += "null" if language is None else f"'{language}'"
        seed_overwrite_filter = "&& seed="
        seed_overwrite_filter += "null" if seed is None else f"'{seed}'"
        return [
            Item.model_validate(item.__dict__)
            for item in [
                self.cache.upsert(
                    collection=ITEMS_COLLECTION_NAME,
                    item={
                        "itemId": item_id,
                        "promptId": prompt_id,
                        "parameter": param,
                        "llmResponse": {param: llm_response.output.get(param, [])},
                        "isCollectionItem": is_collection_item,
                        "language": language,
                        "seed": str(seed) if seed else None,
                        "generationParameters": generation_params,
                    },
                    overwrite_on=f"(promptId='{prompt_id}' && itemId='{item_id}' "
                    f"&& parameter='{self._sanitize_parameter_for_pocketbase(param)}' "
                    f"{language_overwrite_filter} {seed_overwrite_filter})",
                )
                for param, generation_params in parameters.items()
            ]
        ]

    def add_abstract_synthesis_to_cache(
        self,
        prompt_id: int,
        item_ids: List[int | str],
        question: str,
        llm_response: ParserOutput[Dict],
        mappings: Dict[str, Dict[int, str | int]],
        language: str | None = None,
        seed: int | None = None,
    ) -> Item:
        """
        Add the abstract synthesis response to the cache
        :param prompt_id: the prompt ID
        :param item_ids: the item IDs
        :param question: the research question
        :param llm_response: the LLM response
        :param mappings: the mappings of item IDs to citations
        :param language: the language of the response (if explicitly set)
        :param seed: the seed used during synthesis (if explicitly set)
        :return: the item that was added to the cache
        """
        ids_string = ",".join([str(item_id) for item_id in sorted(item_ids)])
        key = f"{ids_string}_{self._sanitize_parameter_for_pocketbase(question)}"
        payload_key = f"{ids_string}_{question}"
        overwrite_filter = f"(promptId='{prompt_id}' && parameter='{key}' && language="
        overwrite_filter += f"'{language}'" if language else "null"
        overwrite_filter += " && seed="
        overwrite_filter += f"'{seed}'" if seed else "null"
        overwrite_filter += ")"

        llm_response.output.update(mappings)

        return Item.model_validate(
            self.cache.upsert(
                collection=ITEMS_COLLECTION_NAME,
                item={
                    "itemId": None,
                    "promptId": prompt_id,
                    "parameter": payload_key,
                    "llmResponse": llm_response.output,
                    "isCollectionItem": False,
                    "language": language,
                    "seed": str(seed) if seed else None,
                    "generationParameters": llm_response.llm_params,
                },
                overwrite_on=overwrite_filter,
            ).__dict__
        )

    def add_prompt_to_cache(
        self, prompt_id: PromptIds, cached_items_ids: List[str], variables: Dict
    ) -> UsedPrompt:
        # Fetch all required components
        prompt_template = Prompts.PROMPT_MAP[prompt_id]["template"]
        system_prompt = prompt_template.system_prompt
        user_prompt = prompt_template.user_prompt

        # compute hash of the components
        string_to_hash = f"{system_prompt}\n{user_prompt}\n{variables}"
        hash_value = hashlib.md5(string_to_hash.encode("utf-8")).hexdigest()

        # Check if the prompt is already in the cache
        result = self.cache.search(
            collection=PROMPTS_COLLECTION_NAME,
            filters=f"(hashCode='{hash_value}')",
        )

        if result.has_items:
            # The prompt is already in the cache, only updated the produced list to contain the union
            # of the old and new produced items
            prompt_record = result.items[0]
            prompt_record.produced = list(
                set(prompt_record.produced + cached_items_ids)
            )
            item = prompt_record.__dict__
            item.pop("created")
            item.pop("updated")
            return UsedPrompt.model_validate(
                self.cache.upsert(
                    collection=PROMPTS_COLLECTION_NAME,
                    item=item,
                    overwrite_on=f"(hashCode='{hash_value}')",
                ).__dict__
            )

        # The prompt is not in the cache, add it
        return UsedPrompt.model_validate(
            self.cache.upsert(
                collection=PROMPTS_COLLECTION_NAME,
                item={
                    "system": system_prompt,
                    "user": user_prompt,
                    "variables": variables,
                    "hashCode": hash_value,
                    "produced": cached_items_ids,
                },
            ).__dict__
        )

    def get_prompt_by_item_internal_id(self, internal_id: str) -> UsedPrompt | None:
        results = self.cache.search(
            collection=PROMPTS_COLLECTION_NAME,
            filters=f"(produced='{internal_id}')",
        )
        if results.has_items:
            return UsedPrompt.model_validate(results.items[0].__dict__)
        return None

    def get_collection_item_from_cache(self, collection_item_id: str) -> CollectionItem:
        """
        Get a collection item from the cache.
        :param collection_item_id: the collection item ID
        :return: the collection item
        """
        try:
            result = self.cache.get(
                collection=COLLECTION_ITEMS_COLLECTION_NAME, item_id=collection_item_id
            )
        except ClientResponseError:
            raise ItemNotFoundError(
                f"Collection item with ID {collection_item_id} not found"
            )
        return CollectionItem.model_validate(result.__dict__)

    def check_if_token_is_valid(self, token: str) -> bool:
        """
        Check if the token is valid.
        :param token: the token
        :return: True if the token is valid, otherwise False
        """
        try:
            token_record = self.cache.get(
                collection=TOKENS_COLLECTION_NAME,
                item_id=token,
            )
            # Found the token, check if it is active
            return token_record.active
        except ClientResponseError:
            # That means a 404 was returned, so the token is not valid
            return False

    def add_question_to_cache(self, question: str) -> QuestionRecord:
        """
        Add a question to the cache, or update the frequency if it already exists.
        :param question: the question to add
        :return: the question record
        """
        # Check if the question is already in the cache
        sanitized_question = self._sanitize_parameter_for_pocketbase(question)
        result = self.cache.search(
            collection=QUESTIONS_COLLECTION_NAME,
            filters=f"(question='{sanitized_question}')",
        )
        if result.has_items:
            # The question is already in the cache, update the frequency
            question_record = QuestionRecord.model_validate(result.items[0].__dict__)
            question_record.frequency += 1
            return QuestionRecord.model_validate(
                self.cache.upsert(
                    collection=QUESTIONS_COLLECTION_NAME,
                    item=question_record.__dict__,
                    overwrite_on=f"(question='{sanitized_question}')",
                ).__dict__
            )

        # The question is not in the cache, add it
        return QuestionRecord.model_validate(
            self.cache.upsert(
                collection=QUESTIONS_COLLECTION_NAME,
                item={"question": question, "frequency": 1, "nsfw": False},
            ).__dict__
        )

    def get_recent_questions(self, limit: int) -> List[QuestionRecord]:
        """
        Get the most recent questions from the cache.
        :param limit: the maximum number of questions to return
        :return: the list of questions
        """
        result = self.cache.search(
            collection=QUESTIONS_COLLECTION_NAME,
            sort="-updated",
            per_page=limit,
            filters="(nsfw=false)",
        )
        return [QuestionRecord.model_validate(item.__dict__) for item in result.items]

    def get_popular_questions(self, limit: int) -> List[QuestionRecord]:
        """
        Get the most popular questions from the cache.
        :param limit: the maximum number of questions to return
        :return: the list of questions
        """
        result = self.cache.search(
            collection=QUESTIONS_COLLECTION_NAME,
            sort="-frequency",
            per_page=limit,
            filters="(nsfw=false)",
        )
        return [QuestionRecord.model_validate(item.__dict__) for item in result.items]

    def count_users(self) -> int:
        """
        Count the number of registered users in Pocketbase
        :return: the number of registered users
        """
        return self.cache.count(collection="users")

    def count_collections(self) -> int:
        """
        Count the number of collections in Pocketbase
        :return: the number of collections
        """
        return self.cache.count(collection="collections")

    def count_collection_items(self) -> int:
        """
        Count the number of collection items in Pocketbase
        :return: the number of collection items
        """
        return self.cache.count(collection=COLLECTION_ITEMS_COLLECTION_NAME)

    def count_saved_searches(self) -> int:
        """
        Count the number of saved searches in Pocketbase
        :return: the number of saved searches
        """
        return self.cache.count(collection="savedSearches")

    def count_shared_links(self) -> int:
        """
        Count the number of shared links in Pocketbase
        :return: the number of shared links
        """
        return self.cache.count(collection="sharedLinks")

    def count_cache_hits(self) -> int:
        """
        Count the of cache hits in Pocketbase
        :return: the number of cache hits
        """
        return self.cache.count(collection=ITEMS_COLLECTION_NAME)

    def count_questions(self) -> int:
        """
        Count the number of questions in Pocketbase
        :return: the number of asked questions
        """
        return self.cache.count(collection=QUESTIONS_COLLECTION_NAME)
