import json
import os
import random
from dataclasses import dataclass
from datetime import datetime
from typing import Literal

import click
import torch
from datasets import Dataset, load_dataset
from peft import LoraConfig, TaskType, get_peft_model
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    BitsAndBytesConfig,
    DataCollatorForLanguageModeling,
)
from trl import SFTConfig, SFTTrainer

# Create type alias for literals of the model name
MODEL = Literal["mistral", "phi3", "phi3.5"]
MODEL_MAP = {
    "mistral": "mistralai/Mistral-7B-Instruct-v0.3",
    "phi3": "microsoft/Phi-3-mini-128k-instruct",
    "phi3.5": "microsoft/Phi-3.5-mini-instruct",
}


@dataclass
class Components:
    model: AutoModelForCausalLM
    tokenizer: AutoTokenizer
    config: LoraConfig
    dataset: Dataset
    trainer: SFTTrainer
    collator: DataCollatorForLanguageModeling


def apply_model_prompt_template(
    model: MODEL, system_prompt: str, user_prompt: str, response: str
) -> str:
    if model == "mistral":
        return f"""<s>[INST]{system_prompt} {user_prompt}[/INST]{response}</s>"""
    elif model in ["phi3", "phi3.5"]:
        return f"""<|system|>\n{system_prompt}<|end|>\n<|user|>{user_prompt}<|end|>\n<|assistant|>\n{response}<|end|>"""
    else:
        raise ValueError("Invalid model")


def convert_summarization_data_to_language_modeling_data(
    jsonl_file: str, output_file: str, model: MODEL
):
    with open(jsonl_file, "r", encoding="utf8") as f:
        data = [json.loads(line) for line in f]
    with open(output_file, "w", encoding="utf8") as f:
        for item in data:
            question = item["question"]
            documents = item["documents"]
            language = item["language"]
            synthesis = item["synthesis"]
            level = item["level"]

            system = """Generate a comprehensive answer to the given research question (but no more than three/four sentences)
solely based on the content provided.
Cite the number of the content referenced for each claim like this:
[1] for a single reference or [2][3] for multiple references.
Generate the synthesis in the "{}" language, and phrase the complexity of the text to be suitable for a {}"""
            system = system.format(language, level)

            user = """# Research Question: {}
# Abstracts:
{}

# Answer with inline-citations as [#] in {}:
""".format(
                question,
                "\n\n".join(
                    [
                        f"Abstract #{i+1}:\n {doc['title']}\n{doc['abstract']}"
                        for i, doc in enumerate(documents)
                    ]
                ),
                language,
            )

            model_prompt = apply_model_prompt_template(model, system, user, synthesis)

            f.write(json.dumps({"entry": model_prompt}, ensure_ascii=False) + "\n")


def _create_value_tag(value: dict | str | None) -> str:
    if value is None:
        return "<value></value>"
    if isinstance(value, str):
        return f"<value>{value}</value>"
    if isinstance(value, dict):
        if "text" not in value:
            return "<value></value>"
        xml = f"<value>{value['text']}</value>"
        sources = value.get("source", [])
        if sources:
            for source in sources:
                xml += f"<source>{source}</source>"
            return xml
    return ""


def create_extraction_xml_format(
    extraction: dict, used_properties_translated: dict, question: str
) -> str:
    xml = ""
    for key, value in extraction.items():
        if key not in used_properties_translated:
            continue
        if key.lower() == "answer":
            xml += "<extraction>"
            xml += f"<key>{question}</key>"
            xml += _create_value_tag(value)
            xml += "</extraction>\n"
            continue
        translated_name = used_properties_translated[key]["name"]
        xml += "<extraction>"
        if value is None:
            xml += f"<key>{translated_name}</key>"
            xml += _create_value_tag(value)
        elif isinstance(value, list) and len(value) > 0:
            xml += f"<key>{translated_name}</key>"
            xml += "<values>"
            for v in value:
                xml += _create_value_tag(v)
            xml += "</values>"
        else:
            xml += f"<key>{translated_name}</key>"
            v = (
                value[0]
                if isinstance(value, list)
                and not isinstance(value, str)
                and len(value) > 0
                else value
            )
            xml += _create_value_tag(v)
        xml += "</extraction>\n"
    return xml


def convert_extraction_data_to_language_modeling_data(
    jsonl_file: str,
    output_file: str,
    model: MODEL,
    properties_file: str,
    questions_file: str,
):
    with open(jsonl_file, "r", encoding="utf8") as f:
        data = [json.loads(line) for line in f]

    with open(properties_file, "r", encoding="utf8") as f:
        properties = json.load(f)

    def sanitize_properties_keys(properties_dict):
        new_properties = {}
        for key, value in properties_dict.items():
            new_key = key.replace("_", "").replace(";", "").replace(" ", "").lower()
            if new_key.endswith("s"):
                new_key = new_key[:-1]
            new_properties[new_key] = value
        return new_properties

    properties = sanitize_properties_keys(properties)

    with open(questions_file, "r", encoding="utf8") as f:
        questions = json.load(f)

    with open(output_file, "w", encoding="utf8") as f:
        for item in data:
            question = item["question"]
            document = item["document"]
            language = item["language"]
            level = item["level"]
            extraction = item["extraction"]

            system = """You are an analysis-support bot that operates on scholarly documents and follows instructions.
You will get as an input: the research paper content and a set of properties/criteria to look for.
You will extract the values corresponding to the list of provided predicates.
Use the ORKG Ask structured information extraction XML-based format.
The extractions must be in the "{}" language and the complexity of the language should be for a "{}"."""
            user = """## What to extract:\n{}.\n ## Extraction source:\n{}"""

            system_prompt = system.format(language, level)

            def fix_extraction_keys(extraction_dict):
                new_extraction = {}
                # Fix values like sample_size to Sample Size
                for key, value in extraction_dict.items():
                    new_key = (
                        key.replace("_", "").replace(";", "").replace(" ", "").lower()
                    )
                    if new_key.endswith("s"):
                        new_key = new_key[:-1]
                    new_extraction[new_key] = value
                return new_extraction

            extraction = fix_extraction_keys(extraction)
            used_properties_english = extraction.keys()

            used_properties_translated = {
                prop: {
                    "name": properties[prop]["name"][language],
                    "description": properties[prop]["description"][language]
                    if random.random() > 0.7
                    else None,
                }
                for prop in used_properties_english
                if prop.lower() != "answer" and prop in properties
            }
            if language != "English":
                translated_question = questions.get(question, {language: question})[
                    language
                ]
            else:
                translated_question = question

            properties_string = "\n".join(
                [
                    f"- {prop['name']}: {prop['description']}"
                    if prop["description"]
                    else f"- {prop['name']}."
                    for prop in used_properties_translated.values()
                ]
            )
            # Append the question to the properties
            properties_string += f"\n- {translated_question}."

            extraction_source = f"{document['title']}\n{document['abstract']}"
            if "full_text" in document and document["full_text"]:
                extraction_source += f"\n{document['full_text']}"

            user_prompt = user.format(properties_string, extraction_source)

            model_response = create_extraction_xml_format(
                extraction, used_properties_translated, translated_question
            )

            model_prompt = apply_model_prompt_template(
                model, system_prompt, user_prompt, model_response
            )

            f.write(json.dumps({"entry": model_prompt}, ensure_ascii=False) + "\n")


def get_training_components(
    model: MODEL,
    output_directory: str,
    input_data: str,
    load_in_8bits: bool,
    max_length: int = 512,
    hf_hub_key: str | None = None,
    device: str = "auto",
    seed: int = 42,
    dataset_ratio: float = 1.0,
) -> Components:
    # Check GPU capability and set precision
    if torch.cuda.get_device_capability()[0] >= 8:
        torch_dtype = (
            torch.bfloat16
        )  # Use bfloat16 for better performance on recent GPUs
        attn_implementation = (
            "flash_attention_2"  # Efficient attention mechanism for newer GPUs
        )
    else:
        torch_dtype = torch.float16  # Default to float16
        attn_implementation = "eager"  # Fallback attention implementation

    padding_side = "right"

    ml_model = AutoModelForCausalLM.from_pretrained(
        MODEL_MAP[model],
        device_map=device,
        quantization_config=None
        if not load_in_8bits
        else BitsAndBytesConfig(load_in_8bit=load_in_8bits),
        torch_dtype=torch_dtype,
        token=hf_hub_key,
        attn_implementation=attn_implementation,
    )

    # Set padding token ID
    ml_model.config.pad_token_id = ml_model.config.eos_token_id

    tokenizer = AutoTokenizer.from_pretrained(
        MODEL_MAP[model], add_eos_token=True, token=hf_hub_key
    )
    tokenizer.pad_token = (
        tokenizer.unk_token
    )  # use unk rather than eos token to prevent endless generation
    tokenizer.pad_token_id = tokenizer.convert_tokens_to_ids(tokenizer.pad_token)
    tokenizer.padding_side = padding_side

    # Load the dataset
    dataset = load_dataset("json", data_files=input_data)
    # Use only the specified ration of the dataset
    dataset = dataset["train"].take(int(len(dataset["train"]) * dataset_ratio))
    # split the dataset into training and validation
    dataset = dataset.train_test_split(test_size=0.1, seed=seed)

    def tokenizer_function(examples):
        return tokenizer(
            examples["entry"],
            truncation=True,
            max_length=max_length,
            padding="longest",
        )

    tokenized_dataset = dataset.map(
        tokenizer_function,
        batched=True,
        remove_columns=["entry"],
        num_proc=10,
    )
    tokenized_dataset.set_format(type="torch")

    peft_config = LoraConfig(
        task_type=TaskType.CAUSAL_LM,
        inference_mode=False,
        r=16,  # Rank of the adaptation matrices
        lora_alpha=32,  # LoRA scaling
        lora_dropout=0.05,  # Dropout rate
        target_modules="all-linear",
        bias="none",
    )

    ml_model = get_peft_model(ml_model, peft_config)

    # Enable gradient checkpointing for memory efficiency during training
    ml_model.gradient_checkpointing_enable()

    # Enable gradients for input layers (required for LoRA)
    ml_model.enable_input_require_grads()

    # Log the number of trainable parameters for inspection
    ml_model.print_trainable_parameters()

    if torch.cuda.device_count() > 1:  # If more than 1 GPU
        ml_model.is_parallelizable = True
        ml_model.model_parallel = True

    project = "ask-finetune"
    base_model_name = model
    run_name = f"{base_model_name}-{project}"
    output_dir = f"{output_directory}/{run_name}"

    data_collator = DataCollatorForLanguageModeling(
        tokenizer=tokenizer,
        mlm=False,
    )

    train_args = SFTConfig(
        output_dir=output_dir,
        overwrite_output_dir=True,
        warmup_ratio=0.2,
        per_device_train_batch_size=4,
        per_device_eval_batch_size=4,
        gradient_checkpointing=True,  # Accumulate gradients to simulate larger batch sizes
        gradient_accumulation_steps=1,
        eval_strategy="steps",  # Evaluate the model at regular intervals
        eval_steps=200,  # Evaluate every X steps
        optim="adamw_torch",  # Optimizer for model weights
        num_train_epochs=3,
        learning_rate=2.5e-5,
        weight_decay=0.01,
        logging_steps=20,
        save_steps=100,
        save_total_limit=1,
        seed=seed,
        logging_dir="./logs",
        bf16=True,  # Use bfloat16 for efficient training
        fp16=False,  # Do not use float16 (overridden by bfloat16)
        push_to_hub=False,
        run_name=f"{run_name}-{datetime.now().strftime('%Y-%m-%d-%H-%M')}",
        max_seq_length=max_length,
        dataset_text_field="input_ids",
        packing=False,
    )

    trainer = SFTTrainer(
        model=ml_model,
        train_dataset=tokenized_dataset["train"],
        eval_dataset=tokenized_dataset["test"],
        peft_config=peft_config,
        data_collator=data_collator,
        args=train_args,
    )

    ml_model.config.use_cache = False
    torch.cuda.empty_cache()

    return Components(
        model=ml_model,
        tokenizer=tokenizer,
        config=peft_config,
        dataset=tokenized_dataset,
        trainer=trainer,
        collator=data_collator,
    )


def finetune_orkg_ask_model(
    model: MODEL,
    output_directory: str,
    input_data: str,
    load_in_8bits: bool,
    max_length: int,
    hf_hub_key: str | None,
    device: str = "auto",
    seed: int = 42,
    dataset_ratio: float = 1.0,
):
    print(">>>>>>>>>>>>> Getting training components")
    components = get_training_components(
        model,
        output_directory,
        input_data,
        load_in_8bits,
        max_length,
        hf_hub_key,
        device,
        seed,
        dataset_ratio,
    )
    print(">>>>>>>>>>>>> Training the model")
    components.trainer.train()
    print(">>>>>>>>>>>>> Saving the model")
    components.model.save_pretrained(output_directory)
    components.tokenizer.save_pretrained(output_directory)
    try:
        print(">>>>>>>>>>>>> Evaluation")
        components.tokenizer.padding_side = "left"
        metrics = components.trainer.evaluate()
        metrics["eval_samples"] = len(components.dataset)
        components.trainer.log_metrics("eval", metrics)
        components.trainer.save_metrics("eval", metrics)
    except Exception as e:
        print(f"Error during evaluation: {e}")


@click.command()
@click.option(
    "--model",
    "-m",
    help="Model to finetune",
    required=True,
    type=click.Choice(list(MODEL_MAP.keys())),
)
@click.option(
    "--output-directory",
    "-o",
    help="Output directory for the finetuned model",
    required=True,
)
@click.option("--input-data", "-i", help="Input data in jsonl format", required=True)
@click.option("--load-in-8bits", "-l8", help="Load the model in 8 bits", is_flag=True)
@click.option(
    "--max-length", "-ml", help="Maximum length of the input data", default=512
)
@click.option(
    "--task",
    "-t",
    help="Task to perform",
    required=True,
    type=click.Choice(["synthesis", "extraction"]),
)
@click.option("--hf-hub-key", "-k", help="Hugging Face Hub API key", required=False)
@click.option(
    "--device", "-d", help="Device to use for training", required=False, default="auto"
)
@click.option(
    "--seed", "-s", help="Seed for reproducibility", required=False, default=42
)
@click.option(
    "--properties", "-p", help="Properties JSON file with translations", required=False
)
@click.option(
    "--questions", "-q", help="Questions JSON file with translations", required=False
)
@click.option(
    "--dataset-ratio",
    "-dr",
    help="Ratio of the dataset to use for training",
    required=False,
    default=1.0,
)
def main(
    model: MODEL,
    output_directory: str,
    input_data: str,
    load_in_8bits: bool,
    max_length: int,
    task: Literal["synthesis", "extraction"],
    hf_hub_key: str,
    device: str,
    seed: int,
    properties: str,
    questions: str,
    dataset_ratio: float,
):
    random.seed(seed)
    if task == "synthesis":
        interm_output_file = "./synthesis-processed.jsonl"
        if not os.path.exists(interm_output_file):
            print(
                ">>>>>>>>>>>>> Converting summarization data to language modeling data"
            )
            convert_summarization_data_to_language_modeling_data(
                input_data, interm_output_file, model
            )
        else:
            print(
                ">>>>>>>>>>>>> Intermediate output file already exists. Skipping conversion"
            )
    else:
        if not properties or not questions:
            raise ValueError(
                "Properties and questions files are required for the 'extraction' task"
            )
        interm_output_file = "./extraction-processed.jsonl"
        if not os.path.exists(interm_output_file):
            print(">>>>>>>>>>>>> Converting extraction data to language modeling data")
            convert_extraction_data_to_language_modeling_data(
                input_data, interm_output_file, model, properties, questions
            )
        else:
            print(
                ">>>>>>>>>>>>> Intermediate output file already exists. Skipping conversion"
            )
    finetune_orkg_ask_model(
        model,
        output_directory,
        interm_output_file,
        load_in_8bits,
        max_length,
        hf_hub_key,
        device,
        seed,
        dataset_ratio,
    )


if __name__ == "__main__":
    main()
