from typing import Dict, List, Literal, NewType

from pydantic import BaseModel, Field

from app.models import TimeStampedResponse

KVPReproducibility = NewType(
    "KVPReproducibility", "ExtractItemValuesFromPropertiesResponse.Reproducibility"
)
SynReproducibility = NewType(
    "SynReproducibility",
    "SynthesisAnswerOfQuestionFromAbstractsResponse.Reproducibility",
)


class ExtractItemValuesFromPropertiesRequest(BaseModel):
    item_id: int | str | None = Field(
        None, description="The ID of the item to extract properties from"
    )
    collection_item_id: str | None = Field(
        None,
        description="The custom collection item ID to extract the values from",
    )
    properties: List[str] = Field(
        ..., description="The properties to extract from the item"
    )
    invalidate_cache: bool = Field(
        False,
        description="Whether to invalidate the cache for the item",
    )
    response_language: str | None = Field(
        None,
        description="The language of the response",
    )
    seed: int | None = Field(
        None,
        description="The seed to use during extraction",
    )


class ReproducibilityPrompt(BaseModel):
    system: str = Field(..., description="The system that used the prompt")
    user: str = Field(..., description="The user that used the prompt")
    variables: Dict = Field(..., description="The variables used with the prompt")


class ExtractItemValuesFromPropertiesResponse(TimeStampedResponse):
    class GenerationParameters(BaseModel):
        indexes: Dict[str, int]
        collection: List[Dict]

    class PromptsUsed(BaseModel):
        indexes: Dict[str, int]
        collection: List[ReproducibilityPrompt]

    class Reproducibility(BaseModel):
        parameters: "ExtractItemValuesFromPropertiesResponse.GenerationParameters" = (
            Field(..., description="The llm generation parameters")
        )
        prompts: "ExtractItemValuesFromPropertiesResponse.PromptsUsed" = Field(
            ..., description="The prompt used for generation"
        )

    class Payload(BaseModel):
        item_id: int | str = Field(
            ..., description="The ID of the item to extract properties from"
        )
        properties: List[str] = Field(
            ..., description="The properties to extract from the item"
        )
        values: Dict[str, str | List[str | int | float] | int | float] = Field(
            ..., description="The extracted values for the properties"
        )
        extra: Dict[str, str | List[str | int | float] | int | float] = Field(
            ..., description="Extra values detected by the LLM"
        )
        seed: int | None = Field(
            None, description="The seed used during extraction (if any)"
        )
        reproducibility: KVPReproducibility | None = Field(
            None, description="The reproducibility parameters for the extraction"
        )

    payload: Payload


class SynthesisAnswerOfQuestionFromAbstractsRequest(BaseModel):
    item_ids: List[int | str] = Field(
        ..., description="The item IDs to synthesize the answer from"
    )
    question: str = Field(..., description="The question to synthesize the answer for")
    invalidate_cache: bool = Field(
        False,
        description="Whether to invalidate the cache for the item",
    )


class SynthesisAnswerOfQuestionFromAbstractsResponse(TimeStampedResponse):
    class Reproducibility(BaseModel):
        parameters: Dict = Field(..., description="The llm generation parameters")
        prompt: ReproducibilityPrompt = Field(
            ..., description="The prompt used for generation"
        )

    class Payload(BaseModel):
        items_mapping: Dict[int, str | int] = Field(
            ..., description="The item IDs mappings to the citations"
        )
        collection_items_mapping: Dict[int, str | int] = Field(
            ..., description="The collection item IDs mappings to the citations"
        )
        question: str = Field(
            ..., description="The question to synthesize the answer for"
        )
        synthesis: str = Field(
            ..., description="The synthesized answer for the question"
        )
        seed: int | None = Field(
            None, description="The seed used during synthesis (if any)"
        )
        reproducibility: SynReproducibility | None = Field(
            None, description="The reproducibility parameters for the synthesis"
        )

    payload: Payload


class LLMGenerateRequest(BaseModel):
    prompt: str = Field(..., description="The prompt to generate the response for")
    system: str | None = Field(
        None, description="The system to use for generation. None by default."
    )
    model: str | None = Field(
        None,
        description="The model to use for generation. If not provided, the default model will be used",
    )
    stream: bool = Field(
        True, description="Whether to stream the output or not (default: True)"
    )
    temperature: float = Field(
        0.5, description="The temperature to use for generation. Default is 0.5."
    )
    top_k: int = Field(
        10, description="The number of tokens to consider for the top-k sampling"
    )
    top_n: int = Field(
        5, description="The number of tokens to consider for the top-n sampling"
    )
    top_p: float = Field(0.95, description="The nucleus sampling")
    truncate: int | None = Field(
        150, description="The maximum number of tokens to truncate. Default is 150."
    )
    context_size: int | None = Field(
        None, description="The context size to use for generation"
    )
    context: List[int] | None = Field(
        None, description="The context for the short-term memory"
    )
    seed: int | None = Field(None, description="The seed to use for generation")


class LLMChatMessage(BaseModel):
    role: Literal["user", "assistant", "system"] = Field(
        ..., description="The role of the message"
    )
    content: str = Field(..., description="The content of the message")


class LLMChatRequest(BaseModel):
    model: str | None = Field(
        None,
        description="The model to use for generation. If not provided, the default model will be used",
    )
    stream: bool = Field(
        True, description="Whether to stream the output or not (default: True)"
    )
    messages: List[LLMChatMessage] = Field(
        ..., description="The messages to generate the response for"
    )
    temperature: float = Field(
        0.5, description="The temperature to use for generation. Default is 0.5."
    )
    top_p: float = Field(0.95, description="The nucleus sampling")
    truncate: int = Field(150, description="The maximum number of tokens to truncate")
    stop: str | None = Field(None, description="The stop token to use for generation")
    seed: int | None = Field(None, description="The seed to use for generation")
    frequency_penalty: float = Field(1, description="The frequency penalty")
    presence_penalty: float = Field(0.1, description="The presence penalty")


class LLMRawResponse(TimeStampedResponse):
    payload: Dict
