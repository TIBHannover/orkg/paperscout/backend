from abc import abstractmethod


class State:
    """
    Represents a state in a state machine.
    """

    machine = None

    def __init__(self, machine):
        self.machine = machine

    @abstractmethod
    def run(self):
        pass
