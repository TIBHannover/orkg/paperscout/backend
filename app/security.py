from fastapi import Request, status
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import PlainTextResponse

from app.core import settings
from app.modules.caching.manager import CacheManager


class AskAuthMiddleware(BaseHTTPMiddleware):
    @staticmethod
    def _is_valid_token(token: str | None, verb: str, route: str) -> bool:
        # If the verb is not protected, return True
        if (
            settings.security.protected_verbs is not None
            and verb not in settings.security.protected_verbs
        ):
            return True

        # If the route is not protected, return True
        if settings.security.protected_routes is not None:
            # if any of the protected routes partially match the route, return True
            if not all(
                protected_route in route
                for protected_route in settings.security.protected_routes
            ):
                return True

        # Now we need the token to check for access
        # If the token is None, return False
        if token is None:
            return False

        # If the token is a single token, check if it is valid
        if settings.security.is_single_access_token():
            return token == settings.security.single_token

        # If the token is a list of tokens, check if it is valid
        if settings.security.is_multi_access_token():
            return CacheManager().check_if_token_is_valid(token)

        # Nothing matches, return False
        return False

    async def dispatch(self, request: Request, call_next):
        if not settings.security.is_enabled():
            return await call_next(request)

        token = request.headers.get("Authorization")
        # Clean the token
        if token is not None:
            token = token.replace("Bearer ", "").strip()

        # Get HTTP verb and the route
        verb = request.method
        route = request.url.path

        if not self._is_valid_token(token, verb, route):
            return PlainTextResponse(
                "Unauthorized access to the API! Please provide a valid token.",
                status_code=status.HTTP_401_UNAUTHORIZED,
            )
        else:
            return await call_next(request)
