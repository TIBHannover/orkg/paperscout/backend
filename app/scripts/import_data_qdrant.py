import json
import os

from tqdm import tqdm

from app.modules.indexing.indexer import Indexer

indexer = Indexer()
indexer.store().turn_off_indexing()


def fix_journal_object(data: dict) -> dict:
    new_data = data.copy()
    if "journals" in data and data["journals"]:
        if isinstance(data["journals"], list) and not all(
            isinstance(journal, str) for journal in data["journals"]
        ):
            new_data["journals"] = []
            if all(journal is None for journal in data["journals"]):
                return new_data
            for journal in data["journals"]:
                if "title" in journal and journal["title"]:
                    new_data["journals"].append(journal["title"])
                else:
                    new_data["journals"].append(str(journal["identifiers"][0]))
        if isinstance(data["journals"], dict):
            if "title" in data["journals"] and data["journals"]["title"]:
                new_data["journals"] = data["journals"]["title"]
            else:
                new_data["journals"] = str(data["journals"]["identifiers"][0])
    return new_data


def index_jsonl_files(input_directory: str, move_directory: str):
    for file in os.listdir(input_directory):
        if file.endswith(".jsonl"):
            print(f"Indexing {file}")
            with open(os.path.join(input_directory, file), "r") as f:
                documents = []
                count = 0
                for line in tqdm(f.readlines(), unit="lines", position=0, leave=True):
                    count += 1
                    data = json.loads(line)
                    # Add title checks
                    if data.get("title", None) is None:
                        continue
                    # Add abstract checks
                    if data.get("abstract", None) is None:
                        continue
                    abstract = data["abstract"]
                    if len(abstract) < 80:
                        continue
                    # indexer.index_document(fix_journal_object(data), ignore_existing=True)
                    documents = bulk_index_documents(data, documents, 1000)
                if len(documents) > 0:
                    indexer.index_documents(documents)
            # Move file to another directory
            os.rename(
                os.path.join(input_directory, file), os.path.join(move_directory, file)
            )


def bulk_index_documents(data, documents, batch_size=5000):
    documents.append(fix_journal_object(data))
    if len(documents) >= batch_size:
        indexer.index_documents(documents)
        documents = []
    return documents


if __name__ == "__main__":
    index_jsonl_files("/media/jaradeh/HDD2/jsonl", "/media/jaradeh/HDD2/done")
    indexer.store().turn_on_indexing()
