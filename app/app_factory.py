import http
import json
import os

import toml
from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.core import settings
from app.core.decorators import log
from app.models.backend import BackendVersionResponse
from app.routers import backend, internal, llm, search
from app.routers.common import ResponseWrapper
from app.security import AskAuthMiddleware


def get_application(testing: bool = False) -> FastAPI:
    docs_url = (
        settings.general.interactive_docs
        if settings.general.is_prod_environment()
        else "/docs"
    )
    redoc_url = "/docs" if settings.general.is_prod_environment() else "/redoc"
    _app = FastAPI(
        title=f"🥥 {settings.general.app_name}",
        docs_url=docs_url,
        redoc_url=redoc_url,
        version=_extract_app_version_from_toml(),
    )

    __configure_routers(_app, testing=testing)
    _configure_cors_policy(_app)
    __configure_access_security(_app)
    _save_openapi_specification(_app)
    return _app


def _configure_cors_policy(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.general.cors_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def _add_version_endpoint(app: FastAPI, backend_router: APIRouter):
    @backend_router.get(
        "/version",
        status_code=http.HTTPStatus.OK,
        response_model=BackendVersionResponse,
    )
    @log(__name__)
    def get_backend_version():
        """
        Get the backend version
        """
        return ResponseWrapper.wrap_response(
            payload=BackendVersionResponse.Payload(version=app.version),
            type_=BackendVersionResponse,
        )


def __configure_routers(app: FastAPI, testing: bool = False):
    app.include_router(llm.router)
    app.include_router(search.router)

    _add_version_endpoint(app, backend.router)
    app.include_router(backend.router)

    if testing:
        app.include_router(internal.router)


def __configure_access_security(app: FastAPI):
    # Add our custom middleware to check for the authentication header
    app.add_middleware(AskAuthMiddleware)


def _extract_app_version_from_toml() -> str:
    app_dir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(app_dir, "..", "pyproject.toml")) as f:
        pyproject = toml.load(f)
    return pyproject["tool"]["poetry"]["version"]


def _save_openapi_specification(app: FastAPI):
    app_dir = os.path.dirname(os.path.realpath(__file__))
    _write_json(app.openapi(), os.path.join(app_dir, "..", "openapi.json"))


def _write_json(data, input_path):
    with open(input_path, "w") as f:
        json.dump(data, f, indent=4)
