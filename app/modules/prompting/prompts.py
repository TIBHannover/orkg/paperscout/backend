from enum import Enum
from typing import Optional

from app.errors import PromptIdNotRecognizedError
from app.modules.llms.base import BaseOutputParser, PromptTemplate
from app.modules.llms.parsers import KeyValueListXMLParser, SynthesisStringParser

SYS_ROW_EXTRACTION = """You are an analysis-support bot that operates on scholarly documents and follows instructions.
You will get as an input: the research paper content and a set of properties/criteria to look for.
You will extract the values corresponding to the list of provided predicates.
Limit the values to only the content without prefixing it with the paper contains or the text says.
Your output should ALWAYS be like this:
<extraction><key>PROPERTY</key><value>VALUE</value></extraction>
Avoid lengthy values (one sentence MAX), rather split them into multiple values.
If the property contain multiple values, then your output MUST be like this:
<extraction><key>PROPERTY</key><values><value>VALUE1</value><value>VALUE2</value></values></extraction>
If you DO NOT find the value in the content then your output MUST BE THIS:
<extraction><key>PROPERTY</key><value>UNKNOWN</value></extraction>
If you have multiple properties to extract, then you should have multiple <extraction> tags.
Also, if the property is a question, write it down AS IS, WITHOUT any changes to the text.
AVOID extracting any extra properties or values in the output, focus only on the given properties.
"""

USR_ROW_EXTRACTION = """## properties\n{properties}.\n ## paper content\n{content}"""


SYS_ABSTRACT_SYNTHESIS = """You are an analysis-support bot of scholarly documents that follows instructions.
You will generate a comprehensive answer to the given research question (but no more than three/four sentences)
solely based on the content provided.
Cite the number of the content referenced for each claim like this:
[1] for a single reference or [2][3] for multiple references.
Emphasize brevity, focusing on essential details and omitting unnecessary information.
Avoid adding the question in the answer and Do not include any notes or comments.
"""

USR_ABSTRACT_SYNTHESIS = """# Research Question: {question}
# Abstracts:
{abstracts}

# Answer with inline-citations as [#]:
"""

SYS_TRANSLATE_KVP_RESPONSE = """You are a translation bot that follows instructions to the letter.
You have four very important tasks:
1. Do NOT translate the XML tags. LEAVE them as they are without changing them!!.
2. You only translate the content inside the XML <value> tags into the "{language}" language.
3. If the tag contains the word "UNKNOWN", you should leave it as is.
4. Your response is the translated text with the original XML tags.
"""

USR_TRANSLATE_KVP_RESPONSE = """# Text
{response}

# Translated text with original XML tags and the translated <value> tags\n
"""

SYS_TRANSLATE_SYN_RESPONSE = """You are a translation AI assistance that follows instructions to the letter.
You have three very important tasks:
1. Translate the current synthesis into "{language}".
2. Only reply with the translated synthesis, without any additional information.
3. Do not break character or add any extra information like comments or notes.
"""

USR_TRANSLATE_SYN_RESPONSE = """# Synthesis
{synthesis}

# Translated synthesis in "{language}"\n"""


class PromptIds(Enum):
    ROW_EXTRACTION = 1
    ABSTRACT_SYNTHESIS = 2
    TRANSLATE_KVP_RESPONSE = 3
    TRANSLATE_SYN_RESPONSE = 4


class Prompts:
    ROW_EXTRACTION = PromptTemplate(
        id=PromptIds.ROW_EXTRACTION.value,
        system_prompt=SYS_ROW_EXTRACTION,
        user_prompt=USR_ROW_EXTRACTION,
    )
    ABSTRACT_SYNTHESIS = PromptTemplate(
        id=PromptIds.ABSTRACT_SYNTHESIS.value,
        system_prompt=SYS_ABSTRACT_SYNTHESIS,
        user_prompt=USR_ABSTRACT_SYNTHESIS,
    )
    TRANSLATE_KVP_RESPONSE = PromptTemplate(
        id=PromptIds.TRANSLATE_KVP_RESPONSE.value,
        system_prompt=SYS_TRANSLATE_KVP_RESPONSE,
        user_prompt=USR_TRANSLATE_KVP_RESPONSE,
    )
    TRANSLATE_SYN_RESPONSE = PromptTemplate(
        id=PromptIds.TRANSLATE_SYN_RESPONSE.value,
        system_prompt=SYS_TRANSLATE_SYN_RESPONSE,
        user_prompt=USR_TRANSLATE_SYN_RESPONSE,
    )

    PROMPT_MAP = {
        PromptIds.ROW_EXTRACTION: {
            "template": ROW_EXTRACTION,
            "parser": KeyValueListXMLParser,
            "param_field": "properties",
        },
        PromptIds.ABSTRACT_SYNTHESIS: {
            "template": ABSTRACT_SYNTHESIS,
            "parser": SynthesisStringParser,
            "param_field": "abstracts",
        },
        PromptIds.TRANSLATE_KVP_RESPONSE: {
            "template": TRANSLATE_KVP_RESPONSE,
            "parser": KeyValueListXMLParser,
            "param_field": "response",  # Not used! The state takes care of injecting the parameter
        },
        PromptIds.TRANSLATE_SYN_RESPONSE: {
            "template": TRANSLATE_SYN_RESPONSE,
            "parser": SynthesisStringParser,
            "param_field": "synthesis",  # Not used! The state takes care of injecting the parameter
        },
    }

    @staticmethod
    def map(prompt_id: PromptIds) -> PromptTemplate:
        """
        Maps a prompt ID to a prompt template.
        :param prompt_id: the prompt ID
        :return: the prompt template corresponding to the ID
        """
        try:
            return Prompts.PROMPT_MAP[prompt_id]["template"]
        except KeyError:
            raise PromptIdNotRecognizedError(f"Prompt with id({prompt_id}) not found")

    @staticmethod
    def get_parser(
        prompt_template: Optional[PromptTemplate] = None,
        prompt_id: Optional[PromptIds] = None,
    ) -> BaseOutputParser:
        """
        Get the parser for the given prompt template or prompt ID.
        :param prompt_template: the prompt template
        :param prompt_id: the prompt ID
        :return: the parser for the given prompt template or prompt ID
        """
        if prompt_template is None and prompt_id is None:
            raise ValueError("Either prompt_template or prompt_id must be provided")
        if prompt_id is not None:
            prompt_template = Prompts.map(prompt_id)

        try:
            return Prompts.PROMPT_MAP[PromptIds(prompt_template.id_)]["parser"]()
        except KeyError:
            raise PromptIdNotRecognizedError(f"Prompt with id({prompt_id}) not found")

    @staticmethod
    def get_parameter_field(
        prompt_template: Optional[PromptTemplate] = None,
        prompt_id: Optional[PromptIds] = None,
    ) -> str:
        """
        Get the parameter field for the given prompt template or prompt ID.
        :param prompt_template: the prompt template
        :param prompt_id: the prompt ID
        :return: the parameter field for the given prompt template or prompt ID
        """
        if prompt_template is None and prompt_id is None:
            raise ValueError("Either prompt_template or prompt_id must be provided")
        if prompt_id is not None:
            prompt_template = Prompts.map(prompt_id)
        try:
            return Prompts.PROMPT_MAP[PromptIds(prompt_template.id_)]["param_field"]
        except KeyError:
            raise PromptIdNotRecognizedError(f"Prompt with id({prompt_id}) not found")
