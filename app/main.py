# -*- coding: utf-8 -*-
from fastapi.responses import HTMLResponse

from app.app_factory import get_application

app = get_application()


@app.get("/", response_class=HTMLResponse)
def root():
    return """
    <html>
        <head>
            <title>🥥 ORKG-Ask 🥥 </title>
        </head>
        <body style="position: fixed;top: 50%;left: 50%;margin-top: -100px;margin-left: -200px;">
            <h3><strong>ORKG Ask</strong>: Find research you are actually looking for!</h3>
            <span>Check the <a href="/docs">/docs</a> page for more information</span>
            <pre>
            ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⠀⠀⠀⠀⣠⡶⠟⠉⠉⠉⠙⠛⠶⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⠀⠀⢰⣾⠏⠀⠀⠀⣿⣦⠀⠀⠀⠈⢷⣦⠄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⠀⡆⣿⠃⠀⢰⣿⠀⠈⠉⠀⠀⠀⠀⠀⠻⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⢀⣿⠇⠀⠀⠘⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⢉⣀⣀⣀⡀⠀⠀⠀⠀⠀⠀⠀
            ⠀⠀⢸⡟⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⠴⠚⣛⣉⣉⣉⣉⣙⣿⡆⠀⠀⠀⠀⠀
            ⠀⠀⣾⠃⠀⠀⠀⠀⠀⠀⠀⢀⣴⢟⣥⣶⣿⣿⣿⣿⣿⣿⣿⠟⢁⣼⣇⠀⠀⠀
            ⠀⢰⡟⠀⠀⠀⠀⠀⠀⠀⠀⠘⠿⠿⣿⣿⣿⠿⠿⠟⠛⢉⣠⣴⡟⠁⢻⡄⠀⠀
            ⠀⣸⡇⠀⠀⠀⠀⠀⠀⠀⠈⣿⠶⢦⣤⣤⣤⣤⣶⣶⣿⠿⠟⠛⠀⠀⠸⣧⠀⠀
            ⠀⢹⡇⠀⠀⠀⠀⠀⠀⠀⠀⢿⡀⠀⠀⠀⠈⠉⠉⣿⡇⠀⠀⠀⠀⠀⠀⣿⡀⠀
            ⠀⢸⣇⠀⠀⠀⠀⠀⠀⠀⠀⢸⣧⠀⠀⠀⠀⠀⠀⣿⣇⠀⠀⠀⠀⠀⠀⣿⠇⠀
            ⠀⢦⣿⡄⠀⠀⠀⠀⠀⠀⠀⠈⣿⣆⠀⠀⠀⠀⠀⢿⣿⠇⠀⠀⠀⠀⢠⣿⠀⠀
            ⠀⠈⣿⣷⡄⠀⠀⠀⠀⠀⠀⠀⠈⠿⣧⡀⠀⠀⠀⠀⠁⠀⠀⠀⠀⢀⣾⠻⠀⠀
            ⠀⠀⠈⠙⠛⢷⣤⣄⣀⣀⣀⣀⣀⣤⣴⠿⠶⣤⣄⣀⣀⣀⣤⣤⡶⠟⠁⠀⠀⠀
            ⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠉⠉⠉⠁⠀⠀⠀⠀⠉⠉⠉⠉⠁⠀⠀⠀⠀
          </pre>⠀⠀⠀
        </body>
    </html>
    """


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
