from typing import Any, Dict, List
from uuid import UUID

from app import FilterString
from app.modules.indexing.base import IDocument, IManager
from app.modules.indexing.vectordb.models import (
    QdrantDocument,
    QdrantFilter,
    QdrantPageResponse,
)
from app.modules.indexing.vectordb.store import QdrantStore


class Indexer(IManager):
    """
    A conductor class that handles all the indexing/searching operations.
    """

    _store: QdrantStore | None = None
    instance: "Indexer" = None

    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self):
        self._store = QdrantStore()

    def store(self) -> QdrantStore:
        return self._store

    def index_document(self, document: Dict[str, Any], ignore_existing: bool = False):
        """
        Index a document in all the available indices.
        :param document: The document to index.
        :param ignore_existing: Whether to ignore existing documents with the same ID. (default: False)
        """
        self._store.index(QdrantDocument(**document), ignore_existing=ignore_existing)

    def index_documents(self, documents: List[Dict[str, Any]]):
        """
        Index a list of documents in all the available indices.
        :param documents: The document to index.
        """
        self._store.bulk_index([QdrantDocument(**d) for d in documents])

    def vector_search(
        self,
        text: str,
        limit: int = 10,
        offset: int | None = None,
        criteria: FilterString | None = None,
        focus: List[str] | None = None,
    ) -> QdrantPageResponse:
        """
        Perform an ANN search on the vector database.
        :param text: the text to search for
        :param limit: the maximum number of results to return
        :param offset: The offset to start fetching documents from. (Optional)
        :param criteria: The criteria to filter the search results. (Optional)
        :param focus: The sources to search from. (Optional)
        :return: A list of PaperDocuments.
        """
        return self._store.search(
            query=text, limit=limit, offset=offset, criteria=criteria, focus=focus
        )

    def retrieve_documents(
        self,
        document_id: str | None = None,
        criteria: FilterString | None = None,
        offset: UUID | None = None,
        limit: int = 1,
        focus: List[str] | None = None,
    ) -> QdrantPageResponse:
        """
        Retrieve documents from the vector database.
        :param document_id: The ID of the document to retrieve. (Optional)
        :param criteria: The criteria to retrieve the document. (Optional)
        :param limit: The maximum number of documents to return. (default: 1)
        :param offset: The offset UUID to start fetching documents from. (Optional)
        :param focus: The sources to search from. (Optional)
        :return: A page of documents.
        """
        return self._store.get(
            doc_id=document_id,
            criteria=criteria,
            limit=limit,
            offset=offset,
            focus=focus,
        )

    def count_documents(self, criteria: FilterString | None = None) -> int:
        """
        Count documents from the vector database.
        :param criteria: The criteria to filter the search results. (Optional)
        :return: The number of documents that match the search.
        """
        return self._store.count(criteria=criteria)

    def recommend_documents(
        self,
        document_ids: List[str],
        avoid_ids: List[str] | None = None,
        limit: int = 10,
        offset: int | None = None,
        criteria: FilterString | None = None,
    ) -> List[IDocument]:
        """
        Perform an ANN search on the vector database.
        :param document_ids: the IDs of the documents to recommend based of
        :param avoid_ids: the IDs of the documents to avoid recommending (Optional)
        :param limit: the maximum number of results to return (default: 10)
        :param offset: The offset to start fetching documents from. (Optional)
        :param criteria: The criteria to filter the search results. (Optional)
        :return: A list of PaperDocuments.
        """
        return [
            document
            for document in self._store.recommend(
                ids=document_ids,
                avoid=avoid_ids,
                limit=limit,
                offset=offset,
                criteria=criteria,
            )
        ]

    def get_info(self) -> Dict[str, Any]:
        """
        Get information about the available indices.
        :return: A dictionary containing information about the available indices.
        """
        return self._store.info()

    def delete_all_documents(self) -> int:
        """
        Delete all the documents in the store.
        :return: The number of documents deleted.
        """
        return self._store.delete(criteria=QdrantFilter.universal_filter())
