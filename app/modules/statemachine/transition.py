from typing import Callable, Optional

from app.modules.statemachine.state import State


class Transition:
    """
    Represents a transition between two states in a state machine.
    """

    from_state: State
    to_state: State
    condition: Optional[Callable[[], bool]]

    def __init__(
        self,
        from_state: State,
        to_state: State,
        condition: Optional[Callable[[], bool]] = None,
    ):
        self.from_state = from_state
        self.to_state = to_state
        self.condition = condition

    @property
    def can_transition(self) -> bool:
        if self.condition is None:
            return True
        return self.condition()
