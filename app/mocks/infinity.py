from typing import List

from app.core import settings
from app.modules.embeddings.base import EmbeddingsEngine, Text

if settings.general.is_test_environment():
    from fastembed import TextEmbedding


class DummyInfinityEngine(EmbeddingsEngine):
    embedding_model: "TextEmbedding"

    def __init__(self, *args, **kwargs):
        self.embedding_model = TextEmbedding()

    def _dummy_embeddings(self, text: str | list) -> List:
        return list(self.embedding_model.embed(text))

    def compute_query_embeddings(self, query: str, *args, **kwargs) -> List[List]:
        return self._dummy_embeddings(query)

    def compute_source_embeddings(self, texts: Text, *args, **kwargs) -> List[List]:
        return self._dummy_embeddings(texts)

    def ndims(self) -> int:
        return len(self._dummy_embeddings("dummy")[0])
