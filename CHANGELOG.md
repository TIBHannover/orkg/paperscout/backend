# CHANGELOG

Here is a list of all releases and changes for the ORKG-Ask project.


## v1.14.2 (2025-02-17)




### Documentation


* Update OpenAPI specs to 1.14.1 ([`9577d54`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/9577d54c41afe57ea4559fd697bf455bf2c2ee27))






### Fixes


* Deduplicating points removed the UUID offset ([`3004d8f`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/3004d8fb9435bf53b8b2a7c829d78a6edd2de88c))


* Explore endpoint didn&#39;t pass the focus variable correctly ([`900223d`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/900223db2baa374c8bda85b44050fc35692b6c54))





## v1.14.1 (2025-02-14)




### Fixes


* Encoding issues with the ids in the vector store endpoints ([`ab8483a`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/ab8483a4df9a152d48bd4aa8a3d5b6fee21aa7c0))





## v1.14.0 (2025-02-13)




### Feature


* Update pre-commit hooks ([`affb53c`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/affb53ce411dbd8855cc4318b6dfcd01c46915c9))






### Fixes


* Add poetry export plugin to build image ([`b73695b`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/b73695b593c27ed7cdae5b4603a206ac8ba1a2e5))


* Item_ids couldn&#39;t be url encoded ([`a4231fe`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/a4231fe91c32d82b636718bc135d8750525ce492))





## v1.13.4 (2024-12-17)




### Fixes


* Only check for prompts when reproducibility is requested ([`df6c34a`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/df6c34adca94aca881843118961c82a5de06ef26))





## v1.13.3 (2024-12-13)




### Documentation


* Update openAPI to 1.13.2 ([`0df0f29`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/0df0f2987ceeabd187c3d7212dae14f7a8b16f77))






### Fixes


* Synthesis cache key was unquoted ([`060c817`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/060c817e480509001c55d5a81fd46ee136d13b79))





## v1.13.2 (2024-12-13)




### Documentation


* openapi is now at version 1.13.1 ([`204d368`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/204d368fa6d33ffa4c1b724d78e6f8091b01735c))






### Fixes


* increase the output buffer to 1k token ([`ae17d92`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/ae17d92928b90873175f8d7becb7371b89e52705))


* Bug with unquoted string for the questions collection ([`dc98a12`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/dc98a12b33cf07117e22bae01f4abf1d1042668b))





## v1.13.1 (2024-12-09)




### Fixes


* Broken numbers of cache hits ([`d625228`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/d6252286033364230b2a996607d8b93aa37c5aa4))





## v1.13.0 (2024-12-09)




### Documentation


* Fix typo in finetuning instructions ([`2f387b3`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/2f387b313807e80d1a9c4dbe1d9dfb1a4844d598))






### Feature


* Add prompts to reproducibility endpoints ([`73ab39b`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/73ab39b41e60c4cde14430294f897055cb674af4))






### Fixes


* Bug when reconstructing XML-format of single value items ([`8d4af99`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/8d4af998d976ee2d0b0add943c060107c2af5b0e))


* Backend stats for questions are now more accurate ([`941e213`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/941e213e8814d49b28c6755661f0fa676d59ce63))






### Tests


* Fix broken mock/default parsers ([`84daa13`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/84daa13300d2c793f933eb8af37763dd6bb7994f))





## v1.12.0 (2024-10-30)




### Feature


* Add basic CLI scripts for finetuning LLMs for Ask ([`6b7b0f2`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/6b7b0f26df19748f6bf0ebd7a6766dc02034fcd7))





## v1.11.4 (2024-10-21)




### Documentation


* Fix limit on commit history for semantic-release ([`208ccc5`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/208ccc5bab9e55de1686b411fb4d58c7b80f5442))






### Fixes


* rollback to ignore token ([`6dfb231`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/6dfb231f8c15eda983a2bc4e76bf277a3dd04825))


* Filter string EQ operator supports DOIs ([`7b651f0`](https://gitlab.com/TIBHannover/orkg/orkg-ask/backend/-/commit/7b651f0c0d867010ee467d4ed9ddd231dfbf0e88))




