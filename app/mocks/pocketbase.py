import uuid
from dataclasses import make_dataclass
from typing import Dict

from app.modules.caching.base import ICache


def filter_data(filter_str: str, data: list):
    """
    Filters a list of dictionaries based on a filter string.
    :param filter_str: The filter string, in the format 'key1=value1 && key2=value2 && ...'
    :param data: A list of dictionaries, where each dictionary represents a data point.
    :return A list of dictionaries that match the filter.
    """
    # Remove the ( and ) characters from the filter string
    filter_str = filter_str.replace("(", "").replace(")", "")

    # Split the filter string into individual conditions
    conditions = filter_str.split(" && ")

    # Create a dictionary to store the filter values
    filter_values = {}
    for condition in conditions:
        key, value = condition.split("=")
        filter_values[key] = value

    # Filter the data based on the filter values
    selected_data = []
    for item in data:
        match = True
        for key, value in filter_values.items():
            if item[key] != value:
                match = False
                break
        if match:
            selected_data.append(item)

    return selected_data


class InMemoryPocketBase(ICache):
    store: dict

    def __init__(self):
        self.store = {"itemsCache": [], "collectionItems": []}

    @staticmethod
    def to_snake_case(string: str) -> str:
        return "".join(["_" + i.lower() if i.isupper() else i for i in string]).lstrip(
            "_"
        )

    @staticmethod
    def make_record(item: Dict):
        record_cls = make_dataclass("Record", item.keys())
        instance = record_cls(**item)
        # Iterate over the fields of the record (dataclass) and convert them to snake case
        for field in record_cls.__dict__["__annotations__"].keys():
            setattr(
                instance,
                InMemoryPocketBase.to_snake_case(field),
                getattr(instance, field),
            )
        return instance

    def search(
        self,
        collection: str,
        page: int | None = None,
        per_page: int = 30,
        sort: str | None = None,
        filters: str | None = None,
        expand: str | None = None,
        fields: str | None = None,
    ):
        if filters:
            return filter_data(filters, self.store[collection])[:30]
        return self.store[collection][:30]

    def get_all(
        self,
        collection: str,
        sort: str | None = None,
        filters: str | None = None,
        expand: str | None = None,
        fields: str | None = None,
    ):
        return self.store[collection]

    def get(
        self,
        collection: str,
        item_id: str,
        expand: str | None = None,
        fields: str | None = None,
    ):
        results = filter(lambda x: x.get("id", "") == item_id, self.store[collection])
        if results:
            return self.make_record(list(results)[0])
        return None

    def insert(
        self,
        collection: str,
        item: Dict,
        expand: str | None = None,
        fields: str | None = None,
    ):
        if "id" not in item:
            item["id"] = uuid.uuid4().hex
        self.store[collection].append(item)
        return self.make_record(item)

    def upsert(
        self,
        collection: str,
        item: Dict,
        expand: str | None = None,
        fields: str | None = None,
        overwrite_on: str | None = None,
    ):
        if overwrite_on:
            results = self.search(collection, filters=overwrite_on)
            if results:
                item_id = results[0]["id"]
                return self.update(collection, item_id, item, expand, fields)
        return self.insert(collection, item, expand, fields)

    def update(
        self,
        collection: str,
        item_id: str,
        item: Dict,
        expand: str | None = None,
        fields: str | None = None,
    ):
        for i, x in enumerate(self.store[collection]):
            if x.get("id", "") == item_id:
                self.store[collection][i] = item
                break
        return self.make_record(item)

    def delete(self, collection: str, item_id: str) -> bool:
        for i, x in enumerate(self.store[collection]):
            if x.get("id", "") == item_id:
                del self.store[collection][i]
                return True
        return False
