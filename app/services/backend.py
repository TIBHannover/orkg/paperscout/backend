from app.models.backend import (
    BackendHealthResponse,
    DatasetStats,
    IndexingStats,
    QuestionsResponse,
    StatisticsResponse,
    UsageStats,
)
from app.modules.caching.manager import CacheManager
from app.modules.indexing.indexer import Indexer

builder_method_field_mapper = {
    "id": DatasetStats.Builder.with_num_items_with_abstracts,
    "authors": DatasetStats.Builder.with_num_items_with_authors,
    "language": DatasetStats.Builder.with_num_items_with_languages,
    "topics": DatasetStats.Builder.with_num_items_with_topics,
    "subjects": DatasetStats.Builder.with_num_items_with_subjects,
    "year": DatasetStats.Builder.with_num_items_with_year,
    "identifiers": DatasetStats.Builder.with_num_items_with_identifiers,
    "document_type": DatasetStats.Builder.with_num_items_with_type,
    "issn": DatasetStats.Builder.with_num_items_with_issn,
    "doi": DatasetStats.Builder.with_num_items_with_doi,
    "citation_count": DatasetStats.Builder.with_num_items_with_citations,
    "date_published": DatasetStats.Builder.with_num_items_with_publication_date,
    "publisher": DatasetStats.Builder.with_num_items_with_publisher,
    "journals": DatasetStats.Builder.with_num_items_with_journal,
    "extractions.entities.dbpedia[].uri": DatasetStats.Builder.with_num_items_with_dbpedia_entities,
}


def get_stats() -> StatisticsResponse.Payload:
    """
    Get the statistics.
    :return: the StatisticsResponse.Payload object
    """
    return StatisticsResponse.Payload(
        dataset=get_data_stats(),
        indexing=get_indexing_stats(),
        usage=get_usage_stats(),
    )


def get_data_stats() -> DatasetStats:
    """
    Get the dataset stats.
    :return: the DatasetStats object
    """
    store_info = Indexer().get_info()
    builder = DatasetStats.Builder()
    for field, method in builder_method_field_mapper.items():
        if field in store_info["payload_schema"]:
            bound_method = method.__get__(builder, DatasetStats.Builder)
            builder = bound_method(store_info["payload_schema"][field].points)
    return builder.build()


def get_indexing_stats() -> IndexingStats:
    """
    Get the indexing stats.
    :return: the IndexingStats object
    """
    store_info = Indexer().get_info()
    return IndexingStats.from_values(
        num_indexed_vectors=store_info["indexed_vectors_count"],
        vector_size=store_info["config"]["params"]["vectors"]["size"],
        distance_method=store_info["config"]["params"]["vectors"]["distance"],
    )


def get_usage_stats() -> UsageStats:
    """
    Get the usage stats.
    :return: the UsageStats object
    """
    return UsageStats(
        num_registered_users=CacheManager().count_users(),
        num_collections=CacheManager().count_collections(),
        num_collection_items=CacheManager().count_collection_items(),
        num_saved_searches=CacheManager().count_saved_searches(),
        num_shared_links=CacheManager().count_shared_links(),
        num_cache_hits=CacheManager().count_cache_hits(),
        num_questions_asked=CacheManager().count_questions(),
    )


def get_recent_questions(limit: int) -> QuestionsResponse.Payload:
    """
    Get the recent questions.
    :return: the list of QuestionRecord objects
    """
    return QuestionsResponse.Payload(
        questions=CacheManager().get_recent_questions(limit=limit)
    )


def get_popular_questions(limit: int) -> QuestionsResponse.Payload:
    """
    Get the popular questions.
    :return: the list of QuestionRecord objects
    """
    return QuestionsResponse.Payload(
        questions=CacheManager().get_popular_questions(limit=limit)
    )


def get_backend_health() -> BackendHealthResponse.Payload:
    """
    Get the backend health.
    :return: the BackendHealthResponse.Payload object
    """
    return BackendHealthResponse.Payload.a_ok()
