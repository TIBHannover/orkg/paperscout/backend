# This script builds the sitemap of ORKG Ask.
# Note: A sitemap can contain maximum 50,000 urls, for it seems like multiple files have to be created
# https://developers.google.com/search/docs/crawling-indexing/sitemaps/large-sitemaps

# The slugify code is based on https://github.com/simov/slugify/blob/master/slugify.js

import gzip
import os
import re
import unicodedata
import uuid
from typing import Dict, Optional, Pattern, Union

import click
from qdrant_client import QdrantClient


class Slugify:
    def __init__(self):
        # Initialize the character map by parsing the JSON string from JS
        self.char_map: Dict[str, str] = {
            "$": "dollar",
            "%": "percent",
            "&": "and",
            "<": "less",
            ">": "greater",
            "|": "or",
            "¢": "cent",
            "£": "pound",
            "¤": "currency",
            "¥": "yen",
            "©": "(c)",
            "ª": "a",
            "®": "(r)",
            "º": "o",
            "À": "A",
            "Á": "A",
            "Â": "A",
            "Ã": "A",
            "Ä": "A",
            "Å": "A",
            "Æ": "AE",
            "Ç": "C",
            "È": "E",
            "É": "E",
            "Ê": "E",
            "Ë": "E",
            "Ì": "I",
            "Í": "I",
            "Î": "I",
            "Ï": "I",
            "Ð": "D",
            "Ñ": "N",
            "Ò": "O",
            "Ó": "O",
            "Ô": "O",
            "Õ": "O",
            "Ö": "O",
            "Ø": "O",
            "Ù": "U",
            "Ú": "U",
            "Û": "U",
            "Ü": "U",
            "Ý": "Y",
            "Þ": "TH",
            "ß": "ss",
            "à": "a",
            "á": "a",
            "â": "a",
            "ã": "a",
            "ä": "a",
            "å": "a",
            "æ": "ae",
            "ç": "c",
            "è": "e",
            "é": "e",
            "ê": "e",
            "ë": "e",
            "ì": "i",
            "í": "i",
            "î": "i",
            "ï": "i",
            "ð": "d",
            "ñ": "n",
            "ò": "o",
            "ó": "o",
            "ô": "o",
            "õ": "o",
            "ö": "o",
            "ø": "o",
            "ù": "u",
            "ú": "u",
            "û": "u",
            "ü": "u",
            "ý": "y",
            "þ": "th",
            "ÿ": "y",
            "Ā": "A",
            "ā": "a",
            "Ă": "A",
            "ă": "a",
            "Ą": "A",
            "ą": "a",
            "Ć": "C",
            "ć": "c",
            "Č": "C",
            "č": "c",
            "Ď": "D",
            "ď": "d",
            "Đ": "DJ",
            "đ": "dj",
            "Ē": "E",
            "ē": "e",
            "Ė": "E",
            "ė": "e",
            "Ę": "e",
            "ę": "e",
            "Ě": "E",
            "ě": "e",
            "Ğ": "G",
            "ğ": "g",
            "Ģ": "G",
            "ģ": "g",
            "Ĩ": "I",
            "ĩ": "i",
            "Ī": "i",
            "ī": "i",
            "Į": "I",
            "į": "i",
            "İ": "I",
            "ı": "i",
            "Ķ": "k",
            "ķ": "k",
            "Ļ": "L",
            "ļ": "l",
            "Ľ": "L",
            "ľ": "l",
            "Ł": "L",
            "ł": "l",
            "Ń": "N",
            "ń": "n",
            "Ņ": "N",
            "ņ": "n",
            "Ň": "N",
            "ň": "n",
            "Ō": "O",
            "ō": "o",
            "Ő": "O",
            "ő": "o",
            "Œ": "OE",
            "œ": "oe",
            "Ŕ": "R",
            "ŕ": "r",
            "Ř": "R",
            "ř": "r",
            "Ś": "S",
            "ś": "s",
            "Ş": "S",
            "ş": "s",
            "Š": "S",
            "š": "s",
            "Ţ": "T",
            "ţ": "t",
            "Ť": "T",
            "ť": "t",
            "Ũ": "U",
            "ũ": "u",
            "Ū": "u",
            "ū": "u",
            "Ů": "U",
            "ů": "u",
            "Ű": "U",
            "ű": "u",
            "Ų": "U",
            "ų": "u",
            "Ŵ": "W",
            "ŵ": "w",
            "Ŷ": "Y",
            "ŷ": "y",
            "Ÿ": "Y",
            "Ź": "Z",
            "ź": "z",
            "Ż": "Z",
            "ż": "z",
            "Ž": "Z",
            "ž": "z",
            "Ə": "E",
            "ƒ": "f",
            "Ơ": "O",
            "ơ": "o",
            "Ư": "U",
            "ư": "u",
            "ǈ": "LJ",
            "ǉ": "lj",
            "ǋ": "NJ",
            "ǌ": "nj",
            "Ș": "S",
            "ș": "s",
            "Ț": "T",
            "ț": "t",
            "ə": "e",
            "˚": "o",
            "Ά": "A",
            "Έ": "E",
            "Ή": "H",
            "Ί": "I",
            "Ό": "O",
            "Ύ": "Y",
            "Ώ": "W",
            "ΐ": "i",
            "Α": "A",
            "Β": "B",
            "Γ": "G",
            "Δ": "D",
            "Ε": "E",
            "Ζ": "Z",
            "Η": "H",
            "Θ": "8",
            "Ι": "I",
            "Κ": "K",
            "Λ": "L",
            "Μ": "M",
            "Ν": "N",
            "Ξ": "3",
            "Ο": "O",
            "Π": "P",
            "Ρ": "R",
            "Σ": "S",
            "Τ": "T",
            "Υ": "Y",
            "Φ": "F",
            "Χ": "X",
            "Ψ": "PS",
            "Ω": "W",
            "Ϊ": "I",
            "Ϋ": "Y",
            "ά": "a",
            "έ": "e",
            "ή": "h",
            "ί": "i",
            "ΰ": "y",
            "α": "a",
            "β": "b",
            "γ": "g",
            "δ": "d",
            "ε": "e",
            "ζ": "z",
            "η": "h",
            "θ": "8",
            "ι": "i",
            "κ": "k",
            "λ": "l",
            "μ": "m",
            "ν": "n",
            "ξ": "3",
            "ο": "o",
            "π": "p",
            "ρ": "r",
            "ς": "s",
            "σ": "s",
            "τ": "t",
            "υ": "y",
            "φ": "f",
            "χ": "x",
            "ψ": "ps",
            "ω": "w",
            "ϊ": "i",
            "ϋ": "y",
            "ό": "o",
            "ύ": "y",
            "ώ": "w",
            "Ё": "Yo",
            "Ђ": "DJ",
            "Є": "Ye",
            "І": "I",
            "Ї": "Yi",
            "Ј": "J",
            "Љ": "LJ",
            "Њ": "NJ",
            "Ћ": "C",
            "Џ": "DZ",
            "А": "A",
            "Б": "B",
            "В": "V",
            "Г": "G",
            "Д": "D",
            "Е": "E",
            "Ж": "Zh",
            "З": "Z",
            "И": "I",
            "Й": "J",
            "К": "K",
            "Л": "L",
            "М": "M",
            "Н": "N",
            "О": "O",
            "П": "P",
            "Р": "R",
            "С": "S",
            "Т": "T",
            "У": "U",
            "Ф": "F",
            "Х": "H",
            "Ц": "C",
            "Ч": "Ch",
            "Ш": "Sh",
            "Щ": "Sh",
            "Ъ": "U",
            "Ы": "Y",
            "Ь": "",
            "Э": "E",
            "Ю": "Yu",
            "Я": "Ya",
            "а": "a",
            "б": "b",
            "в": "v",
            "г": "g",
            "д": "d",
            "е": "e",
            "ж": "zh",
            "з": "z",
            "и": "i",
            "й": "j",
            "к": "k",
            "л": "l",
            "м": "m",
            "н": "n",
            "о": "o",
            "п": "p",
            "р": "r",
            "с": "s",
            "т": "t",
            "у": "u",
            "ф": "f",
            "х": "h",
            "ц": "c",
            "ч": "ch",
            "ш": "sh",
            "щ": "sh",
            "ъ": "u",
            "ы": "y",
            "ь": "",
            "э": "e",
            "ю": "yu",
            "я": "ya",
            "ё": "yo",
            "ђ": "dj",
            "є": "ye",
            "і": "i",
            "ї": "yi",
            "ј": "j",
            "љ": "lj",
            "њ": "nj",
            "ћ": "c",
            "ѝ": "u",
            "џ": "dz",
            "Ґ": "G",
            "ґ": "g",
            "Ғ": "GH",
            "ғ": "gh",
            "Қ": "KH",
            "қ": "kh",
            "Ң": "NG",
            "ң": "ng",
            "Ү": "UE",
            "ү": "ue",
            "Ұ": "U",
            "ұ": "u",
            "Һ": "H",
            "һ": "h",
            "Ә": "AE",
            "ә": "ae",
            "Ө": "OE",
            "ө": "oe",
            "Ա": "A",
            "Բ": "B",
            "Գ": "G",
            "Դ": "D",
            "Ե": "E",
            "Զ": "Z",
            "Է": "E'",
            "Ը": "Y'",
            "Թ": "T'",
            "Ժ": "JH",
            "Ի": "I",
            "Լ": "L",
            "Խ": "X",
            "Ծ": "C'",
            "Կ": "K",
            "Հ": "H",
            "Ձ": "D'",
            "Ղ": "GH",
            "Ճ": "TW",
            "Մ": "M",
            "Յ": "Y",
            "Ն": "N",
            "Շ": "SH",
            "Չ": "CH",
            "Պ": "P",
            "Ջ": "J",
            "Ռ": "R'",
            "Ս": "S",
            "Վ": "V",
            "Տ": "T",
            "Ր": "R",
            "Ց": "C",
            "Փ": "P'",
            "Ք": "Q'",
            "Օ": "O''",
            "Ֆ": "F",
            "և": "EV",
            "ء": "a",
            "آ": "aa",
            "أ": "a",
            "ؤ": "u",
            "إ": "i",
            "ئ": "e",
            "ا": "a",
            "ب": "b",
            "ة": "h",
            "ت": "t",
            "ث": "th",
            "ج": "j",
            "ح": "h",
            "خ": "kh",
            "د": "d",
            "ذ": "th",
            "ر": "r",
            "ز": "z",
            "س": "s",
            "ش": "sh",
            "ص": "s",
            "ض": "dh",
            "ط": "t",
            "ظ": "z",
            "ع": "a",
            "غ": "gh",
            "ف": "f",
            "ق": "q",
            "ك": "k",
            "ل": "l",
            "م": "m",
            "ن": "n",
            "ه": "h",
            "و": "w",
            "ى": "a",
            "ي": "y",
            "ً": "an",
            "ٌ": "on",
            "ٍ": "en",
            "َ": "a",
            "ُ": "u",
            "ِ": "e",
            "ْ": "",
            "٠": "0",
            "١": "1",
            "٢": "2",
            "٣": "3",
            "٤": "4",
            "٥": "5",
            "٦": "6",
            "٧": "7",
            "٨": "8",
            "٩": "9",
            "پ": "p",
            "چ": "ch",
            "ژ": "zh",
            "ک": "k",
            "گ": "g",
            "ی": "y",
            "۰": "0",
            "۱": "1",
            "۲": "2",
            "۳": "3",
            "۴": "4",
            "۵": "5",
            "۶": "6",
            "۷": "7",
            "۸": "8",
            "۹": "9",
            "฿": "baht",
            "ა": "a",
            "ბ": "b",
            "გ": "g",
            "დ": "d",
            "ე": "e",
            "ვ": "v",
            "ზ": "z",
            "თ": "t",
            "ი": "i",
            "კ": "k",
            "ლ": "l",
            "მ": "m",
            "ნ": "n",
            "ო": "o",
            "პ": "p",
            "ჟ": "zh",
            "რ": "r",
            "ს": "s",
            "ტ": "t",
            "უ": "u",
            "ფ": "f",
            "ქ": "k",
            "ღ": "gh",
            "ყ": "q",
            "შ": "sh",
            "ჩ": "ch",
            "ც": "ts",
            "ძ": "dz",
            "წ": "ts",
            "ჭ": "ch",
            "ხ": "kh",
            "ჯ": "j",
            "ჰ": "h",
            "Ṣ": "S",
            "ṣ": "s",
            "Ẁ": "W",
            "ẁ": "w",
            "Ẃ": "W",
            "ẃ": "w",
            "Ẅ": "W",
            "ẅ": "w",
            "ẞ": "SS",
            "Ạ": "A",
            "ạ": "a",
            "Ả": "A",
            "ả": "a",
            "Ấ": "A",
            "ấ": "a",
            "Ầ": "A",
            "ầ": "a",
            "Ẩ": "A",
            "ẩ": "a",
            "Ẫ": "A",
            "ẫ": "a",
            "Ậ": "A",
            "ậ": "a",
            "Ắ": "A",
            "ắ": "a",
            "Ằ": "A",
            "ằ": "a",
            "Ẳ": "A",
            "ẳ": "a",
            "Ẵ": "A",
            "ẵ": "a",
            "Ặ": "A",
            "ặ": "a",
            "Ẹ": "E",
            "ẹ": "e",
            "Ẻ": "E",
            "ẻ": "e",
            "Ẽ": "E",
            "ẽ": "e",
            "Ế": "E",
            "ế": "e",
            "Ề": "E",
            "ề": "e",
            "Ể": "E",
            "ể": "e",
            "Ễ": "E",
            "ễ": "e",
            "Ệ": "E",
            "ệ": "e",
            "Ỉ": "I",
            "ỉ": "i",
            "Ị": "I",
            "ị": "i",
            "Ọ": "O",
            "ọ": "o",
            "Ỏ": "O",
            "ỏ": "o",
            "Ố": "O",
            "ố": "o",
            "Ồ": "O",
            "ồ": "o",
            "Ổ": "O",
            "ổ": "o",
            "Ỗ": "O",
            "ỗ": "o",
            "Ộ": "O",
            "ộ": "o",
            "Ớ": "O",
            "ớ": "o",
            "Ờ": "O",
            "ờ": "o",
            "Ở": "O",
            "ở": "o",
            "Ỡ": "O",
            "ỡ": "o",
            "Ợ": "O",
            "ợ": "o",
            "Ụ": "U",
            "ụ": "u",
            "Ủ": "U",
            "ủ": "u",
            "Ứ": "U",
            "ứ": "u",
            "Ừ": "U",
            "ừ": "u",
            "Ử": "U",
            "ử": "u",
            "Ữ": "U",
            "ữ": "u",
            "Ự": "U",
            "ự": "u",
            "Ỳ": "Y",
            "ỳ": "y",
            "Ỵ": "Y",
            "ỵ": "y",
            "Ỷ": "Y",
            "ỷ": "y",
            "Ỹ": "Y",
            "ỹ": "y",
            "–": "-",
            "‘": "'",
            "’": "'",
            "“": '\\"',
            "”": '\\"',
            "„": '\\"',
            "†": "+",
            "•": "*",
            "…": "...",
            "₠": "ecu",
            "₢": "cruzeiro",
            "₣": "french franc",
            "₤": "lira",
            "₥": "mill",
            "₦": "naira",
            "₧": "peseta",
            "₨": "rupee",
            "₩": "won",
            "₪": "new shequel",
            "₫": "dong",
            "€": "euro",
            "₭": "kip",
            "₮": "tugrik",
            "₯": "drachma",
            "₰": "penny",
            "₱": "peso",
            "₲": "guarani",
            "₳": "austral",
            "₴": "hryvnia",
            "₵": "cedi",
            "₸": "kazakhstani tenge",
            "₹": "indian rupee",
            "₺": "turkish lira",
            "₽": "russian ruble",
            "₿": "bitcoin",
            "℠": "sm",
            "™": "tm",
            "∂": "d",
            "∆": "delta",
            "∑": "sum",
            "∞": "infinity",
            "♥": "love",
            "元": "yuan",
            "円": "yen",
            "﷼": "rial",
            "ﻵ": "laa",
            "ﻷ": "laa",
            "ﻹ": "lai",
            "ﻻ": "la",
        }
        # Initialize the locales by parsing the JSON string from JS
        self.locales: Dict[str, Dict[str, str]] = {
            "bg": {
                "Й": "Y",
                "Ц": "Ts",
                "Щ": "Sht",
                "Ъ": "A",
                "Ь": "Y",
                "й": "y",
                "ц": "ts",
                "щ": "sht",
                "ъ": "a",
                "ь": "y",
            },
            "de": {
                "Ä": "AE",
                "ä": "ae",
                "Ö": "OE",
                "ö": "oe",
                "Ü": "UE",
                "ü": "ue",
                "ß": "ss",
                "%": "prozent",
                "&": "und",
                "|": "oder",
                "∑": "summe",
                "∞": "unendlich",
                "♥": "liebe",
            },
            "es": {
                "%": "por ciento",
                "&": "y",
                "<": "menor que",
                ">": "mayor que",
                "|": "o",
                "¢": "centavos",
                "£": "libras",
                "¤": "moneda",
                "₣": "francos",
                "∑": "suma",
                "∞": "infinito",
                "♥": "amor",
            },
            "fr": {
                "%": "pourcent",
                "&": "et",
                "<": "plus petit",
                ">": "plus grand",
                "|": "ou",
                "¢": "centime",
                "£": "livre",
                "¤": "devise",
                "₣": "franc",
                "∑": "somme",
                "∞": "infini",
                "♥": "amour",
            },
            "pt": {
                "%": "porcento",
                "&": "e",
                "<": "menor",
                ">": "maior",
                "|": "ou",
                "¢": "centavo",
                "∑": "soma",
                "£": "libra",
                "∞": "infinito",
                "♥": "amor",
            },
            "uk": {
                "И": "Y",
                "и": "y",
                "Й": "Y",
                "й": "y",
                "Ц": "Ts",
                "ц": "ts",
                "Х": "Kh",
                "х": "kh",
                "Щ": "Shch",
                "щ": "shch",
                "Г": "H",
                "г": "h",
            },
            "vi": {"Đ": "D", "đ": "d"},
            "da": {
                "Ø": "OE",
                "ø": "oe",
                "Å": "AA",
                "å": "aa",
                "%": "procent",
                "&": "og",
                "|": "eller",
                "$": "dollar",
                "<": "mindre end",
                ">": "større end",
            },
            "nb": {
                "&": "og",
                "Å": "AA",
                "Æ": "AE",
                "Ø": "OE",
                "å": "aa",
                "æ": "ae",
                "ø": "oe",
            },
            "it": {"&": "e"},
            "nl": {"&": "en"},
            "sv": {
                "&": "och",
                "Å": "AA",
                "Ä": "AE",
                "Ö": "OE",
                "å": "aa",
                "ä": "ae",
                "ö": "oe",
            },
        }

    def extend(self, custom_map: Dict[str, str]) -> None:
        """
        Extend the character map with a custom mapping.
        """
        self.char_map.update(custom_map)

    def slugify(
        self,
        string: str,
        locale: Optional[str] = None,
        replacement: str = "-",
        trim: bool = True,
        remove: Optional[Union[str, Pattern]] = None,
        strict: bool = False,
        lower: bool = False,
    ) -> str:
        """
        Convert a string into a slug.

        Parameters:
            string (str): The input string to slugify.
            locale (str, optional): Locale code to apply locale-specific mappings.
            replacement (str, optional): Replacement character for spaces. Default is '-'.
            trim (bool, optional): Whether to trim leading and trailing replacement characters. Default is True.
            remove (str or Pattern, optional): Regex pattern to remove unwanted characters.
                Default is r'[^\w\s$*_+~.()\'"!\-:@]+'
            strict (bool, optional): If True, remove all non-alphanumeric characters. Default is False.
            lower (bool, optional): If True, convert slug to lowercase. Default is False.

        Returns:
            str: The slugified string.
        """
        if not isinstance(string, str):
            raise ValueError("slugify: string argument expected")

        # Get locale-specific character mapping
        locale_map = self.locales.get(locale, {})

        # Normalize the string using NFKD to separate characters and diacritics
        normalized = unicodedata.normalize("NFKD", string)

        # Replace characters based on locale and char_map
        replaced = []
        for char in normalized:
            # Apply locale-specific mapping first
            if char in locale_map:
                replacement_char = locale_map[char]
            else:
                # Fallback to global char_map
                replacement_char = self.char_map.get(char, char)

            # If the replacement character is the same as the replacement symbol, turn it into space
            if replacement_char == replacement:
                replacement_char = " "

            # Replace the character and remove unwanted characters
            if remove:
                if isinstance(remove, str):
                    # Compile the pattern if it's a string
                    pattern = re.compile(remove)
                else:
                    pattern = remove  # Assume it's a compiled pattern
                replacement_char = pattern.sub("", replacement_char)
            else:
                # Default removal pattern similar to JS: /[^\w\s$*_+~.()'"!\-:@]+/g
                replacement_char = re.sub(
                    r'[^\w\s$*_+~.()\'"!\-:@]+', "", replacement_char
                )

            replaced.append(replacement_char)

        # Join the replaced characters
        slug = "".join(replaced)

        if strict:
            # Strict mode: remove all non-alphanumeric characters except spaces
            slug = re.sub(r"[^A-Za-z0-9\s]", "", slug)

        if trim:
            # Trim leading and trailing replacement characters or spaces
            slug = slug.strip()

        # Replace spaces (including multiple spaces) with the replacement character
        slug = re.sub(r"\s+", replacement, slug)

        if lower:
            slug = slug.lower()

        return slug


COUNTER: int = 1
ENTRIES: list[dict] = []
PROCESSED: set[str] = set()
slugifier = Slugify()


def get_batch_of_content_from_ask(
    client: QdrantClient, collection: str, offset: str | None, limit: int = 100
):
    results = client.scroll(
        collection_name=collection,
        limit=limit,
        offset=str(offset) if isinstance(offset, uuid.UUID) else offset,
        with_payload=True,
    )
    if not results:
        return [], None
    # convert the record into a dictionary
    results_converted = [
        {"id": record.payload["id"], "title": record.payload["title"]}
        for record in results[0]
        if "id" in record.payload and "title" in record.payload
    ]
    # check if any of the records are already processed
    results_converted = [
        record for record in results_converted if record["id"] not in PROCESSED
    ]
    return results_converted, results[1]


def write_single_sitemap_file(output: str, max_entries: int) -> None:
    global COUNTER
    global ENTRIES
    sitemap = """<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">"""
    for entry in ENTRIES[:max_entries]:
        slug = slugifier.slugify(entry["title"])
        url = f"https://ask.orkg.org/item/{entry['id']}/{slug}"
        sitemap += f"""
      <url>
        <loc>{url}</loc>
      </url>
    """
    sitemap += "</urlset>"

    # Write the sitemap file
    with open(os.path.join(output, f"sitemap-{COUNTER}.xml"), "w") as f:
        f.write(sitemap)
    # Compress the file
    with open(os.path.join(output, f"sitemap-{COUNTER}.xml"), "rb") as f_in:
        with gzip.open(
            os.path.join(output, f"sitemap-{COUNTER}.xml.gz"), "wb"
        ) as f_out:
            f_out.writelines(f_in)
    # Delete the uncompressed file
    os.remove(os.path.join(output, f"sitemap-{COUNTER}.xml"))

    print(f"File {COUNTER} created")
    COUNTER += 1
    ENTRIES = ENTRIES[max_entries:]


@click.command()
@click.option("--output", "-o", help="Output directory path", required=True)
@click.option("--limit", "-l", help="Limit of items to fetch each call", default=100)
@click.option("--collection", "-c", help="Collection to fetch", default="papers")
@click.option(
    "--include-lastmod",
    "-i",
    help="Include lastmod in the sitemap",
    is_flag=True,
    default=False,
)
@click.option(
    "--max-entries-per-file",
    "-m",
    help="Maximum number of entries per file",
    default=50000,
)
@click.option("--qdrant-host", "-q", help="Qdrant host URL")
@click.option(
    "--task", "-t", help="Task to run", type=click.Choice(["create", "index"])
)
def main(
    output, limit, collection, include_lastmod, max_entries_per_file, qdrant_host, task
):
    if task == "create":
        if qdrant_host is None:
            raise ValueError("Qdrant host URL is required for the create task")
        global ENTRIES
        client = QdrantClient(qdrant_host)
        if not os.path.exists(output):
            os.makedirs(output)
        offset = None
        while True:
            lst, offset = get_batch_of_content_from_ask(
                client, collection, offset, limit
            )
            if not lst:
                break
            ENTRIES.extend(lst)
            PROCESSED.update([record["id"] for record in lst])
            if len(ENTRIES) >= max_entries_per_file:
                write_single_sitemap_file(output, max_entries_per_file)
    else:
        # Create a xml file with the name sitemap_index.xml
        # The file will contain the list of sitemap files
        sitemap_index = """<?xml version="1.0" encoding="UTF-8"?>
    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">"""
        # iterate the output directory and get all the sitemap files
        for file in os.listdir(output):
            if file.endswith(".xml.gz"):
                sitemap_index += f"""
        <sitemap>
            <loc>https://ask.orkg.org/sitemaps/{file}</loc>
        </sitemap>
    """
        sitemap_index += "</sitemapindex>"
        # Write the sitemap_index file
        with open(os.path.join(output, "sitemap_index.xml"), "w") as f:
            f.write(sitemap_index)
        print("Sitemap index file created")


if __name__ == "__main__":
    main()
