from abc import ABC, abstractmethod
from typing import Any, Optional


class ICache(ABC):
    @abstractmethod
    def search(
        self,
        collection: str,
        page: Optional[int] = None,
        per_page: int = 30,
        sort: Optional[str] = None,
        filters: Optional[str] = None,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ):
        raise NotImplementedError("The list_or_search method must be implemented!")

    @abstractmethod
    def get_all(
        self,
        collection: str,
        sort: Optional[str] = None,
        filters: Optional[str] = None,
        expand: Optional[str] = None,
        fields: Optional[str] = None,
    ):
        raise NotImplementedError("The get_all method must be implemented!")

    @abstractmethod
    def get(
        self,
        collection: str,
        item_id: str,
        expand: Optional[str],
        fields: Optional[str],
    ):
        raise NotImplementedError("The get method must be implemented!")

    @abstractmethod
    def insert(
        self, collection: str, item: Any, expand: Optional[str], fields: Optional[str]
    ):
        raise NotImplementedError("The insert method must be implemented!")

    @abstractmethod
    def upsert(
        self,
        collection: str,
        item: Any,
        expand: Optional[str],
        fields: Optional[str],
        overwrite_on: Optional[str],
    ):
        raise NotImplementedError("The upsert method must be implemented!")

    @abstractmethod
    def update(
        self,
        collection: str,
        item_id: str,
        item: Any,
        expand: Optional[str],
        fields: Optional[str],
    ):
        raise NotImplementedError("The update method must be implemented!")

    @abstractmethod
    def delete(self, collection: str, item_id: str) -> bool:
        raise NotImplementedError("The delete method must be implemented!")
