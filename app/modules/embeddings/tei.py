import json
from typing import List

import requests

from app.core import settings
from app.modules.embeddings.base import EmbeddingsEngine, Text


class TextEmbeddingsInferenceEngine(EmbeddingsEngine):
    _ndims: int

    @staticmethod
    def _ask_for_embeddings(texts: Text) -> List[List]:
        response = requests.post(
            f"{settings.embeddings.host}{'' if settings.embeddings.host.endswith('/') else '/'}embed",
            json={"normalize": True, "truncate": True, "inputs": texts},
        )
        response.raise_for_status()
        return response.json()

    @staticmethod
    def _tokenize_text(text: str) -> List:
        response = requests.request(
            "POST",
            f"{settings.embeddings.host}{'' if settings.embeddings.host.endswith('/') else '/'}tokenize",
            headers={"Content-Type": "application/json"},
            data=json.dumps({"inputs": text}),
        )
        response.raise_for_status()
        return response.json()[0]

    def _cut_text_based_on_token_count(
        self, text: str, max_token_size: int = 8192
    ) -> str:
        tokens = self._tokenize_text(text)
        if len(tokens) > max_token_size:
            return "".join(
                [t["text"] for t in tokens[:max_token_size] if not t["special"]]
            )
        return text

    def compute_query_embeddings(self, query: str, *args, **kwargs) -> List[List]:
        query = self._cut_text_based_on_token_count(query)
        return self._ask_for_embeddings(texts=[query])

    def compute_source_embeddings(self, texts: Text, *args, **kwargs) -> List[List]:
        texts = self.sanitize_input(texts)
        texts = [self._cut_text_based_on_token_count(text) for text in texts]
        return self._ask_for_embeddings(texts=texts)

    def ndims(self) -> int:
        if self._ndims is None:
            self._ndims = len(self._ask_for_embeddings(["hello"])[0])
        return self._ndims
