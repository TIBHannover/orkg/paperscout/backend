# ORKG-Ask Backend
This is the backend that serves the ORKG-Ask app, as deployed under https://api.ask.orkg.org.
It is written mainly in Python 3.11 using the [FastAPI](https://fastapi.tiangolo.com/) framework.

## Components
The backend is composed of the following components:
* The FastAPI application which contains the logic for all RAG operations (main entry point).
* LLM inference engine: The backend supports two providers right now, [Ollama](https://ollama.com/) and [TGI](https://huggingface.co/docs/text-generation-inference/en/index).
* Embeddings engine that semantically embeds queries and documents. The backend supports two providers at the moment, [Infinity](https://github.com/michaelfeil/infinity) and [TEI](https://huggingface.co/docs/text-embeddings-inference/en/index).
* Caching engine to cache and reserve LLM responses faster. The backend supports [Pocketbase](https://pocketbase.io/) for just that.
* Vector store to perform neural document search and similarity. The store of choice is [Qdrant](https://qdrant.tech/).
* Proxy application to load balance the requests to multiple LLM instances. The backend has its own version based on the work by [ParisNeo](https://github.com/ParisNeo/ollama_proxy_server).

## How to run the application
You can easily run the whole thing with the following command:
```bash
$ docker-compose up -d
```

Note: you need to have enough resources to run the backend. The docker compose will contain the following services:
* The FastAPI application
* Multiple instances of the LLM server
* A PocketBase instance
* A Qdrant instance
* An embeddings engine
* A proxy server for the LLMs


### Getting started locally
The project uses poetry to manage the dependencies. To install poetry, follow the instructions [here](https://python-poetry.org/docs/#installation).

Once you have poetry installed, you can install the dependencies using the following command:
```bash
$ poetry install
```

Copy the file `config.toml.example` to `config.toml` and fill in the required configuration.
Make any necessary changes to it to match your local setup.
```bash
$ cp config.toml.example config.toml
```

For development make sure that you run the following command to install the development dependencies:
```bash
$ pre-commit install
$ pre-commit install --hook-type commit-msg
```

Note 1: the repo uses conventional commits. Make sure to follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) when committing your changes.

Note 2: During development, we recommend always using `ASK_CONFIG` to point to the absolute path of the configuration file.
For this you can either create a `.env` file in the root of the project and add the following line:
```bash
ASK_CONFIG=/path/to/config.toml
```
or you can set the environment variable directly in your terminal:
```bash
$ export ASK_CONFIG=/path/to/config.toml
```

(Recommended) Some IDEs like PyCharm allow you to set environment variables in the run configuration.

Finally, you can run the server using the following command:
```bash
$ poetry run uvicorn main:app --reload
```

For debugging purposes, you can use run the `main.py` file within your IDE which will enable you to debug the code.

We recommend that you check the [contribution guidelines](CONTRIBUTING.md) before contributing to the project.

### Configuring the application
The application uses a toml file for the configuration.

In production the default location for the file is `/ask/config/config.toml`.
You can override this by setting the `ASK_CONFIG` environment variable to any other desired location.

The configuration is based on the following pydantic classes:

[AskSettings](app/core/config/models.py)

### Security of the application
The application's configuration allows you to set the security settings for the application.

You can enable or disabled protected access to the API.
You are also able to select either the HTTP verbs or the endpoints that you want to protect.

Furthermore, you can either set a static token for access or if not set,
the system will default to reading the tokens from the internal store.
