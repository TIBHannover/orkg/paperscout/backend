from typing import Callable, Dict, List, Optional

from app.errors import StateMachineNotInitializedError
from app.modules.caching.models import UsedPrompt
from app.modules.llms.base import Chain, ParserOutput, PromptTemplate
from app.modules.prompting.prompts import PromptIds
from app.modules.statemachine.state import State
from app.modules.statemachine.states.common import FetchPromptState
from app.modules.statemachine.states.kvp import (
    AddKVPResponseToCacheState,
    CheckKVPItemInCacheState,
    GetKVPPaperContentState,
    MakeKVPLLMChainState,
    RunKVPLLMChainState,
    TranslateKVPResponseState,
)
from app.modules.statemachine.states.syn import (
    AddSynResponseToCacheState,
    CheckSynInCacheState,
    GetItemsContentState,
    MakeSynLLMChainState,
    RunSynLLMChainState,
    TranslateSynthesisResponseState,
)
from app.modules.statemachine.transition import Transition


class Machine:
    states: List[State]
    start_state: State
    transitions: List[Transition]
    current_state: State

    def __init__(self):
        self.states = []
        self.transitions = []
        self.start_state = None
        self.current_state = None

    @property
    def is_ready(self) -> bool:
        """
        Check if the state machine is ready to run.
        :return: True if the state machine is ready to run, False otherwise
        """
        return (
            len(self.states) > 0
            and len(self.transitions) > 0
            and self.start_state is not None
        )

    def add_state(
        self,
        state: State,
        from_state: Optional[State] = None,
        to_state: Optional[State] = None,
        condition: Optional[Callable[[], bool]] = None,
        is_start: bool = False,
    ):
        """
        Add a state to the state machine with optional transitions.
        When specifying both from_state and to_state, two transitions will be added!
        :param state: the state to add
        :param from_state: from which state to transition here
        :param to_state: to which state to transition
        :param condition: an optional condition to check before transitioning
        :param is_start: an optional flag to set the start state
        """
        if state not in self.states:
            self.states.append(state)
        if is_start:
            if self.start_state is not None:
                raise ValueError("Start state already set")
            self.start_state = state
        if (from_state and not to_state) or (to_state and not from_state):
            self._add_transition(
                from_state=from_state if from_state else state,
                to_state=to_state if to_state else state,
                condition=condition,
            )
        elif from_state and to_state:
            # add the first transition
            self._add_transition(
                from_state=from_state,
                to_state=state,
                condition=condition,
            )
            # add the second transition
            self._add_transition(
                from_state=state,
                to_state=to_state,
            )

    def _add_transition(
        self,
        from_state: State,
        to_state: State,
        condition: Optional[Callable[[], bool]] = None,
        unique: bool = False,
    ):
        """
        Add a transition between two states.
        :param from_state: from which state to transition
        :param to_state: to which state to transition
        :param condition: an optional condition to check before transitioning
        :param unique: an optional flag to check if the transition is unique
        """
        if from_state not in self.states:
            self.states.append(from_state)
        if to_state not in self.states:
            self.states.append(to_state)
        if unique:
            if any(
                t.from_state == from_state
                and t.to_state == to_state
                and t.condition == condition
                for t in self.transitions
            ):
                return
        self.transitions.append(Transition(from_state, to_state, condition))

    def run(self):
        """
        Invoke the state machine and start running the states.
        """
        if not self.is_ready:
            raise StateMachineNotInitializedError(
                "State machine is not ready to run, make sure the current state is "
                "set and there are states and transitions"
            )
        self.current_state = self.start_state
        transitions = [
            t for t in self.transitions if t.from_state == self.current_state
        ]
        self.current_state.run()
        while len(transitions) > 0:
            transition = transitions.pop(0)
            if transition.can_transition:
                self.current_state = transition.to_state
                transitions = [
                    t for t in self.transitions if t.from_state == self.current_state
                ]
                self.current_state.run()


class LLMStateMachine(Machine):
    """
    Represents a state machine for any LLM task.
    """

    prompt_id: PromptIds
    chain: Chain
    llm_response: ParserOutput
    template: PromptTemplate
    user_kwargs: Dict
    machine_kwargs: Dict

    def __init__(self):
        super().__init__()
        self.prompt_id = None
        self.chain = None
        self.llm_response = None
        self.template = None
        self.user_kwargs = {}
        self.machine_kwargs = {}


class CachableLLMStateMachine(LLMStateMachine):
    """
    Represents a state machine for any LLM task that can be cached.
    """

    in_cache: bool
    invalidate_cache: bool

    def __init__(self):
        super().__init__()
        self.in_cache = False
        self.invalidate_cache = False


class MultiLingualLLMStateMachine(CachableLLMStateMachine):
    """
    Represents a state machine for any LLM task that can be translated to other languages.
    """

    translation_language: str | None

    def __init__(self):
        super().__init__()
        self.translation_language = None


class SeededLLMStateMachine(MultiLingualLLMStateMachine):
    """
    Represents a state machine for any LLM task that can be seeded.
    """

    seed: int | None

    def __init__(self):
        super().__init__()
        self.seed = None


class ReproducibleLLMStateMachine(SeededLLMStateMachine):
    """
    Represents a state machine for any LLM task that can be reproduced.
    """

    prompts_used: Dict[str, UsedPrompt]
    should_fetch_prompts: bool

    def __init__(self):
        super().__init__()
        self.prompts_used = dict()
        self.should_fetch_prompts = True


class KeyValuePairsExtractionStateMachine(ReproducibleLLMStateMachine):
    """
    Represents a state machine for the task of extracting key-value pairs from a given item.
    This LLM state machine is responsible for fetching the prompt, getting the paper content,
    making the LLM chain, and running the LLM chain.
    """

    item_id: int | str | None
    collection_item_id: str | None
    paper_content: dict
    llm_response: ParserOutput[Dict]
    prompts_used: Dict[str, UsedPrompt]

    def __init__(
        self,
        prompt_id: PromptIds,
        item_id: int | str | None,
        collection_item_id: str | None,
        invalidate_cache: bool,
        response_language: str | None,
        seed: int | None,
        should_fetch_prompts: bool,
        **kwargs
    ):
        super().__init__()
        self._set_states()
        self.prompt_id = prompt_id
        self.collection_item_id = collection_item_id
        self.item_id = item_id
        self.user_kwargs = kwargs
        self.in_cache = False
        self.machine_kwargs = {}
        self.invalidate_cache = invalidate_cache
        self.translation_language = response_language
        self.seed = seed
        self.prompts_used = {}
        self.should_fetch_prompts = should_fetch_prompts

    @classmethod
    def from_values(
        cls,
        prompt_id: PromptIds,
        item_id: int | str | None,
        collection_item_id: str | None,
        invalidate_cache: bool = False,
        response_language: str | None = None,
        seed: int | None = None,
        should_fetch_prompts: bool = False,
        **kwargs
    ):
        """
        Factory method to create an instance of the LLMStateMachine.
        :param prompt_id: the prompt id
        :param item_id: the item id (optional)
        :param collection_item_id: the collection item id (optional)
        :param invalidate_cache: whether to invalidate the cache
        :param response_language: the response language (optional)
        :param seed: the seed to use during extraction (optional)
        :param should_fetch_prompts: whether to fetch prompts (default: False)
        :param kwargs: any keyword arguments
        """
        return cls(
            prompt_id,
            item_id,
            collection_item_id,
            invalidate_cache,
            response_language,
            seed,
            should_fetch_prompts,
            **kwargs
        )

    def _set_states(self):
        # Initialize states
        fetch_prompt_state = FetchPromptState(self)
        get_paper_content_state = GetKVPPaperContentState(self)
        make_llm_chain_state = MakeKVPLLMChainState(self)
        run_llm_chain_state = RunKVPLLMChainState(self)
        check_item_in_cache_state = CheckKVPItemInCacheState(self)
        translate_response_state = TranslateKVPResponseState(self)
        add_to_cache_state = AddKVPResponseToCacheState(self)

        # Add states to the state machine with their transitions
        self.add_state(
            fetch_prompt_state,
            is_start=True,
            to_state=get_paper_content_state,
            condition=lambda: self.invalidate_cache,
        )
        self.add_state(
            check_item_in_cache_state,
            from_state=fetch_prompt_state,
        )
        self.add_state(
            get_paper_content_state,
            from_state=check_item_in_cache_state,
            condition=lambda: not self.in_cache or "cache" in self.machine_kwargs,
        )
        self.add_state(
            make_llm_chain_state,
            from_state=get_paper_content_state,
        )
        self.add_state(
            run_llm_chain_state,
            from_state=make_llm_chain_state,
        )
        self.add_state(
            translate_response_state,
            from_state=run_llm_chain_state,
            condition=lambda: self.translation_language is not None,
            to_state=add_to_cache_state,
        )
        self.add_state(
            add_to_cache_state,
            from_state=run_llm_chain_state,
        )


class SynthesisStateMachine(ReproducibleLLMStateMachine):
    """
    Represents a state machine for the task of synthesizing abstracts for a given question.
    This LLM state machine is responsible for making the LLM chain and running the LLM chain.
    """

    item_ids: List[int | str]
    collection_item_ids: List[int | str]
    question: str
    abstracts: List[str]
    mappings: Dict[str, Dict[int, str | int]]
    llm_response: ParserOutput[Dict]
    prompts_used: Dict[str, UsedPrompt]

    def __init__(
        self,
        prompt_id: PromptIds,
        item_ids: List[int | str],
        collection_item_ids: List[int | str],
        question: str,
        invalidate_cache: bool,
        response_language: str | None,
        seed: int | None,
        should_fetch_prompts: bool,
        **kwargs
    ):
        super().__init__()
        self._set_states()
        self.prompt_id = prompt_id
        self.item_ids = item_ids
        self.collection_item_ids = collection_item_ids
        self.question = question
        self.user_kwargs = kwargs
        self.in_cache = False
        self.machine_kwargs = {}
        self.invalidate_cache = invalidate_cache
        self.translation_language = response_language
        self.mappings = {"collection": {}, "core": {}}
        self.seed = seed
        self.prompts_used = {}
        self.should_fetch_prompts = should_fetch_prompts

    @classmethod
    def from_values(
        cls,
        prompt_id: PromptIds,
        item_ids: List[int | str],
        collection_item_ids: List[int | str],
        question: str,
        invalidate_cache: bool = False,
        response_language: str | None = None,
        seed: int | None = None,
        should_fetch_prompts: bool = False,
        **kwargs
    ):
        """
        Factory method to create an instance of the LLMStateMachine.
        :param prompt_id: the prompt id
        :param item_ids: the item ids
        :param collection_item_ids: the custom item ids
        :param question: the research question
        :param invalidate_cache: whether to invalidate the cache (default: False)
        :param response_language: the response language (optional)
        :param seed: the seed to use during synthesis (optional)
        :param should_fetch_prompts: whether to fetch prompts (default: False)
        :param kwargs: any keyword arguments
        """
        return cls(
            prompt_id,
            item_ids,
            collection_item_ids,
            question,
            invalidate_cache,
            response_language,
            seed,
            should_fetch_prompts,
            **kwargs
        )

    def _set_states(self):
        # Initialize states
        fetch_prompt_state = FetchPromptState(self)
        check_synthesis_in_cache_state = CheckSynInCacheState(self)
        get_items_content_state = GetItemsContentState(self)
        make_llm_chain_state = MakeSynLLMChainState(self)
        run_llm_chain_state = RunSynLLMChainState(self)
        add_to_cache_state = AddSynResponseToCacheState(self)
        translate_response_state = TranslateSynthesisResponseState(self)

        # Add states to the state machine with their transitions
        self.add_state(
            fetch_prompt_state,
            is_start=True,
        )
        self.add_state(
            check_synthesis_in_cache_state,
            from_state=fetch_prompt_state,
        )
        self.add_state(
            get_items_content_state,
            from_state=check_synthesis_in_cache_state,
            condition=lambda: not self.in_cache or self.invalidate_cache,
        )
        self.add_state(
            make_llm_chain_state,
            from_state=get_items_content_state,
        )
        self.add_state(
            run_llm_chain_state,
            from_state=make_llm_chain_state,
        )
        self.add_state(
            translate_response_state,
            from_state=run_llm_chain_state,
            condition=lambda: self.translation_language is not None,
            to_state=add_to_cache_state,
        )
        self.add_state(
            add_to_cache_state,
            from_state=run_llm_chain_state,
        )
