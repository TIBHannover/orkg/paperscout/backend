import json
import os
import random

import click
import openai
from openai import OpenAI
from tqdm import tqdm

random.seed(42)

LANGUAGES = [
    "Spanish",
    "French",
    "German",
    "Italian",
    "Dutch",
    "Portuguese",
    "Russian",
    "Chinese",
    "Japanese",
    "Korean",
    "Arabic",
    "Farsi",
]

LEVELS = {
    "Child": "The language is at a middle school aged child's level. It is simple and easy to understand.",
    "Teenager": "The language is at a (high school) teenager's level. It is more complex than a child's level but still easy to understand.",
    "Adult": "The language is at an adult's level. It is complex and contains few (if any) technical terms.",
}

PROPERTIES = {
    "Methods": "The methods used in the work.",
    "Results": "The key results of the work or the study.",
    "Conclusions": "The conclusions of the work.",
    "Insights": "Any insights or observations that are not covered by the above categories.",
    "TL;DR": "A short summary of the work.",
    "City": "The city where the work was conducted.",
    "Dataset": "The dataset(s) used in the evaluation.",
    "Platform": "The platforms investigated in the work.",
    "Sample Size": "The sample size of the study.",
}


def get_some_languages(count: int) -> list[str]:
    if count > len(LANGUAGES):
        return LANGUAGES
    if count == 1:
        return ["English"]
    return ["English"] + random.sample(LANGUAGES, count - 1)


def get_some_levels(count: int, add_researcher: bool = True) -> list[dict]:
    if count > len(LEVELS):
        return [{"level": level, "description": desc} for level, desc in LEVELS.items()]
    levels = random.sample(list(LEVELS.items()), count)
    if add_researcher:
        levels.append(
            (
                "Researcher",
                "The language is at a researcher's level. It is highly complex and contains technical terms.",
            )
        )
    return [{"level": level, "description": desc} for level, desc in levels]


def get_some_properties(count: int, include_answer: bool = True) -> list[dict]:
    properties = random.sample(list(PROPERTIES.items()), count)
    if include_answer:
        properties.append(("Answer", "The answer to the question."))
    return [{"property": prop, "description": desc} for prop, desc in properties]


def fix_json_via_llm(client: OpenAI, defect_json: str) -> str:
    response = client.chat.completions.create(
        model="gpt-4o-mini",
        messages=[
            {
                "role": "system",
                "content": "You are a JSON fixer bot. Your task is to fix the provided JSON. Only output the fixed JSON.",
            },
            {"role": "user", "content": defect_json},
        ],
    )
    fixed_json = response.choices[0].message.content
    if fixed_json.startswith("```json\n"):
        fixed_json = fixed_json[8:-3]
    return fixed_json


@click.command()
@click.option("--api-key", "-a", help="OpenAI API key", required=True)
@click.option("--model", "-m", default="gpt-4o-mini", help="OpenAI model name")
@click.option(
    "--input-file",
    "-i",
    help="Input datasets in json file as created by the fetching.py script",
    required=True,
)
@click.option(
    "--output-file",
    "-o",
    help="Output file to store the generated data in jsonl format",
    required=True,
)
def extract_information(api_key: str, model: str, input_file: str, output_file: str):
    client = openai.OpenAI(api_key=api_key)
    with open(input_file, "r") as f:
        data = json.load(f)
        prompt = """You are a scientific information extraction system. You extract information in an abstractive way and not extractive. You operate on scholarly text.
You will be asked to do a task and you should follow it exactly. You will be provided with a set of properties and their descriptions to extract. Also provided with the language of the text and the level of the language."""
        extraction_prompt = """You should extract that values of following properties from the provided text as well as the which part of the text they came from (verbatim):
{}
And also answer the question: {}

Your answer should be as a JSON object like the following example:
{{
    "methods": [{{ "text": "method 1", "source": ["part of the text"] }}, {{ "text": "method 2", "source": ["part of the text"] }}],
    "results": [{{ "text": "result 1", "source": ["part of the text 1", "part of the text 2"] }}],
    "conclusions": [{{ "text": "conclusion 1", "source": ["part of the text 1", "part of the text 2"]  }}],
    ....
    "answer": {{ "text": "answer to the question", "source": ["part of the text 1", "part of the text 2"] }}
}}

if a property is not present in the text, you must leave it as a null.
The extracted values MUST be paraphrased to fit the question. But the sources should always be verbatim.
Make sure that your values are all in "{}" and are relevant to the question and the text provided. The sources must remain in their original language!.

IMPORTANT: The extracted values must be paraphrased to a "{}" language level ({}).

The source text is:
"""
        # check if output file exists then get a count of the number of lines in the file
        num_lines = -6
        if os.path.exists(output_file):
            with open(output_file, "r") as f:
                num_lines = sum(1 for line in f)
        counter = 0
        with open(output_file, "a+", encoding="utf8") as jsonl, open(
            "error.log", "a+", encoding="utf8"
        ) as error_log:
            for item in tqdm(data, unit="question", desc="Generating extraction data"):
                question = item["question"]
                documents = item["documents"][:5]
                if len(documents) < 3:
                    continue
                # randomly select 3 documents
                documents = random.sample(documents, 3)
                for document in tqdm(documents, unit="document", leave=False):
                    text = document["title"] + "\n" + document["abstract"]
                    if "full_text" in document and document["full_text"]:
                        text += "\n" + document["full_text"]
                    languages = get_some_languages(random.randint(3, 5))
                    levels = get_some_levels(random.randint(1, 2))
                    properties = get_some_properties(random.randint(2, 4))
                    properties_text = "\n".join(
                        [
                            f"- {prop['property']}: {prop['description']}"
                            for prop in properties
                        ]
                    )
                    for language in tqdm(languages, unit="language", leave=False):
                        for level in levels:
                            counter += 1
                            if counter <= num_lines:
                                continue
                            extraction_prompt_instance = extraction_prompt.format(
                                properties_text,
                                question,
                                language,
                                level["level"],
                                level["description"],
                            )
                            try:
                                response = client.chat.completions.create(
                                    model=model,
                                    messages=[
                                        {"role": "system", "content": prompt},
                                        {
                                            "role": "user",
                                            "content": extraction_prompt_instance,
                                        },
                                        {"role": "system", "content": text},
                                    ],
                                )
                            except openai.BadRequestError as e:
                                print(f"Error: {e}")
                                continue
                            extraction = response.choices[0].message.content
                            if extraction.startswith("```json\n"):
                                extraction = extraction[8:-3]
                            try:
                                parsed_extraction = json.loads(extraction)
                            except json.JSONDecodeError:
                                try:
                                    parsed_extraction = json.loads(
                                        fix_json_via_llm(client, extraction)
                                    )
                                except json.JSONDecodeError:
                                    print("Error parsing response!")
                                    error_log.write(f"{extraction}\n\n\n")
                                    continue
                            jsonl.write(
                                json.dumps(
                                    {
                                        "question": question,
                                        "document": document,
                                        "language": language,
                                        "level": level,
                                        "properties": properties,
                                        "extraction": parsed_extraction,
                                    },
                                    ensure_ascii=False,
                                )
                                + "\n"
                            )


def translate_questions_and_update_jsonl(
    input_file: str, output_file: str, model: str, api_key: str
):
    client = openai.OpenAI(api_key=api_key)
    with open(input_file, "r", encoding="utf8") as f:
        # Collect a list of the unique questions and translate them to all the available languages (except English)
        questions = list(set([json.loads(line)["question"] for line in f]))
        languages = LANGUAGES.copy()
        full_list = {}
        with open(output_file, "a+", encoding="utf8") as jsonl:
            for question in tqdm(
                questions, unit="question", desc="Translating questions"
            ):
                response = client.chat.completions.create(
                    model=model,
                    messages=[
                        {
                            "role": "system",
                            "content": f"""You are a translation bot. Your task is to translate the provided research question to the following languages:
{languages}. You output is a JSON object with the following format: {{"Spanish": "translated question", "French": "translated question", ...}}""",
                        },
                        {"role": "user", "content": question},
                    ],
                )
                translation = response.choices[0].message.content
                if translation.startswith("```json\n"):
                    translation = translation[8:-3]
                try:
                    translated_questions = json.loads(translation)
                except json.JSONDecodeError:
                    print("Error parsing response!")
                    continue
                full_list[question] = translated_questions
            # Write the final list to the output file
            jsonl.write(json.dumps(full_list, ensure_ascii=False) + "\n")


if __name__ == "__main__":
    extract_information()
