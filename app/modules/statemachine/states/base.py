from typing import TYPE_CHECKING

from app.modules.statemachine.state import State

if TYPE_CHECKING:
    from app.modules.statemachine.machines import (
        KeyValuePairsExtractionStateMachine,
        LLMStateMachine,
        SynthesisStateMachine,
    )


class LLMState(State):
    machine: "LLMStateMachine"


class KVPLLMState(LLMState):
    machine: "KeyValuePairsExtractionStateMachine"

    def __init__(self, machine: "KeyValuePairsExtractionStateMachine"):
        super().__init__(machine)


class SynLLMState(LLMState):
    machine: "SynthesisStateMachine"

    def __init__(self, machine: "SynthesisStateMachine"):
        super().__init__(machine)
