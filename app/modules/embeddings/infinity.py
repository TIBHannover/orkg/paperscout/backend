from typing import List

import requests

from app.core import settings
from app.modules.embeddings.base import EmbeddingsEngine, Text


class InfinityEmbeddingsEngine(EmbeddingsEngine):
    _ndims: int
    _model: str

    def __init__(self, model: str):
        self._ndims = None
        self._model = model

    def _ask_for_embeddings(self, texts: Text) -> List[List]:
        response = requests.post(
            f"{settings.embeddings.host}{'' if settings.embeddings.host.endswith('/') else '/'}embeddings",
            json={"model": self._model, "input": texts},
        )
        response.raise_for_status()
        return [obj["embedding"] for obj in response.json()["data"]]

    def compute_query_embeddings(self, query: str, *args, **kwargs) -> List[List]:
        return self._ask_for_embeddings(texts=[query])

    def compute_source_embeddings(self, texts: Text, *args, **kwargs) -> List[List]:
        texts = self.sanitize_input(texts)
        return self._ask_for_embeddings(texts=texts)

    def ndims(self) -> int:
        if self._ndims is None:
            self._ndims = len(self._ask_for_embeddings(["hello"])[0])
        return self._ndims
