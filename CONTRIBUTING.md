# ORKG-Ask contribution guidelines
Yay! you made it here!

This document will serve as the bases for your onboarding into creating/changing/proposing features into the ORKG Ask system.

The code is based mainly in python. Also note that the code contains some >python3.10 language features.

## General architecture

This figure here is a general depiction of the main components of the system and how they interact with each other.

![High level architecture](./docs/Architecture.png)


## Basic code layout
The repository is structured as follows:

Some file names are used for clarification purposes only.

```
├── app
│   ├── core # core project configuration
│   │   ├── config.py # main project settings
│   ├── models # the data models used by the API
│   │   ├── llm.py # the LLM extraction models
│   │   ├── search.py # the vector store models
│   ├── modules # the individual functionality components
│   │   ├── statemachine # for the components of the state machine
│   │   │   ├── base.py
│   │   ├── embeddings # for the components of the embeddings system
│   │   │   ├── engine.py
│   ├── services # the services where the logic is consolidated to be used by the API
│   │   ├── llm.py
│   ├── scripts # various scripts
│   │   ├── import.py
│   ├── routers # API endpoints
│   │   ├── llm.py
│   ├── app_factory.py
│   ├── main.py
├── tests
│   ├── mock
│   │   ├── # individual files that contain mocked functionality
│   ├── test_feature.py # the unit/end-to-end tests
├── deploy # deployment related functionality
│   ├── ollama
├── Dockerfile
├── docker-compose.yml
├── pyproject.toml
├── .gitlab-ci.yml
└── .pre-commit-config.yaml
```

## Features
ORKG-Ask is built with a long vision in mind. All features should be documented and discussed on the mutual board between the frontend and the backend of the system.
The roadmap of the system details which features are to be implemented and when.

### How-to start
Once a feature is selected and all the requirements are cleared out. Implementation should be straightforward.

New functionality should be implemented in its own subpackage inside the `modules` directory.
In which basic skeletons/interfaces/classes are to be added to `base.py` file. Common functionality should be in `utils.py` or `common.py` depending on their usage and how they will be invoked.

For extending existing functionality, features can be built on top of existing components. Most components have been built with extension in mind and should be easily extended.

All features should be tested. If a feature depends on external components (for instance a language model or a vector store), then the external functionality should be mocked and the added features should be tested for expected and unexpected behaviour.

## Notes on development
The repository is using pre-commit for a bunch of code organization tasks. Certain styles and notations are enforced and must be followed. The repo also employs [conventional commits](https://www.conventionalcommits.org/en/) and dependencies like `commitizen` will ensure that all commits follow the required format.

Changelog and releases are handled automatically via [semantic release](https://python-semantic-release.readthedocs.io/en/latest/).

## Other stuff
This documentation and this code base is a work in progress. If you find issues or bugs or even if you would like to improve on some parts, please feel free to create a merge request with the changes, and it will be reviewed by one of the maintainers.

Have fun!

Y.
