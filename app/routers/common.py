# -*- coding: utf-8 -*-
import datetime
import uuid
from typing import Any, Type

from pydantic import BaseModel

allowed_translation_languages = {
    "en": "english",
    "de": "german",
    "es": "spanish",
    "fr": "french",
    "it": "italian",
    "pt": "portuguese",
    "nl": "dutch",
    "ru": "russian",
    "ja": "japanese",
    "ar": "arabic",
    "fa": "farsi",
    "tr": "turkish",
    "ko": "korean",
    "zh": "chinese",
    "hi": "hindi",
    "vi": "vietnamese",
    "id": "indonesian",
}


class ResponseWrapper:
    @staticmethod
    def wrap_response(payload: Any, type_: Type[BaseModel]):
        wrapped_response = {
            "timestamp": datetime.datetime.now(),
            "uuid": uuid.uuid4(),
            "payload": payload,
        }

        return type_(**wrapped_response)
