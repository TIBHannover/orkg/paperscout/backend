from typing import Dict, List
from uuid import UUID

from pydantic import BaseModel, Field

from app.models import TimeStampedResponse
from app.modules.indexing.vectordb.models import QdrantDocument, QdrantPageResponse


class SearchRequest(BaseModel):
    query: str = Field(..., description="The text to search for")
    limit: int = Field(10, description="The maximum number of documents to return")
    offset: int | None = Field(
        None, description="The offset to start fetching documents from"
    )
    filter: str | None = Field(
        None, description="The criteria to filter the search results"
    )
    focus: List[str] | None = Field(None, description="The sources to search from")


class CountRequest(BaseModel):
    filter: str | None = Field(
        None,
        default_factory=None,
        description="The criteria to filter the search results",
    )


class ExploreRequest(BaseModel):
    filter: str = Field(..., description="The criteria to filter the retrieval request")
    limit: int = Field(10, description="The maximum number of documents to return")
    offset: UUID | None = Field(
        None,
        description="The offset internal document ID to start fetching documents from",
    )
    order_by: str | None = Field(None, description="The payload field to order by")
    focus: List[str] | None = Field(
        None, description="The sources to explore documents from"
    )


class RecommendRequest(BaseModel):
    document_ids: List[str] = Field(
        ..., description="The document IDs to be used for recommendations"
    )
    avoid_ids: List[str] | None = Field(
        None, description="The document IDs to be avoided for recommendations"
    )
    limit: int = Field(10, description="The maximum number of documents to return")
    offset: int | None = Field(
        None, description="The offset to start fetching documents from"
    )
    filter: str | None = Field(
        None, description="The criteria to filter the search results"
    )


class IndexRequest(QdrantDocument):
    ...


class BulkIndexRequest(BaseModel):
    documents: List[IndexRequest] = Field(
        ..., description="The documents to be indexed"
    )


class QdrantSingleDocumentResponse(TimeStampedResponse):
    payload: QdrantDocument = Field(
        ..., description="The item that matches the selection criteria"
    )


class QdrantPagedDocumentsResponse(TimeStampedResponse):
    payload: QdrantPageResponse = Field(
        ..., description="The page of documents that match the search"
    )


class QdrantListDocumentsResponse(TimeStampedResponse):
    payload: List[QdrantDocument] = Field(
        ..., description="The list of documents that match the search"
    )


class QdrantDictResponse(TimeStampedResponse):
    payload: Dict = Field(..., description="The payload of the response")
