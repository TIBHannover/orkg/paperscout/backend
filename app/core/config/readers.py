import toml

from app.core.config.models import AskSettings, LLMConfig, LLMSpecs

DEFAULT_CONFIG_PATH = "/ask/config/config.toml"


class TomlReader:
    def __init__(self, file_path: str):
        self.file_path = file_path

    def read(self) -> AskSettings:
        with open(self.file_path, "r") as file:
            data = toml.load(file)
        ask_data = data.get("ask", {})
        llm_data = ask_data.pop("llm", {})
        llm_instances_data = llm_data.pop("instances", {})
        llm_instances = [
            LLMSpecs(name=name, **config) for name, config in llm_instances_data.items()
        ]
        llm = LLMConfig(params=llm_data.get("params", {}), instances=llm_instances)
        return AskSettings(llm=llm, **ask_data)

    @staticmethod
    def get_settings(file_path: str | None) -> AskSettings:
        path = file_path if file_path else DEFAULT_CONFIG_PATH
        try:
            return TomlReader(path).read()
        except FileNotFoundError:
            print(
                f"WARNING: Config file not found at path {path}. Using default settings."
            )
            return AskSettings()
