from typing import List, Literal

from pydantic import Field
from pydantic_settings import BaseSettings


class ProviderBasedConfig(BaseSettings):
    def has_valid_provider(self) -> bool:
        """
        Check if the provider is valid.
        :return: True if the provider is valid, otherwise False
        """
        ...


class GeneralConfig(BaseSettings):
    app_name: str = Field("ASK backend", description="The name of the application.")
    host: str = Field("127.0.0.1", description="The host of the application.")
    port: int = Field(8000, description="The port of the application.")
    environment: Literal["dev", "test", "prod"] = Field(
        "test", description="The environment of the application."
    )
    interactive_docs: str | None = Field(
        None, description="The host for interactive docs of the application."
    )
    cors_origins: list[str] = Field(
        ["*"], description="The CORS origins of the application."
    )

    def is_dev_environment(self) -> bool:
        """
        Check if the environment is dev.
        :return: True if the environment is development, otherwise False
        """
        return self.environment == "dev"

    def is_test_environment(self) -> bool:
        """
        Check if the environment is test.
        :return: True if the environment is testing, otherwise False
        """
        return self.environment == "test"

    def is_prod_environment(self) -> bool:
        """
        Check if the environment is prod.
        :return: True if the environment is production, otherwise False
        """
        return self.environment == "prod"


class LLMParameters(BaseSettings):
    max_input_tokens: int | None = Field(
        None, description="The maximum number of input tokens."
    )
    context_size: int | None = Field(
        None,
        description="The size of the context that the model can process. Mainly used for Ollama provider.",
    )


class LLMSpecs(ProviderBasedConfig):
    name: str = Field("LLM", description="The name of the LLM.")
    provider: Literal["ollama", "tgi"] = Field(
        "ollama", description="The provider of the LLM."
    )
    host: str = Field("http://127.0.0.1:11434/", description="The host of the LLM.")
    model: str = Field("mistral", description="The model of the LLM.")
    max_input_tokens: int | None = Field(
        None, description="The maximum number of input tokens."
    )
    stream: bool = Field(False, description="Whether to stream the LLM.")

    def has_valid_provider(self) -> bool:
        return self.provider in ["ollama", "tgi"]


class LLMConfig(BaseSettings):
    params: LLMParameters | None = Field(
        None, description="The global parameters of the LLM."
    )
    instances: list[LLMSpecs] = Field([], description="The instances of running LLMs.")


class VectorStoreSearchConfig(BaseSettings):
    # Constants
    DEFAULT_SOURCE: str = "core"

    # Fields
    indexed_only: bool = Field(
        True, description="Whether to search only indexed vectors."
    )
    distinct_by_title: bool | None = Field(
        False, description="Whether to distinct by title."
    )
    default_source: str = Field(
        DEFAULT_SOURCE, description="The default source of the search. Default is None."
    )


class VectorStoreConfig(ProviderBasedConfig):
    provider: Literal["qdrant"] = Field(
        "qdrant", description="The provider of the vector store."
    )
    host: str = Field(":memory:", description="The host of the vector store.")
    grpc_port: int = Field(6334, description="The gRPC port of the vector store.")
    prefer_grpc: bool = Field(False, description="Whether to prefer gRPC over HTTP.")
    search: VectorStoreSearchConfig = Field(
        VectorStoreSearchConfig(),
        description="The search settings of the vector store.",
    )

    def has_valid_provider(self) -> bool:
        return self.provider in ["qdrant"]


class CacheCredentials(BaseSettings):
    email: str = Field("test@test.com", description="The email of the cache.")
    password: str = Field("test@test.com", description="The password of the cache.")


class CacheConfig(ProviderBasedConfig):
    provider: Literal["pocketbase"] = Field(
        "pocketbase", description="The provider of the cache."
    )
    host: str = Field("http://locahost:8080", description="The host of the cache.")
    credentials: CacheCredentials = Field(
        CacheCredentials(), description="The credentials of the cache."
    )
    authenticate: bool = Field(True, description="Whether to authenticate the cache.")

    def has_valid_provider(self) -> bool:
        return self.provider in ["pocketbase"]


class EmbeddingsConfig(ProviderBasedConfig):
    provider: Literal["infinity", "tei"] = Field(
        "infinity", description="The provider of the embeddings."
    )
    host: str = Field(
        "http://127.0.0.1:8081/", description="The host of the embeddings."
    )
    model: str = Field(
        "sentence-transformers/msmarco-MiniLM-L-12-v3",
        description="The model of the embeddings.",
    )

    def has_valid_provider(self) -> bool:
        return self.provider in ["infinity", "tei"]


class SecurityConfig(BaseSettings):
    limit_access: bool = Field(
        False,
        description="Whether to limit access to the application. Default is False.",
    )
    single_token: str | None = Field(
        None, description="The single token to access the application."
    )
    protected_routes: List[str] | None = Field(
        None,
        description="The protected routes of the application. [*] for all routes. Default is None.",
    )
    protected_verbs: List[str] | None = Field(
        None,
        description="The protected HTTP verbs of the application. Default is None.",
    )

    def is_enabled(self) -> bool:
        """
        Check if the security is enabled.
        :return: True if the security is enabled, otherwise False
        """
        return self.limit_access

    def is_single_access_token(self) -> bool:
        """
        Check if the access token is single.
        :return: True if the access token is single, otherwise False
        """
        return self.limit_access and self.single_token is not None

    def is_multi_access_token(self) -> bool:
        """
        Check if the access token is multi.
        :return: True if the access token is multi, otherwise False
        """
        return self.limit_access and self.single_token is None

    def protect_all_routes(self) -> bool:
        """
        Check if all routes are protected.
        :return: True if all routes are protected, otherwise False
        """
        return self.protected_routes is not None and "*" in self.protected_routes


class AskSettings(BaseSettings):
    general: GeneralConfig = Field(
        GeneralConfig(), description="The general settings of the application."
    )
    vector_store: VectorStoreConfig = Field(
        VectorStoreConfig(), description="The vector store settings of the application."
    )
    cache: CacheConfig = Field(
        CacheConfig(), description="The cache settings of the application."
    )
    embeddings: EmbeddingsConfig = Field(
        EmbeddingsConfig(), description="The embeddings settings of the application."
    )
    llm: LLMConfig = Field(
        LLMConfig(), description="The llm settings of the application."
    )
    security: SecurityConfig = Field(
        SecurityConfig(), description="The security settings of the application."
    )

    def get_default_llm(self) -> LLMSpecs:
        return self.llm.instances[0] or LLMSpecs()
