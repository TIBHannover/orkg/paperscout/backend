from abc import ABC, abstractmethod
from typing import List

Text = str | List[str]


class EmbeddingsEngine(ABC):
    @abstractmethod
    def compute_query_embeddings(self, *args, **kwargs) -> List[List]:
        """
        Compute the embeddings for a given user query
        """
        pass

    @abstractmethod
    def compute_source_embeddings(self, *args, **kwargs) -> List[List]:
        """
        Compute the embeddings for the source column in the database
        """
        pass

    @abstractmethod
    def ndims(self) -> int:
        """
        Return the dimensions of the vector column
        """
        pass

    @staticmethod
    def sanitize_input(texts: Text) -> List:
        """
        Sanitize the input to the embedding function.
        """
        if isinstance(texts, str):
            texts = [texts]
        return texts
