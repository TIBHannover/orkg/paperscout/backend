import http
import uuid
from typing import Annotated, List
from urllib.parse import unquote

from fastapi import APIRouter, Query

from app.core.decorators import log
from app.models.search import (
    BulkIndexRequest,
    CountRequest,
    ExploreRequest,
    IndexRequest,
    QdrantDictResponse,
    QdrantListDocumentsResponse,
    QdrantPagedDocumentsResponse,
    QdrantSingleDocumentResponse,
    RecommendRequest,
    SearchRequest,
)
from app.routers import FILTER_EXAMPLES
from app.routers.common import ResponseWrapper
from app.services import search

router = APIRouter(prefix="/index", tags=["Semantic Neural Search"])


@router.post(
    "/add",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantDictResponse,
)
@log(__name__)
def index_new_document(request: IndexRequest):
    """
    Index a new document in the vector database.
    """
    return ResponseWrapper.wrap_response(
        payload={"indexed": search.index_new_document(document=request)},
        type_=QdrantDictResponse,
    )


@router.post(
    "/add/bulk",
    status_code=http.HTTPStatus.OK,
)
@log(__name__)
def bulk_index_documents(request: BulkIndexRequest):
    """
    Index a list of documents in the vector database.
    """
    return ResponseWrapper.wrap_response(
        payload={"indexed": search.index_new_documents(documents=request.documents)},
        type_=QdrantDictResponse,
    )


@router.get(
    "/search",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantPagedDocumentsResponse,
)
@log(__name__)
def semantic_search(
    query: Annotated[str, Query(description="The text to search for")],
    limit: Annotated[
        int,
        Query(description="The maximum number of documents to return", le=100, gt=0),
    ] = 10,
    offset: Annotated[
        int | None,
        Query(description="The offset to start fetching documents from", ge=0),
    ] = None,
    filter: Annotated[
        str | None,
        Query(
            description="The criteria to filter the search results",
            openapi_examples=FILTER_EXAMPLES,
        ),
    ] = None,
    focus: Annotated[
        list[str] | None,
        Query(description="The sources to search from"),
    ] = None,
):
    """
    Searches for similar documents using ANN
    """
    return ResponseWrapper.wrap_response(
        payload=search.vector_search(
            request=SearchRequest(
                query=query, limit=limit, offset=offset, filter=filter, focus=focus
            )
        ),
        type_=QdrantPagedDocumentsResponse,
    )


@router.get(
    "/get/{document_id}",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantSingleDocumentResponse,
)
@log(__name__)
def retrieve_document(document_id: str):
    """
    Get a single document from the vector store via its ID
    """
    # URL decode the document ID
    document_id = unquote(document_id)
    return ResponseWrapper.wrap_response(
        payload=search.retrieve_single_document(document_id=document_id),
        type_=QdrantSingleDocumentResponse,
    )


@router.get(
    "/explore",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantPagedDocumentsResponse,
)
@log(__name__)
def explore_documents(
    filter: Annotated[
        str,
        Query(
            description="The criteria to filter the search results",
            openapi_examples=FILTER_EXAMPLES,
        ),
    ],
    limit: Annotated[
        int,
        Query(description="The maximum number of documents to return", le=100, gt=0),
    ] = 10,
    offset: Annotated[
        uuid.UUID | None,
        Query(description="The offset to start fetching documents from"),
    ] = None,
    order_by: Annotated[
        str | None,
        Query(description="The payload field to order by"),
    ] = None,
    focus: Annotated[
        list[str] | None,
        Query(description="The sources to explore documents from"),
    ] = None,
):
    """
    Explore documents from the vector store using filters
    """
    return ResponseWrapper.wrap_response(
        payload=search.retrieve_documents(
            request=ExploreRequest(
                filter=filter,
                limit=limit,
                offset=offset,
                order_by=order_by,
                focus=focus,
            )
        ),
        type_=QdrantPagedDocumentsResponse,
    )


@router.get(
    "/recommend",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantListDocumentsResponse,
)
@log(__name__)
def recommend_documents(
    document_ids: Annotated[
        List[str], Query(description="The document IDs to be used for recommendations")
    ],
    avoid_ids: Annotated[
        List[str] | None,
        Query(description="The document IDs to be avoided for recommendations"),
    ] = None,
    limit: Annotated[
        int,
        Query(description="The maximum number of documents to return", le=100, gt=0),
    ] = 10,
    offset: Annotated[
        int | None,
        Query(description="The offset to start fetching documents from", ge=0),
    ] = None,
    filter: Annotated[
        str | None,
        Query(
            description="The criteria to filter the search results",
            openapi_examples=FILTER_EXAMPLES,
        ),
    ] = None,
):
    """
    Searches for similar documents using an ANN index
    """
    # URL decode the document IDs
    document_ids = [unquote(document_id) for document_id in document_ids]
    if avoid_ids:
        avoid_ids = [unquote(avoid_id) for avoid_id in avoid_ids]
    return ResponseWrapper.wrap_response(
        payload=search.recommend_documents(
            request=RecommendRequest(
                document_ids=document_ids,
                avoid_ids=avoid_ids,
                limit=limit,
                offset=offset,
                filter=filter,
            )
        ),
        type_=QdrantListDocumentsResponse,
    )


@router.get(
    "/count",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantDictResponse,
)
@log(__name__)
def count_documents(
    filter: Annotated[
        str | None,
        Query(
            description="The criteria to filter the search results",
            openapi_examples=FILTER_EXAMPLES,
        ),
    ] = None,
):
    """
    Count documents from the vector store using regular filters
    """
    return ResponseWrapper.wrap_response(
        payload={"count": search.count_documents(request=CountRequest(filter=filter))},
        type_=QdrantDictResponse,
    )


@router.get(
    "/info",
    status_code=http.HTTPStatus.OK,
    response_model=QdrantDictResponse,
)
@log(__name__)
def get_indices_info():
    """
    Get information about the available indices.
    """
    return ResponseWrapper.wrap_response(
        payload=search.index_info(),
        type_=QdrantDictResponse,
    )
