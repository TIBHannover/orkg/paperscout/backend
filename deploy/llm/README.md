# Deploy components
Mainly this is used to deploy LLMs on the internal TIB infrastructure.

## How-to
The following files are required for this task: `ollama.def`

This file will be used to build the singularity image

```bash
singularity build ollama.sif ollama.def
```

Once you are done, and then run it with
```bash
sbatch --mem 32G -c 8 -w devbox5 -J orkg-ask-llm --gres=gpu:a3090:1 singularity.sh exec ollama.sif sh serve.sh mistral:instruct
```
