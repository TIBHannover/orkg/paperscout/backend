import unittest

from tests import TestAPIEndpoints, create_randomized_document_object


class TestSearchEndpoints(TestAPIEndpoints):
    def setUp(self):
        # reinitialize the store
        self._client.get("/index/reinitialize")

    def test_get_indices_info_200(self):
        response = self._client.get("/index/info")
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            "status" in response.json()["payload"]
        ), "The status should be in the payload"

    def test_count_all_documents_200(self):
        response = self._client.get("/index/count", params={})
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            "count" in response.json()["payload"]
        ), "The count should be in the payload"
        assert isinstance(
            response.json()["payload"]["count"], int
        ), "The count should be an integer"

    def test_count_with_filter_200(self):
        # Create some documents
        self._index_dummy_documents(3, year=2021)
        self._index_dummy_documents(3, year=2022)

        response = self._client.get("/index/count", params={})
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            "count" in response.json()["payload"]
        ), "The count should be in the payload"
        assert isinstance(
            response.json()["payload"]["count"], int
        ), "The count should be an integer"
        assert response.json()["payload"]["count"] == 6, "The count should be 6"

        response = self._client.get("/index/count", params={"filter": "year = 2021"})
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            "count" in response.json()["payload"]
        ), "The count should be in the payload"
        assert isinstance(
            response.json()["payload"]["count"], int
        ), "The count should be an integer"
        assert response.json()["payload"]["count"] == 3, "The count should be 3"

    def _index_dummy_documents(self, count: int, **kwargs):
        docs = [create_randomized_document_object(**kwargs) for _ in range(count)]
        response = self._client.post(
            "/index/add/bulk", json={"documents": [doc.model_dump() for doc in docs]}
        )
        assert response.status_code == 200, "The status code should be 200"

    def test_count_with_incorrect_filter_422(self):
        response = self._client.get("/index/count", params={"filter": "year ~= 2021"})
        assert (
            response.status_code == 422
        ), "The status code should be 422 (Unprocessable Entity)"
        assert "detail" in response.json(), "The detail should be in the response"

    def test_index_document_200(self):
        document = create_randomized_document_object()
        response = self._client.post("/index/add", json=document.model_dump())
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert response.json()["payload"] == {
            "indexed": True
        }, "The response should be indexed"

    def test_index_incorrect_document_422(self):
        response = self._client.post("/index/add", json={"SELECT * FROM": "documents"})
        assert (
            response.status_code == 422
        ), "The status code should be 422 (Unprocessable Entity)"
        assert (
            "detail" in response.json()
        ), "The details of the problem should be in the response"

    def test_index_multiple_documents_200(self):
        documents = [create_randomized_document_object() for _ in range(3)]
        response = self._client.post(
            "/index/add/bulk",
            json={"documents": [doc.model_dump() for doc in documents]},
        )
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert response.json()["payload"] == {
            "indexed": True
        }, "The response should be indexed"

    def test_search_documents_200(self):
        # Create some documents
        awesome_title = "Open Research Knowledge Graph is the best thing ever"
        self._index_dummy_documents(3, title=awesome_title)
        self._index_dummy_documents(3)

        response = self._client.get(
            "/index/search",
            params={"query": "Open Research Knowledge Graph", "limit": 4},
        )
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            len(response.json()["payload"]["items"]) == 4
        ), "The response should have 4 documents"
        assert all(
            doc["title"] == awesome_title
            for doc in response.json()["payload"]["items"][:-1]
        ), "All documents should have the awesome title except the last one"
        assert (
            response.json()["payload"]["items"][-1]["title"] != awesome_title
        ), "The last document should not have the awesome title"

    def test_search_documents_with_filter_200(self):
        awesome_title = "Open Research Knowledge Graph is the best thing ever"
        awesome_year = 2024
        not_awesome_year = 1996
        self._index_dummy_documents(3, title=awesome_title, year=awesome_year)
        self._index_dummy_documents(3, title=awesome_title, year=not_awesome_year)

        response = self._client.get(
            "/index/search",
            params={
                "query": "Open Research Knowledge Graph",
                "limit": 4,
                "filter": f"year = {awesome_year}",
            },
        )
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            len(response.json()["payload"]["items"]) == 3
        ), "The response should only have 3 documents"
        assert all(
            doc["year"] == awesome_year for doc in response.json()["payload"]["items"]
        ), f"All documents should have year={awesome_year}"

    def test_get_document_by_id_200(self):
        awesome_id = "123"
        self._index_dummy_documents(1, id=awesome_id)
        response = self._client.get(f"/index/get/{awesome_id}")
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            response.json()["payload"]["id"] == awesome_id
        ), f"The response should have the id: {awesome_id}"

    def test_explore_documents_by_filter_200(self):
        awesome_year = 2024
        not_awesome_year = 1992
        self._index_dummy_documents(3, year=awesome_year)
        self._index_dummy_documents(3, year=not_awesome_year)

        response = self._client.get(
            "/index/explore",
            params={
                "filter": f"year = {awesome_year}",
                "limit": 4,
            },
        )
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            len(response.json()["payload"]["items"]) == 3
        ), "The response should only have 3 documents"
        assert all(
            doc["year"] == awesome_year for doc in response.json()["payload"]["items"]
        ), f"All documents should have year={awesome_year}"

    @unittest.skip(
        "Need GPU to get meaningful embeddings. CI tests are running on CPU and will fail."
    )
    def test_recommend_documents_200(self):
        self._index_dummy_documents(1, id="123", title="Open research initiative")
        self._index_dummy_documents(1, id="345", title="Funding bodies for research")
        self._index_dummy_documents(1, id="567", title="Earth is a planet")
        self._index_dummy_documents(1, id="789", title="X-ray crystallography")

        response = self._client.get(
            "/index/recommend", params={"document_ids": ["123"], "limit": 1}
        )
        assert response.status_code == 200, "The status code should be 200"
        assert "payload" in response.json(), "The payload should be in the response"
        assert (
            len(response.json()["payload"]) == 1
        ), "The response should only have 1 document"
        assert (
            response.json()["payload"][0]["id"] != "123"
        ), "The response should not recommend the same document"
        assert (
            response.json()["payload"][0]["id"] == "345"
        ), "The response should recommend the document with id=345"
